import numpy as np

####SETTINGS####

## Global variables
modelname= "ashley39"

# Set to True if you want to run the model in parallel, set the cores to the amount of cores available
run_parallel = True
cores = 8

# Discretization
dates_selected =["2000-01-01", "2000-01-02", "2010-01-01", "2030-01-01", "2050-01-01", "2100-01-01", "2150-01-01", "2200-01-01", "2250-01-01" ]  
cellsize = 250 # x and y[m]
zmin =  -154 # [m from reference level]
zmax = 66. # [m from reference level]
z_discretization = np.array([-4] * 55) # first nmber is layer thickness [m], second number is number of layers [-]
proj_epsg = "epsg:2193" 

# Check if discretization matches with zmin and zmax, throw error if it doesn't match up:
assert z_discretization.sum() == (
    zmin - zmax
), "Vertical discretization does not add up to (zmax - zmin)"

# Porosity 
porosity_subsoil = 0.3 # [-]

# Concentrations 
conc_salt = 19 # [kg/m3]
conc_fresh = 0.0 # [kg/m3]

# Storage properties 
specific_storage_constant = 1.e-5 # [m-1]
specific_yield_constant = 0.15 #[-]

## General head boundary (sea) properties 
resistance_ghb_sea = 1.0 # [d]
slope_density_conc = 1.25 # [-]
density_ref = 1000.0 # [kg/m3]
conc_ghb_sea = conc_salt
conc_ghb_west = conc_fresh

## Properties drainage
resistance_drn = 1.0 # [d]

## Properties river
resistance_riv = 1.0 # [d]

## Recharge
rch_concentration = conc_fresh

## Apply sea level rise, enter sea level in mMSL
sea_level = -0.228 #[m NZVD]

## All external input paths

# Model area
path_area = "data/1-external/study_area/study_area_exact.shp"
path_area_boundary = "data/1-external/study_area/study_area_outline.shp" 

# Modeltop
path_dem = "data/1-external/DEM/DEM.tif" 
path_bathymetry = "data/1-external/bathymetry/gebco_2022_n-30.9375_s-58.3594_w162.0879_e186.6973.tif"  

# Geology
path_top = "data/1-external/geology/top_layers/"  
path_base = "data/1-external/geology/base_layers/"  

# Recharge
# path_rch = "data/1-external/recharge/RainfallRecharge_v3.0_inclAge_NHP_meanAnnual.tif"  
path_rch= "data/1-external/recharge/recharge_arrays.nc"

# River network and stages
path_waterways = "data/1-external/rivers/cant_riv_dn3v5.shp" 
path_watersheds = "data/1-external/watershed/cant_wsd_dn3v5.shp"

# Drainage
path_drainage_raster = "data/1-external/artificial_drainage/TPP_drainagecls_NZ_v1.img"
path_channels = "data/1-external/Drains/Stormwater_Channels.shp"  #check which method used
path_pipes = "data/1-external/Drains/Stormwater_Pipes.shp"
path_other = "data/1-external/Drains/Stormwater_Pipes_Other.shp"

#to netcfd
BDG_PKGS = ["rch", "ghb", "riv", "drn", "sto", "bnd"] 

# cross section
number_cross_sections = 6

#measured heads
path_measured_heads = "data/1-external/ashley_ecan_gw_data/ashley_ecan_gwl_data.shp"
path_monitoring_sites = "data/1-external/gw_level_monitoring_sites/Groundwater_Level_Monitoring_Sites.shp"

####ALL######
rule all:
   input:
      f"C:data/5-visualization/{modelname}/head/groundwater_depth_0.png",
      f"C:data/5-visualization/{modelname}/head/groundwater_level_0.png",
      f"C:data/5-visualization/{modelname}/head/groundwater_contours_0.png",     
      f"data/5-visualization/{modelname}/conc_cross_sections/map1.png",
      f"data/5-visualization/{modelname}/conc_cross_sections/map2.png",
      f"data/5-visualization/{modelname}/total_mass/total_amount_salt.png",
      f"data/5-visualization/{modelname}/total_mass/salt_mass.csv",
      f"data/5-visualization/{modelname}/heads_comparison/absolute_difference_heads.png",
      f"data/5-visualization/{modelname}/heads_comparison/measured_head.png",
      f"data/5-visualization/{modelname}/heads_comparison/modelled_heads.png",
      f"data/5-visualization/{modelname}/waterbalance/sankey.png",
      f"data/5-visualization/{modelname}/waterbalance/river_bdg.png",
      f"data/5-visualization/{modelname}/waterbalance/sea_bdg.png",
      f"data/5-visualization/{modelname}/waterbalance/drain_bdg.png",
      f"data/5-visualization/{modelname}/waterbalance/west_bdg.png",
      f"data/5-visualization/{modelname}/waterbalance/bdgdrn.png",

####BUILD#####
rule make_template:
    input:
        # External data
        path_area = path_area,
    params:
        cellsize=cellsize,
        zmin=zmin,
        zmax=zmax,
        z_discretization=z_discretization,
    output:
        path_template = f"data/2-interim/{modelname}/template.nc",
        path_template_2d = f"data/2-interim/{modelname}/template_2d.nc",
    script:
        "src/1-prepare/template.py"

rule make_modeltop:
    input:
        # External data
        path_dem = path_dem,
        path_bathymetry = path_bathymetry,
        # Interim paths
        path_template = f"data/2-interim/{modelname}/template.nc",
        path_template_2d = f"data/2-interim/{modelname}/template_2d.nc",
    params:
        modelname = modelname,  
    output:
        path_bathymetry_reprojected = f"data/2-interim/{modelname}/bathy_reproj.tif",
        path_modeltop = f"data/2-interim/{modelname}/modeltop.nc",
    script:
        "src/1-prepare/modeltop.py"

rule make_watermask:
    input:
        # Interim paths
        path_modeltop = f"data/2-interim/{modelname}/modeltop.nc",
        path_template = f"data/2-interim/{modelname}/template.nc",
        path_template_2d = f"data/2-interim/{modelname}/template_2d.nc",
    params:
        sea_level= sea_level,
    output:
        path_water_masks = f"data/2-interim/{modelname}/water_masks.nc",
    script:
        "src/1-prepare/watermask.py"

rule make_geology:
    input:
        # External data
        path_top = path_top,
        path_base = path_base,
        path_area = path_area,
        # Interim data
        path_template = f"data/2-interim/{modelname}/template.nc",
        path_template_2d = f"data/2-interim/{modelname}/template_2d.nc",
        path_modeltop = f"data/2-interim/{modelname}/modeltop.nc",
        path_water_masks = f"data/2-interim/{modelname}/water_masks.nc",
    params:
        porosity_subsoil = porosity_subsoil,
        specific_storage_constant = specific_storage_constant,
        specific_yield_constant = specific_yield_constant,
        modelname = modelname, 
        path_cross_section_k = f"data/2-interim/{modelname}/",
        number_cross_sections = number_cross_sections,
    output:
        path_conductivity = f"data/2-interim/{modelname}/conductivity.nc",
        path_formation_depth = f"data/2-interim/{modelname}/formation_depth.nc",
        path_formation_top = f"data/2-interim/{modelname}/formation_top.nc",
        path_ibound = f"data/2-interim/{modelname}/ibound.nc",
    script:
        "src/1-prepare/geology.py"

rule make_sheads:
    input:
        # External data
        path_area = path_area,
        # Interim data
        path_modeltop = f"data/2-interim/{modelname}/modeltop.nc",
        path_template = f"data/2-interim/{modelname}/template.nc",
        path_template_2d = f"data/2-interim/{modelname}/template_2d.nc",
    params:
        sea_level= sea_level,
    output:
        path_starting_heads = f"data/2-interim/{modelname}/starting_heads.nc",
    script:
        "src/1-prepare/sheads.py"
        
rule make_recharge:
    input:
        # External data
        path_rch = path_rch,
        path_area = path_area,
        # Interim data
        path_template = f"data/2-interim/{modelname}/template.nc",
        path_template_2d = f"data/2-interim/{modelname}/template_2d.nc",
        path_water_masks = f"data/2-interim/{modelname}/water_masks.nc",
    output:
        path_recharge = f"data/2-interim/{modelname}/recharge.nc",
    script:
        "src/1-prepare/recharge.py"

rule make_chloride:
    input: 
        # Interim data
        # path_conductivity = f"data/2-interim/{modelname}/conductivity.nc",
        path_ibound = f"data/2-interim/{modelname}/ibound.nc",
    params:
        initial_conc = conc_salt, # start fresh or salt
    output:
        path_chloride = f"data/2-interim/{modelname}/chloride.nc",
    script:
        "src/1-prepare/iconc.py"

rule make_ghb:
    input:
        # External data
        path_measured_heads = "data/1-external/ashley_ecan_gw_data/ashley_ecan_gwl_data.shp",
        path_boundary = "data/1-external/ghb/area.shp",
        # Interim data
        path_template = f"data/2-interim/{modelname}/template.nc",
        path_template_2d = f"data/2-interim/{modelname}/template_2d.nc",
        path_water_masks = f"data/2-interim/{modelname}/water_masks.nc",
        path_modeltop = f"data/2-interim/{modelname}/modeltop.nc",
        path_conductivity = f"data/2-interim/{modelname}/conductivity.nc",
        path_ibound = f"data/2-interim/{modelname}/ibound.nc",
    params:
        modelname=modelname,
        resistance_ghb_sea = resistance_ghb_sea,
        slope_density_conc = slope_density_conc,
        density_ref = density_ref,
        conc_ghb_sea = conc_ghb_sea,
        conc_ghb_west = conc_ghb_west,
        sea_level = sea_level,
        path_cross_section_ghb = f"data/2-interim/{modelname}/",
        number_cross_sections = number_cross_sections,
    output:
        path_ghb = f"data/2-interim/{modelname}/ghb.nc",
    script:
        "src/1-prepare/ghb.py"

rule make_river:
    input:
        # External data
        path_waterways = path_waterways,
        path_watersheds = path_watersheds,
        path_area = path_area,
        path_area_boundary = path_area_boundary,
        # Interim data
        path_template = f"data/2-interim/{modelname}/template.nc",
        path_template_2d = f"data/2-interim/{modelname}/template_2d.nc",
        path_modeltop = f"data/2-interim/{modelname}/modeltop.nc",
        path_ibound = f"data/2-interim/{modelname}/ibound.nc",
    params:
        modelname=modelname,
        waterway_resistance=resistance_riv,
    output:
        path_river = f"data/2-interim/{modelname}/river.nc",
    script:
        "src/1-prepare/rivers.py"

rule make_drn:
    input:
        # External data
        path_channels = path_channels,
        path_pipes = path_pipes,
        path_other = path_other,
        # path_area = path_area,
        # path_drainage_raster = path_drainage_raster,
        # Interim data
        path_template = f"data/2-interim/{modelname}/template.nc",
        path_template_2d = f"data/2-interim/{modelname}/template_2d.nc",
        path_water_mask = f"data/2-interim/{modelname}/water_masks.nc",
        path_modeltop = f"data/2-interim/{modelname}/modeltop.nc",
        path_ibound = f"data/2-interim/{modelname}/ibound.nc",
    params:
        modelname = modelname, 
        resistance_drn = resistance_drn,
    output:
        path_drn = f"data/2-interim/{modelname}/drn.nc",
    script:
        "src/1-prepare/drain.py"

rule make_drn2:
    input:
        # External data
        path_drainage_raster = "data/1-external/artificial_drainage/TPP_drainagecls_NZ_v1.img", # not needed maye
        # Interim data
        path_template = f"data/2-interim/{modelname}/template.nc",
        path_template_2d = f"data/2-interim/{modelname}/template_2d.nc",
        path_water_mask = f"data/2-interim/{modelname}/water_masks.nc",
        path_modeltop = f"data/2-interim/{modelname}/modeltop.nc",
        path_ibound = f"data/2-interim/{modelname}/ibound.nc",
    params:
        modelname = modelname, 
        resistance_drn = resistance_drn,
    output:
        path_drn2 = f"data/2-interim/{modelname}/drn2.nc",
    script:
        "src/1-prepare/drain_area.py"

rule make_model:
    input:
        # Interim data
        path_conductivity = f"data/2-interim/{modelname}/conductivity.nc",
        path_template = f"data/2-interim/{modelname}/template.nc",
        path_template_2d = f"data/2-interim/{modelname}/template_2d.nc",
        path_water_masks = f"data/2-interim/{modelname}/water_masks.nc",
        path_ghb = f"data/2-interim/{modelname}/ghb.nc",
        path_modeltop = f"data/2-interim/{modelname}/modeltop.nc",
        path_sconc = f"data/2-interim/{modelname}/chloride.nc",
        path_recharge = f"data/2-interim/{modelname}/recharge.nc",
        path_dem = f"data/2-interim/{modelname}/modeltop.nc",
        path_river = f"data/2-interim/{modelname}/river.nc",
        path_drn = f"data/2-interim/{modelname}/drn.nc",
        path_drn2 = f"data/2-interim/{modelname}/drn2.nc",
        path_sheads = f"data/2-interim/{modelname}/starting_heads.nc",
        path_formation_depth = f"data/2-interim/{modelname}/formation_depth.nc",
        path_ibound = f"data/2-interim/{modelname}/ibound.nc",
    params:
        modelname = modelname,
        run_parallel = run_parallel,
        cores = cores,
        rch_concentration = rch_concentration,
        dates_selected = dates_selected,
    output:
        runfile = f"data/3-input/{modelname}/{modelname}.run"
    script:
        "src/2-build/build_model.py"

####RUN####

rule run_model:
    input:
        f"data/3-input/{modelname}/{modelname}.run" # The runfile is the model definition file.
    params:
        modelname = modelname,
        run_parallel = run_parallel,
    output:
        temp(
            directory(
                expand(
                    f"data/4-output/{modelname}/bdg{{bdg_pkg}}", bdg_pkg=BDG_PKGS  #modelname #one bracket, without f
                )
            )
        ),
        f"data/4-output/{modelname}/conc",
        f"data/4-output/{modelname}/head",
    script:
        "src/2-build/run_model.py"    


rule idf_to_netcdf:
    input:
        bdg_dirs=expand(
            f"data/4-output/{modelname}/bdg{{bdg_pkg}}", bdg_pkg=BDG_PKGS  #modelname
        ),
        head_dir = f"data/4-output/{modelname}/head",
        conc_dir = f"data/4-output/{modelname}/conc",
    params:
        proj_epsg=proj_epsg,
    output:
        bdg_nc = f"data/4-output/{modelname}/bdg.nc",
        head_nc = f"data/4-output/{modelname}/head.nc",
        conc_nc = f"data/4-output/{modelname}/conc.nc",
    script:
        f"src/4-analyze/idf_to_netcdf.py"

####VISUALIZE####
rule get_cross_section:
    input:
        # External data
        # path_cross_section_shape = path_cross_section_shape,
        path_area = path_area,
        # Interim data
        path_template = f"data/2-interim/{modelname}/template.nc",
        path_template_2d = f"data/2-interim/{modelname}/template_2d.nc",
        path_formation_depth = f"data/2-interim/{modelname}/formation_depth.nc",
        path_formation_top = f"data/2-interim/{modelname}/formation_top.nc",
        path_water_masks = f"data/2-interim/{modelname}/water_masks.nc",
        path_modeltop = f"data/2-interim/{modelname}/modeltop.nc",
        # Output data
        path_conc_nc = f"data/4-output/{modelname}/conc.nc",
    params:
        modelname = modelname,
        path_cross_section = f"data/5-visualization/{modelname}/conc_cross_sections/",
        number_cross_sections = number_cross_sections,
        sea_level = sea_level,
    output:
        path_cross_section_map1 = f"data/5-visualization/{modelname}/conc_cross_sections/map1.png",
        path_cross_section_map2 = f"data/5-visualization/{modelname}/conc_cross_sections/map2.png",
    script:
        "src/5-visualize/cross_section.py"

rule get_salt_mass:
    input:
        path_template = f"data/2-interim/{modelname}/template.nc",
        path_template_2d = f"data/2-interim/{modelname}/template_2d.nc",
        path_conductivity = f"data/2-interim/{modelname}/conductivity.nc",
        # Output data
        path_conc_nc = f"data/4-output/{modelname}/conc.nc",
    params:
        modelname = modelname,
    output:
        mass_salt_csv=f"data/5-visualization/{modelname}/total_mass/salt_mass.csv",
        mass_salt_png=f"data/5-visualization/{modelname}/total_mass/total_amount_salt.png",
    script:
        "src/5-visualize/mass_salt.py"

rule get_heads:
    input:
        # External path
        path_area = path_area,
        # Interim data
        path_modeltop = f"data/2-interim/{modelname}/modeltop.nc",
        # Output paths
        path_head_nc = f"data/4-output/{modelname}/head.nc",
    params:
        modelname = modelname,
        path_head_visualization = f"data/5-visualization/{modelname}/head/",  
    output:
        f"C:data/5-visualization/{modelname}/head/groundwater_depth_0.png",
        f"C:data/5-visualization/{modelname}/head/groundwater_level_0.png",
        f"C:data/5-visualization/{modelname}/head/groundwater_contours_0.png", 
    script:
        "src/5-visualize/heads.py"

rule get_heads_comparison:
    input:
        # External path
        path_measured_heads = path_measured_heads,
        # path_measured_heads = path_monitoring_sites,
        path_area = path_area,
        path_dem = path_dem,
        # Interim path
        path_template = f"data/2-interim/{modelname}/template.nc",
        path_template_2d = f"data/2-interim/{modelname}/template_2d.nc",
        path_modeltop = f"data/2-interim/{modelname}/modeltop.nc",
        path_water_masks = f"data/2-interim/{modelname}/water_masks.nc",
        # Output paths
        path_head_nc = f"data/4-output/{modelname}/head.nc",
    params:
        modelname = modelname,
        path_validate_heads = f"data/5-visualization/{modelname}/heads_comparison/",
    output:
        absolute_difference_png = f"data/5-visualization/{modelname}/heads_comparison/absolute_difference_heads.png",
        measured_heads_png = f"data/5-visualization/{modelname}/heads_comparison/measured_head.png",
        modelled_heads_png = f"data/5-visualization/{modelname}/heads_comparison/modelled_heads.png",
        depth_measured_heads_png = f"data/5-visualization/{modelname}/heads_comparison/measured_heads_depth.png",
        depth_measured_heads_original_png = f"data/5-visualization/{modelname}/heads_comparison/measured_heads_depth_original.png",
        difference_png = f"data/5-visualization/{modelname}/heads_comparison/difference_heads.png",
        depth_measured_heads_interpolated_png = f"data/5-visualization/{modelname}/heads_comparison/measured_heads_depth_interpolated.png",
        heads_shp = f"data/5-visualization/{modelname}/heads_comparison/heads.shp",
    script:
        "src/5-visualize/heads_comparison.py"

rule get_waterbalance:
    input:
        bdg_nc=f"data/4-output/{modelname}/bdg.nc",
        path_area = path_area,
    params:
        modelname = modelname,
    output:
        sankey_png=f"data/5-visualization/{modelname}/waterbalance/sankey.png",
        path_river_bdg = f"data/5-visualization/{modelname}/waterbalance/river_bdg.png",
        path_sea_bdg = f"data/5-visualization/{modelname}/waterbalance/sea_bdg.png",
        path_drain_bdg = f"data/5-visualization/{modelname}/waterbalance/drain_bdg.png",
        path_west_bdg = f"data/5-visualization/{modelname}/waterbalance/west_bdg.png",
    script:
        "src/5-visualize/sankey.py"

rule get_waterbalance_timeseries:
    input:
        bdg_nc=f"data/4-output/{modelname}/bdg.nc",
    params:
        modelname = modelname,
        bdg_path = f"data/5-visualization/{modelname}/waterbalance/",
    output:
        f"data/5-visualization/{modelname}/waterbalance/bdgdrn.png",
    script:
        "src/5-visualize/waterbalance_timeseries.py"
