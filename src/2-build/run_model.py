import imod
import subprocess
import os, glob

## Parameters
params = snakemake.params
modelname = params["modelname"]
run_parallel = params["run_parallel"]

print("running model")
with imod.util.cd(f"data/3-input/{modelname}/"):
    subprocess.call(r"run_model.bat")

if run_parallel == True:
    # Merge model files if the model has run
    print("merging idf files")
    list_packages = [
        "conc",
        "head",
        "bdgbnd",
        "bdgdrn",
        "bdgfff",
        "bdgflf",
        "bdgfrf",
        "bdgghb",
        "bdgrch",
        "bdgriv",
        "bdgdrn",
        "bdgsto",
        "bdgwel",
        "dcdt"
    ]

    for pck in list_packages:
        print(pck)
        if os.path.exists(f"data/4-output/{modelname}/{pck}/"):
            data = imod.idf.open_subdomains(f"data/4-output/{modelname}/{pck}/*.idf")
            if pck == 'head': ## write heads to netcdf for quicker postprocessing
                data = data.transpose("y", "x","layer","time")
                ## Save last timesteps as idfs for re-start
                endyear = int(data.time.dt.year[-1].values)
                imod.idf.save(f"data/4-output/{modelname}/{pck}/{pck}.idf",data.sel(time=slice(f'{endyear}-1-1',f'{endyear}-12-31')).transpose('time','layer','y','x'))
            else:
                imod.idf.save(f"data/4-output/{modelname}/{pck}/{pck}", data)
            # Removing the parallel idf's to clean up
            for filename in glob.glob(f"data/4-output/{modelname}/{pck}/*p00*.idf"):
                os.remove(filename)
