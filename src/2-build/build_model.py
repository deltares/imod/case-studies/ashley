#%% Build model

import imod
import numpy as np
import xarray as xr

### Snakemake
## Parameters
params = snakemake.params
modelname = params["modelname"]
run_parallel = params["run_parallel"]
cores = params["cores"]
rch_concentration = params["rch_concentration"]
dates_selected = params["dates_selected"]
## Paths
# Input
path_conductivity = snakemake.input.path_conductivity
path_template = snakemake.input.path_template
path_template_2d = snakemake.input.path_template_2d
path_water_masks = snakemake.input.path_water_masks
path_ghb = snakemake.input.path_ghb
path_drn = snakemake.input.path_drn 
path_drn2 = snakemake.input.path_drn2   
path_modeltop = snakemake.input.path_modeltop
path_sconc = snakemake.input.path_sconc  
path_recharge = snakemake.input.path_recharge
path_riv = snakemake.input.path_river
path_sheads = snakemake.input.path_sheads
path_formation_depth = snakemake.input.path_formation_depth #maybe not needed if ibound loaded
path_ibound = snakemake.input.path_ibound

# ### Without Snakemake
# ## Parameters
# modelname= "ashley26"
# run_model = True 
# run_parallel = True
# rch_concentration = 0.0
# dates_selected =["2000-01-01", "2000-01-02", "2020-01-01", "3000-01-01"]
# ## Paths
# # Input
# path_conductivity = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/conductivity.nc" 
# path_template = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/template.nc" 
# path_template_2d = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/template_2d.nc" 
# path_water_masks = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/water_masks.nc"
# path_ghb = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/ghb.nc"
# path_drn = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/drn.nc"
# path_drn2 = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/drn2.nc"
# path_modeltop = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/modeltop.nc" 
# path_sconc = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/chloride.nc" 
# path_recharge = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/recharge.nc" 
# path_riv = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/river.nc"
# path_sheads = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/starting_heads.nc"
# path_ibound = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/ibound.nc"
# command_parallel = rf"call c:\Users\schadt\MPICH2\bin\mpiexec.exe -localonly 2 c:\Users\schadt\iMOD\iMOD-WQ_V5_4.exe {modelname}.run"    
# command_non_parallel = rf"call c:\Users\schadt\iMOD\iMOD-WQ_V5_4.exe {modelname}.run"

### General code

## Open data
# Open templates
like = xr.open_dataset(path_template)["template"]
like_2d = xr.open_dataset(path_template_2d)["template"]
dx, xmin, xmax, dy, ymin, ymax = imod.util.spatial_reference(like)
zmin = like.zbot.min()
zmax = like.ztop.max()
dz = like.dz
# Open Watermask
is_sea = xr.open_dataset(path_water_masks)["is_sea2d"]
# Drainage
drainage = xr.open_dataset(path_drn)
drn_cond = drainage["drn_cond"]
drn_elev = drainage["drn_depth"]
# Drainage 2
drainage2 = xr.open_dataset(path_drn2)
drn_cond2 = drainage2["drn_cond"]
drn_elev2 = drainage2["drn_depth"]
# Open general Head Boundary
ghb = xr.open_dataset(path_ghb)
# Open conductivity
conductivity = xr.open_dataset(path_conductivity)
kh = conductivity["kh"]
kv = conductivity["kv"]
porosity = conductivity["porosity"]
# Setting storange criteria
specific_storage = conductivity["specific_storage"]
specific_yield = conductivity["specific_yield"]
specific_storage_conf = conductivity["specific_storage_conf"]
# Open ibound
ibound = xr.open_dataarray(path_ibound)
bnd = ibound.copy()
# bnd[:,:,-1] = -1 
# bnd = xr.where(ibound==1, bnd, 0)
ibound[0,:,:].plot.imshow() #somehow does not work --error?
icbund = bnd.copy()
# icbund[40,:,:].plot.imshow()
#icbund.sel(z=-90, method="nearest").plot.imshow()
# Set top and bottom
layer = like.layer.values
bottom = bnd["zbot"].assign_coords(layer=("z", layer)) 
bottom = bottom.astype(float) #fixes bug
top = like.ztop.max().values 
# Open starting head 
starting_head = xr.open_dataarray(path_sheads)
# Open starting concentration
sconc = xr.open_dataarray(path_sconc)
# Open recharge 
rch = xr.open_dataset(path_recharge)
recharge_rate = rch["rch"]
# Open rivers
riv = xr.open_dataset(path_riv)


# fix bug
test = bnd.where(np.isnan(starting_head))
test[2,:,:].plot.imshow()


#%% Write model

print("writing model")

# Build the model
m = imod.wq.SeawatModel(modelname)

m["bas"] = imod.wq.BasicFlow(
    ibound=bnd, # put -1 for constant head
    top=top,
    bottom=bottom,
    starting_head=starting_head,
    inactive_head=-9999.0,
)

m["btn"] = imod.wq.BasicTransport(
    icbund=icbund, # put -1 for constant head
    starting_concentration=sconc,  # store bnd package in snakefile
    porosity=porosity,
    inactive_concentration=-9999.0,
)

m["adv"] = imod.wq.AdvectionTVD(courant=1.0) #same as sids
m["dsp"] = imod.wq.Dispersion(longitudinal=1.0) # Try one #in SIDS 1.7
m["vdf"] = imod.wq.VariableDensityFlow(density_concentration_slope=1.34) #Same as SIDS

m["rch"] = imod.wq.RechargeHighestActive(
    rate=recharge_rate, concentration=rch_concentration, save_budget=True
)

m["olf"] = imod.wq.Drainage(
    elevation=drn_elev,
    conductance=drn_cond,
    save_budget=True,
)

m["olf2"] = imod.wq.Drainage(
    elevation=drn_elev2,
    conductance=drn_cond2,
    save_budget=True,
)

m["ghb"] = imod.wq.GeneralHeadBoundary(
    head=ghb["stage"],
    conductance=ghb["cond"],
    density=ghb["density"],
    concentration=ghb["conc"],
    save_budget=True,
)

m["riv"] = imod.wq.River(
    stage=riv["stage"].where(bnd != 0),
    bottom_elevation=riv["bot"].where(bnd != 0),
    conductance=riv["cond"].where(bnd != 0),
    density=riv["density"].where(bnd != 0),
    save_budget=True,
)


m["lpf"] = imod.wq.LayerPropertyFlow(
    k_horizontal=like.copy(data=kh),
    k_vertical=like.copy(data=kv),
    layer_type=0,  # means it is confined / non convertible
    # specific_storage=like.copy(data=specific_storage), #without yield
    specific_storage=like.copy(data=specific_storage_conf), #with yield for upper layer
    # specific_yield=like.copy(data=specific_yield), # only read if layer type is not 0
    save_budget=True,
)

m["oc"] = imod.wq.OutputControl(
    save_head_idf=True, save_concentration_idf=True, save_budget_idf=True
)


m["pksf"] = imod.wq.ParallelKrylovFlowSolver(
    max_iter= 150, #150,
    inner_iter=100,
    hclose=0.001, #0.001# head difference, 0.001 due to steep topography, check where issue , 0.0001 before  #check if changes something when even higher
    rclose=1000.0,  # 100 #depends on cell size, maximum allowed mass change, bigger when bigger cells (cell area * hclose * 1000) # ask Joeri why 1000, would be 62500
    relax=0.98,
    partition="rcb",
)

m["pkst"] = imod.wq.ParallelKrylovTransportSolver(
    max_iter=150, inner_iter=50, cclose=1.0e-6, partition="rcb"
)

cf_dates = []
for date in dates_selected:
    cf_dates.append(imod.wq.timeutil.to_datetime(date, use_cftime=True))
    
m.time_discretization(cf_dates)  #dates_selected
m["time_discretization"]["transient"] = xr.full_like(
    m["time_discretization"]["timestep_duration"], True, dtype=np.bool
)
m["time_discretization"]["transient"][0] = False
m.write(
    f"data/3-input/{modelname}",
    result_dir=f"data/4-output/{modelname}",  
)


# Create batchfile to run model
with open("data/3-input/%s/run_model.bat" % modelname, "w") as f:
    if run_parallel == True:
        f.write(
            rf'"c:\Program Files\MPICH2\bin\mpiexec.exe" -localonly {cores} "..\..\..\bin\iMOD-WQ_V5_6.exe" "{modelname}.run"'
        )
    else:
        f.write(rf'"..\..\..\bin\iMOD-WQ_V5_6.exe" {modelname}.run')
    f.write("\n")
    f.write("timeout /t 2\n")
    # f.write("pause\n")
