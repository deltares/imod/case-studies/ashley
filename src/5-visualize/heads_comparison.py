#%% Validate heads

import pathlib
import geopandas as gpd
import imod
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import contextily as ctx
import xarray as xr
import os
import rasterio

### Snakemake
## Parameters
params = snakemake.params
modelname = params["modelname"]
path_validate_heads = params["path_validate_heads"]
## Paths
# Input
path_area = snakemake.input.path_area
path_measured_heads = snakemake.input.path_measured_heads
path_template = snakemake.input.path_template
path_template_2d = snakemake.input.path_template_2d
path_heads = snakemake.input.path_head_nc
path_dem = snakemake.input.path_dem
path_modeltop = snakemake.input.path_modeltop
path_water_masks = snakemake.input.path_water_masks
# Output
absolute_difference_png = snakemake.output.absolute_difference_png
measured_heads_png = snakemake.output.measured_heads_png
modelled_heads_png = snakemake.output.modelled_heads_png
depth_measured_heads_png = snakemake.output.depth_measured_heads_png
depth_measured_heads_original_png = snakemake.output.depth_measured_heads_original_png
difference_png = snakemake.output.difference_png
depth_measured_heads_interpolated_png = snakemake.output.depth_measured_heads_interpolated_png
heads_shp = snakemake.output.heads_shp


# ### Without Snakemake
# ## Parameters
# modelname = "ashley29"
# ## Paths
# # Input
# path_area = "c:/Users/schadt/training/ashley/data/1-external/study_area/study_area_exact.shp"
# path_measured_heads = "c:/Users/schadt/training/ashley/data/1-external/ashley_ecan_gw_data/ashley_ecan_gwl_data.shp"
# path_template = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/template.nc"
# path_template_2d = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/template_2d.nc"
# path_heads = f"c:/Users/schadt/training/ashley/data/4-output/{modelname}/head.nc"
# path_modeltop = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/modeltop.nc"
# path_dem = r"c:\Users\schadt\training\ashley\data\1-external\DEM\DEM.tif"  
# path_water_masks = f"C:/Users/schadt/training/ashley/data/2-interim/{modelname}/water_masks.nc"
# # Output
# path_validate_heads = f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/heads_comparison/"
# absolute_difference_png = f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/heads_comparison/absolute_difference_heads.png"
# measured_heads_png = f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/heads_comparison/measured_head.png"
# modelled_heads_png = f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/heads_comparison/modelled_heads.png"
# depth_measured_heads_png = f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/heads_comparison/measured_heads_depth.png"
# depth_measured_heads_original_png = f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/heads_comparison/measured_heads_depth_original.png"
# difference_png = f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/heads_comparison/difference_heads.png"
# depth_measured_heads_interpolated_png = f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/heads_comparison/measured_heads_depth_interpolated.png"
# heads_shp = f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/heads_comparison/heads.shp"


#%% General code

## Open data
# Open templates
like = xr.open_dataset(path_template)["template"]
like_2d = xr.open_dataset(path_template_2d)["template"]
dx, xmin, xmax, dy, ymin, ymax = imod.util.spatial_reference(like)
zmin = like.zbot.min()
zmax = like.ztop.max()
dz = like.dz
# Open dem
dem = rasterio.open(path_dem)
modeltop = xr.open_dataset(path_modeltop)['modeltop']
# Open heads
heads_calculated = xr.open_dataarray(path_heads)
heads_calculated = heads_calculated.isel(time=-1)  
depth_modelled = heads_calculated - modeltop

heads_measured = gpd.read_file(path_measured_heads)
heads_measured['x'] = heads_measured.geometry.x
heads_measured['y'] = heads_measured.geometry.y
heads_measured.rename(columns = {'field_1':'id'}, inplace = True)
# Get rid of measured heads with na in relevant columns
heads_measured = heads_measured[heads_measured['bottom_bot'].notna()]
heads_measured = heads_measured[heads_measured['dist_mp_to'].notna()]
heads_measured = heads_measured[heads_measured['mp_elevati'].notna()]
heads_measured = heads_measured[heads_measured['mean_gwl'].notna()]

heads_measured = heads_measured.where((heads_measured['mean_gwl'] < 0)) # do i need this
heads_measured['head'] = heads_measured['mp_elevati'] + heads_measured['mean_gwl'] - 0.356 #conversion factor
# Remove data outside area of interest
heads_measured = heads_measured.where((heads_measured['x'] > xmin) & (heads_measured['x'] < xmax)).dropna(
    axis=0, how="all"
)
heads_measured = heads_measured.where((heads_measured["y"] > ymin) & (heads_measured["y"] < ymax)).dropna(
    axis=0, how="all"
)


# # limit to measured heads with more than 100 measurements
# heads_measured = heads_measured.where(heads_measured["reading_co"] >= 100)

#heads_measured.plot()

# Calcualte bottom and top elevation
heads_measured['bottom_bot'] = heads_measured['mp_elevati'] -0.356 + heads_measured['dist_mp_to']  - heads_measured['bottom_bot'] 
heads_measured['top_topscr'] = heads_measured['mp_elevati'] -0.356 + heads_measured['dist_mp_to']  - heads_measured['top_topscr']
heads_measured["filt_middle"] = (heads_measured["top_topscr"] + heads_measured["bottom_bot"]) / 2

heads_measured["measurement_number"] = np.where(
    heads_measured["reading_co"] == 1, "1", np.where(
    (heads_measured["reading_co"] >1) & (heads_measured["reading_co"] <= 100), "2-100", ">100")) 


# heads_measured["markers"] = np.where(
#     heads_measured["reading_co"] == 1, "v", np.where(
#     (heads_measured["reading_co"] >1) & (heads_measured["reading_co"] <= 100), "x", "d")) 



heads_measured = heads_measured.where(  
    (heads_measured["bottom_bot"] > zmin.item()) & (heads_measured["bottom_bot"] < zmax.item())   #originally both bottom_bot
).dropna(axis=0, how="all")  #zmin is -100, zmax is 60


# depth of measured heads

# Read points from shapefile
heads_measured.index = range(len(heads_measured))
coords = [(x,y) for x, y in zip(heads_measured.x, heads_measured.y)]

# Sample the raster at every point location and store values in DataFrame
heads_measured['modeltop'] = [x for x in dem.sample(coords)]
heads_measured['modeltop'] = heads_measured.apply(lambda x: x['modeltop'][0], axis=1)
heads_measured["depth_measured_heads"] = heads_measured['head']  - heads_measured["modeltop"]  # if negative then below surface
heads_measured["depth_measured_heads_from_original_top"] = heads_measured['mean_gwl'] - heads_measured["dist_mp_to"]



# Add corresponding layer to validation data
def well_layers(like, elevation):
    inds = imod.select.points_indices(like, z = elevation)["z"].values  # does it select the closest one to the elvation value?
    layers = like.coords["layer"].values[inds]
    return layers

heads_measured["layer"] = well_layers(like, heads_measured["filt_middle"])

# #tests
# inds = imod.select.points_indices(like, z=-95)
# inds = imod.select.points_indices(like, z=heads_measured["filt_middle"])
# inds = imod.select.points_indices(like, z=heads_measured["filt_middle"])["z"].values  # does it select the closest one to the elvation value?
# layers = like.coords["layer"].values[inds]

# Select modeldata at measurement site
df_temp = pd.DataFrame()
dfs = []
for well_id, well_df in heads_measured.groupby("id"):
    x = well_df["x"].iloc[0]
    y = well_df["y"].iloc[0]
    layer = well_df["layer"].iloc[0]
    arrayhead = imod.select.points_values(heads_calculated, x=x, y=y, layer=layer)
    df_temp = pd.DataFrame()
    df_temp["time"] = arrayhead.time
    df_temp["modelled_head"] = arrayhead.values
    df_temp["id"] = well_id
    dfs.append(df_temp)

vali_data = pd.concat(dfs)
#vali_data2 = pd.concat(dfs).dropna

df = heads_measured.groupby("id").first()[
    ["x", "y", "top_topscr", "bottom_bot", "filt_middle", "layer", "measurement_number"] #reading_co added
]
df2 = df.dropna

# for depth

# Select modeldata at measurement site
df_temp = pd.DataFrame()
dfs = []
for well_id, well_df in heads_measured.groupby("id"):
    x = well_df["x"].iloc[0]
    y = well_df["y"].iloc[0]
    layer = well_df["layer"].iloc[0]
    arrayhead = imod.select.points_values(depth_modelled, x=x, y=y, layer=layer)
    df_temp = pd.DataFrame()
    df_temp["time"] = arrayhead.time
    df_temp["modelled_depth"] = arrayhead.values
    df_temp["id"] = well_id
    dfs.append(df_temp)

vali_data_2 = pd.concat(dfs)
#vali_data2 = pd.concat(dfs).dropna


df["modelled_head_mean"] = vali_data.groupby("id")["modelled_head"].mean()  #mean across time
df["modelled_depth_mean"] = vali_data_2.groupby("id")["modelled_depth"].mean() 
df["measured_head_mean"] = heads_measured.groupby("id")["head"].mean()  #no mean because only one value anyway?
df["depth_measured_heads"] = heads_measured.groupby("id")["depth_measured_heads"].mean()
df["depth_measured_heads_from_original_top"] = heads_measured.groupby("id")["depth_measured_heads_from_original_top"].mean()







# # should be the same
# df["modelled_head_mean"] = vali_data["modelled_head"]
# df["modelled_depth_mean"] = vali_data_2["modelled_depth"]
# df["measured_head_mean"] = heads_measured["head"]
# df["depth_measured_heads"] = heads_measured["depth_measured_heads"]
# df["depth_measured_heads_from_original_top"] = heads_measured["depth_measured_heads_from_original_top"]




df["depth_measured_heads_from_original_top"].max()
df["depth_measured_heads_from_original_top"].min()
df["modelled_depth_mean"].max()
df["modelled_depth_mean"].min()


df = df[df["modelled_head_mean"].notna()]
df = df[df["measured_head_mean"].notna()]
    
# df["modelled_head_mean"] = vali_data.groupby("id")["modelled_head"].mean()
# df["modelled_head_p10"] = vali_data.groupby("id")["modelled_head"].quantile(0.1)
# df["modelled_head_p90"] = vali_data.groupby("id")["modelled_head"].quantile(0.9)
# df["head_mean"] = val_da.groupby("id")["head"].mean()
# df["head_p10"] = val_da.groupby("id")["head"].quantile(0.1)
# df["head_p90"] = val_da.groupby("id")["head"].quantile(0.9)

df["diff"] = df["modelled_head_mean"] - df["measured_head_mean"]
df["absdiff"] = np.abs(df["diff"])


# df_test = df.where(df["modelled_head_mean"]>100)


#check again if really in aquifer
df_test = df.loc[df["modelled_head_mean"]>100.0]
df_test.layer
df.layer.max()
df = df.loc[df["modelled_head_mean"]<100.0]



SMALL_SIZE = 12 #8
MEDIUM_SIZE = 14 #10
BIGGER_SIZE = 16

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title



#%% Plot absolute difference in head

import shapely.geometry as sg

geometry = [sg.Point(float(x), float(y)) for x, y in zip(df["x"], df["y"])]

gdf = gpd.GeoDataFrame({"geometry": geometry})
for column in df.columns:
    gdf[column] = df[column].values

# Open overlay
study_area = gpd.read_file(path_area)

gdf_rp = gdf.copy()

# converting coordinates epsg:2193 to epsg:3857 to make base map
gdf.crs = "EPSG:2193"
gdfrp = gdf.to_crs("EPSG:3857") 
study_area = study_area.to_crs("EPSG:3857")


# save shapefile
gdf.to_file(heads_shp, driver='ESRI Shapefile')


# color bar settings
colors = "jet"
levels = [0, 0.25, 0.5, 1, 2, 3, 4, 5, 6, 8, 10]


# color bar magic. do not touch
nlevels = len(levels)
cmap = matplotlib.cm.get_cmap(colors)
colors = cmap(np.linspace(0, 1, nlevels + 1))
cmap = matplotlib.colors.ListedColormap(colors[1:-1])
# Make triangles white if data is not larger/smaller than legend_levels-range
cmap.set_under(colors[0])
cmap.set_over(colors[-1])
if gdf_rp["absdiff"].max() < levels[-1]:
    cmap.set_over("#FFFFFF")
if gdf_rp["absdiff"].min() > levels[0]:
    cmap.set_under("#FFFFFF")
norm = matplotlib.colors.BoundaryNorm(levels, cmap.N)
cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)

# Plot data points
fig, ax = plt.subplots(figsize=(15, 10))
# gdfrp.sort_values(by="absdiff").plot(
#     column="absdiff", ax=ax, legend=False, cmap=cmap, norm=norm, edgecolor="k"
# )


# split gdfrp based on attirbute

gdfrp_1 = gdfrp[gdfrp.measurement_number == "1"]
gdfrp_2 = gdfrp[gdfrp.measurement_number == "2-100"]
gdfrp_3 = gdfrp[gdfrp.measurement_number == ">100"]

gdfrp_1.sort_values(by="absdiff").plot(
    column="absdiff", ax=ax, legend=False, cmap=cmap, norm=norm, edgecolor="k", marker="x"
)

gdfrp_2.sort_values(by="absdiff").plot(
    column="absdiff", ax=ax, legend=False, cmap=cmap, norm=norm, edgecolor="k", marker="o"
)

gdfrp_3.sort_values(by="absdiff").plot(
    column="absdiff", ax=ax, legend=False, cmap=cmap, norm=norm, edgecolor="k", marker="d"
)


from matplotlib.lines import Line2D  # put legend outside plot, underneasth color bar, put title (number measurements)

# create manual symbols for legend
marker_1 = Line2D([0], [0], label='1', marker='x', markersize=10, 
         markeredgecolor='k', markerfacecolor='k', linestyle='')

marker_2 = Line2D([0], [0], label='2-100', marker='o', markersize=10, 
         markeredgecolor='k', markerfacecolor='k', linestyle='')

marker_3 = Line2D([0], [0], label='>100', marker='d', markersize=10, 
         markeredgecolor='k', markerfacecolor='k', linestyle='')

plt.legend(handles=[marker_1, marker_2, marker_3],  loc="lower right", title="Number of measurements")   #bbox_to_anchor=(1,0),


# Plot study area
study_area.plot(edgecolor="black", color="none", ax=ax)

# Plot basemap
#ctx.add_basemap(ax)
#ctx.add_basemap(ax,  source=ctx.providers.CartoDB.PositronOnlyLabels)
# ctx.add_basemap(ax,  source=ctx.providers.CartoDB.Voyager)
ctx.add_basemap(ax,  source=ctx.providers.Esri.WorldTopoMap)
from mpl_toolkits.axes_grid1 import make_axes_locatable

# Add colorbar
settings_cbar = {"ticks": levels, "extend": "both"}
divider = make_axes_locatable(ax)
cbar_ax = divider.append_axes("right", size="5%", pad="5%")
fig.colorbar(cbar, cmap=cmap, norm=norm, cax=cbar_ax, **settings_cbar, label ="Absolute difference between modelled and observed head [m]")

# Plot settings
ax.set_title("")
ax.set_ylabel("[m EPSG:3857]")
ax.set_xlabel("[m EPSG:3857]")
fig.tight_layout()

# save fig
fig.savefig(
    absolute_difference_png,
    bbox_inches='tight',
    dpi=300,
)



#%% Plot difference in head  (not absolute)

# color bar settings
colors = "jet"
levels = [-13, -11, -9, -7, -5, -2, -1, -0.5, -0.25, 0, 0.25, 0.5, 1, 2, 5, 7, 9, 11, 13]


# color bar magic. do not touch
nlevels = len(levels)
cmap = matplotlib.cm.get_cmap(colors)
colors = cmap(np.linspace(0, 1, nlevels + 1))
cmap = matplotlib.colors.ListedColormap(colors[1:-1])
# Make triangles white if data is not larger/smaller than legend_levels-range
cmap.set_under(colors[0])
cmap.set_over(colors[-1])
norm = matplotlib.colors.BoundaryNorm(levels, cmap.N)
cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)

# Plot data points
fig, ax = plt.subplots(figsize=(15, 10))

# split gdfrp based on attirbute

gdfrp_1 = gdfrp[gdfrp.measurement_number == "1"]
gdfrp_2 = gdfrp[gdfrp.measurement_number == "2-100"]
gdfrp_3 = gdfrp[gdfrp.measurement_number == ">100"]

gdfrp_1.sort_values(by="diff").plot(
    column="diff", ax=ax, legend=False, cmap=cmap, norm=norm, edgecolor="k", marker="x"
)

gdfrp_2.sort_values(by="diff").plot(
    column="diff", ax=ax, legend=False, cmap=cmap, norm=norm, edgecolor="k", marker="o"
)

gdfrp_3.sort_values(by="diff").plot(
    column="diff", ax=ax, legend=False, cmap=cmap, norm=norm, edgecolor="k", marker="d"
)


from matplotlib.lines import Line2D  # put legend outside plot, underneasth color bar, put title (number measurements)

# create manual symbols for legend
marker_1 = Line2D([0], [0], label='1', marker='x', markersize=10, 
         markeredgecolor='k', markerfacecolor='k', linestyle='')

marker_2 = Line2D([0], [0], label='2-100', marker='o', markersize=10, 
         markeredgecolor='k', markerfacecolor='k', linestyle='')

marker_3 = Line2D([0], [0], label='>100', marker='d', markersize=10, 
         markeredgecolor='k', markerfacecolor='k', linestyle='')

plt.legend(handles=[marker_1, marker_2, marker_3],  loc="lower right", title="Number of measurements")   #bbox_to_anchor=(1,0),

# https://www.statology.org/matplotlib-legend-outside-plot/#:~:text=Often%20you%20may%20want%20to%20place%20the%20legend,Example%201%3A%20Place%20Legend%20in%20Top%20Right%20Corner

# https://stackoverflow.com/questions/39500265/how-to-manually-create-a-legend

# https://stackoverflow.com/questions/53873993/how-to-put-a-colorbar-into-a-matplotlib-legend

# Plot study area
study_area.plot(edgecolor="black", color="none", ax=ax)

# Plot basemap
#ctx.add_basemap(ax)
#ctx.add_basemap(ax,  source=ctx.providers.CartoDB.PositronOnlyLabels)
# ctx.add_basemap(ax,  source=ctx.providers.CartoDB.Voyager)
ctx.add_basemap(ax,  source=ctx.providers.Esri.WorldTopoMap)
from mpl_toolkits.axes_grid1 import make_axes_locatable

# Add colorbar
settings_cbar = {"ticks": levels, "extend": "both"}
divider = make_axes_locatable(ax)
cbar_ax = divider.append_axes("right", size="5%", pad="5%")
fig.colorbar(cbar, cmap=cmap, norm=norm, cax=cbar_ax, **settings_cbar, label ="Modelled - observed head [m]")

# Plot settings
ax.set_title("")
ax.set_ylabel("[m EPSG:3857]")
ax.set_xlabel("[m EPSG:3857]")
fig.tight_layout()

# save fig
fig.savefig(
    difference_png,
    bbox_inches='tight',
    dpi=300,
)


#%% Plot measured heads

# color bar settings
colors = "jet"
levels = [-1, 0, 0.25, 0.5, 1, 2, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 30, 31, 33, 35, 37, 39, 41, 43, 45, 47, 50, 55, 60]

# color bar magic. do not touch
nlevels = len(levels)
cmap = matplotlib.cm.get_cmap(colors)
colors = cmap(np.linspace(0, 1, nlevels + 1))
cmap = matplotlib.colors.ListedColormap(colors[1:-1])
# Make triangles white if data is not larger/smaller than legend_levels-range
cmap.set_under(colors[0])
cmap.set_over(colors[-1])
if gdf_rp["absdiff"].max() < levels[-1]:
    cmap.set_over("#FFFFFF")
if gdf_rp["absdiff"].min() > levels[0]:
    cmap.set_under("#FFFFFF")
norm = matplotlib.colors.BoundaryNorm(levels, cmap.N)
cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)

# Plot data points
fig, ax = plt.subplots(figsize=(15, 10))
gdfrp.sort_values(by="measured_head_mean").plot(
    column="measured_head_mean", ax=ax, legend=False, cmap=cmap, norm=norm, edgecolor="k"
)

# Plot study area
study_area.plot(edgecolor="black", color="none", ax=ax)

# Plot basemap
import contextily as ctx
#ctx.add_basemap(ax)
#ctx.add_basemap(ax,  source=ctx.providers.CartoDB.PositronOnlyLabels)
# ctx.add_basemap(ax,  source=ctx.providers.CartoDB.Voyager)
ctx.add_basemap(ax,  source=ctx.providers.Esri.WorldTopoMap)
from mpl_toolkits.axes_grid1 import make_axes_locatable

# Add colorbar
settings_cbar = {"ticks": levels, "extend": "both"}
divider = make_axes_locatable(ax)
cbar_ax = divider.append_axes("right", size="5%", pad="5%")
fig.colorbar(cbar, cmap=cmap, norm=norm, cax=cbar_ax, **settings_cbar, label ="Observed head [m NZVD]")

# Plot settings
ax.set_title("")
ax.set_ylabel("[m EPSG:3857]")
ax.set_xlabel("[m EPSG:3857]")
fig.tight_layout()


# save fig

fig.savefig(
    measured_heads_png,
    bbox_inches='tight',
    dpi=300,
)


#%% Plot measured heads depth below modeltop

# color bar settings
colors = "jet"
levels = [-10, -8, -6, -5, -3, -2, -1, 0, 1]

# color bar magic. do not touch
nlevels = len(levels)
cmap = matplotlib.cm.get_cmap(colors)
colors = cmap(np.linspace(0, 1, nlevels + 1))
cmap = matplotlib.colors.ListedColormap(colors[1:-1])
# Make triangles white if data is not larger/smaller than legend_levels-range
cmap.set_under(colors[0])
cmap.set_over(colors[-1])
# cmap.set_under(colors[-1])
# cmap.set_over(colors[0])
norm = matplotlib.colors.BoundaryNorm(levels, cmap.N)
cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)

# Plot data points
fig, ax = plt.subplots(figsize=(15, 10))
gdfrp.sort_values(by="depth_measured_heads").plot(
    column="depth_measured_heads", ax=ax, legend=False, cmap=cmap, norm=norm, edgecolor="k"
)

# Plot study area
study_area.plot(edgecolor="black", color="none", ax=ax)

# Plot basemap
import contextily as ctx
#ctx.add_basemap(ax)
#ctx.add_basemap(ax,  source=ctx.providers.CartoDB.PositronOnlyLabels)
# ctx.add_basemap(ax,  source=ctx.providers.CartoDB.Voyager)
ctx.add_basemap(ax,  source=ctx.providers.Esri.WorldTopoMap)
from mpl_toolkits.axes_grid1 import make_axes_locatable

# Add colorbar
settings_cbar = {"ticks": levels, "extend": "both"}
divider = make_axes_locatable(ax)
cbar_ax = divider.append_axes("right", size="5%", pad="5%")
fig.colorbar(cbar, cmap=cmap, norm=norm, cax=cbar_ax, **settings_cbar, label ="Depth of observed head in relation to modeltop [m]")

# Plot settings
ax.set_title("")
ax.set_ylabel("[m EPSG:3857]")
ax.set_xlabel("[m EPSG:3857]")
fig.tight_layout()


# save fig

fig.savefig(
    depth_measured_heads_png,
    bbox_inches='tight',
    dpi=300,
)


#%% Plot measured heads depth original data

# color bar settings
colors = "jet"
# levels = [-10, -9, -7, -5, -3, -1, 0, 1]
levels = [-10, -8, -6, -5, -3, -2, -1, 0, 1]

# color bar magic. do not touch
nlevels = len(levels)
cmap = matplotlib.cm.get_cmap(colors)
colors = cmap(np.linspace(0, 1, nlevels + 1))
cmap = matplotlib.colors.ListedColormap(colors[1:-1])
# Make triangles white if data is not larger/smaller than legend_levels-range
cmap.set_under(colors[0])
cmap.set_over(colors[-1])
# cmap.set_under(colors[-1])
# cmap.set_over(colors[0])
norm = matplotlib.colors.BoundaryNorm(levels, cmap.N)
cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)

# Plot data points
fig, ax = plt.subplots(figsize=(15, 10))
gdfrp.sort_values(by="depth_measured_heads_from_original_top").plot(
    column="depth_measured_heads_from_original_top", ax=ax, legend=False, cmap=cmap, norm=norm, edgecolor="k"
)

# Plot study area
study_area.plot(edgecolor="black", color="none", ax=ax)

# Plot basemap
import contextily as ctx
#ctx.add_basemap(ax)
#ctx.add_basemap(ax,  source=ctx.providers.CartoDB.PositronOnlyLabels)
# ctx.add_basemap(ax,  source=ctx.providers.CartoDB.Voyager)
ctx.add_basemap(ax,  source=ctx.providers.Esri.WorldTopoMap)
from mpl_toolkits.axes_grid1 import make_axes_locatable

# Add colorbar
settings_cbar = {"ticks": levels, "extend": "both"}
divider = make_axes_locatable(ax)
cbar_ax = divider.append_axes("right", size="5%", pad="5%")
fig.colorbar(cbar, cmap=cmap, norm=norm, cax=cbar_ax, **settings_cbar, label ="Observed depth of groundwater in relation to ground level [m]")


# Plot settings
ax.set_title("")
ax.set_ylabel("[m EPSG:3857]")
ax.set_xlabel("[m EPSG:3857]")
fig.tight_layout()


# save fig

fig.savefig(
    depth_measured_heads_original_png,
    bbox_inches='tight',
    dpi=300,
)


#%% Plot measured heads depth original data extrapolation

df["abs_measured_depth"] = np.abs(df["depth_measured_heads_from_original_top"])
measured_depth = df

# # Open area to be used for interpolation (different to model area since it contains all measurement points)
# # cellsize = 250 #snakemake
# # extent = get_extent(path_boundary, cellsize)
# # xmin, ymin, xmax, ymax = extent
# # Discretization
# # cellsize = float(cellsize) #was np.float before, but gives warning
# # dx = cellsize
# # dy = -cellsize
grid = imod.util.empty_2d(dx, xmin, xmax, dy, ymin, ymax)
# # Remove points outside the grid (should be none)
# measured_depth['x'] = df.x
# measured_depth['y'] = df.y
# points_outside_grid = (
#     (measured_depth["x"] < xmin)
#     | (measured_depth["x"] > xmax)
#     | (measured_depth["y"] < ymin)
#     | (measured_depth["y"] > ymax)
# )
# measured_depth_in_grid = measured_depth.loc[~points_outside_grid]
# measured_depth_in_grid.head(5)

# The previous head information needs to be assigned to the model grid.
# imod-python has a tool called `imod.select.points_set_values
# <https://deltares.gitlab.io/imod/imod-python/api/select.html#imod.select.points_set_values>`_, which
# assigns values based on x-y coordinates to a previously defined array. In this
# case, the array is the grid, the values are the mean
# calculated heads and the x and y are the coordinates corresponding to the
# heads.

x = measured_depth["x"]
y = measured_depth["y"]

measured_depth_grid = imod.select.points_set_values(
    grid,
    values=measured_depth["abs_measured_depth"].to_list(),
    x=x.to_list(),
    y=y.to_list(),
)






# Plotting the points
fig, ax = plt.subplots()
measured_depth_grid.plot.imshow(ax=ax)

# The previous information is still only available at certain points, so it
# needs to be interpolated. The iMOD Python tool
# `imod.prepare.laplace_interpolate
# <https://deltares.gitlab.io/imod/imod-python/api/prepare.html#imod.prepare.laplace_interpolate>`_ will be
# used to do an interpolation of the previously indicated head values. It is
# possible to assign interpolation parameters such as the number of iterations
# and the closing criteria.

interpolated_measured_depth = imod.prepare.laplace_interpolate(
    measured_depth_grid, close=0.001, mxiter=150, iter1=100
)


watermask = xr.open_dataset(path_water_masks)["is_sea2d"]
watermask.plot.imshow()
interpolated_measured_depth = interpolated_measured_depth.where(np.isnan(watermask), 0.0)
interpolated_measured_depth = interpolated_measured_depth.where(interpolated_measured_depth >= 0, 0.0).where(~modeltop.isnull()) #do I need this
# depth = depth.where(area_raster.notnull())
interpolated_measured_depth = interpolated_measured_depth.rename("gw depth (m)") #do I need this
# depth.plot.imshow()

import geopandas as gpd
gdf = gpd.read_file(path_area)
ibound_2d = imod.prepare.rasterize(gdf, like_2d, column=None)
ibound_2d.plot.imshow()
interpolated_measured_depth = interpolated_measured_depth.where(ibound_2d == 1) #.fillna(0)

interpolated_measured_depth.plot.imshow()


    
# Plot groundwater depth 
# Set levels
levels_depth = [
    0.0,
    0.25,
    0.5,
    0.75,
    1.0,
    1.25,
    1.5,
    1.75,
    2,
    2.25,
    2.5,
    3,
    4,
    5.0,
    7.5,
    10,
    15,
    20,
    30,
] 

print("plotting depth to groundwater...")

plt.axis("scaled")
fig, ax = imod.visualize.plot_map(
        interpolated_measured_depth,
        colors="jet",
        levels=levels_depth,
        # overlays=overlays,
        figsize=[15, 10],
        kwargs_colorbar={"label": "Observed depth of groundwater from ground level [m] "},
    )
ax.set_facecolor("0.85")
ax.set_title(
        f"" 
    )
ax.set_ylabel("[m EPSG:2193]")
ax.set_xlabel("[m EPSG:2193]")
fig.tight_layout()
fig.savefig(
        depth_measured_heads_interpolated_png,
        bbox_inches='tight',
        dpi=300,
    )    


#%% Plot modelled head
# color bar settings
colors = "jet"
levels = [-1, 0, 0.25, 0.5, 1, 2, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 30, 31, 33, 35, 37, 39, 41, 43, 45, 47, 50, 55, 60]

# color bar magic. do not touch
nlevels = len(levels)
cmap = matplotlib.cm.get_cmap(colors)
colors = cmap(np.linspace(0, 1, nlevels + 1))
cmap = matplotlib.colors.ListedColormap(colors[1:-1])
# Make triangles white if data is not larger/smaller than legend_levels-range
cmap.set_under(colors[0])
cmap.set_over(colors[-1])
if gdf_rp["absdiff"].max() < levels[-1]:  #absdiff probably wrong here
    cmap.set_over("#FFFFFF")
if gdf_rp["absdiff"].min() > levels[0]:
    cmap.set_under("#FFFFFF")
norm = matplotlib.colors.BoundaryNorm(levels, cmap.N)
cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)


# Plot data points
fig, ax = plt.subplots(figsize=(15, 10))
gdfrp.sort_values(by="modelled_head_mean").plot(
    column="modelled_head_mean", ax=ax, legend=False, cmap=cmap, norm=norm, edgecolor="k"
)

# Plot study area
study_area.plot(edgecolor="black", color="none", ax=ax)

# Plot basemap
import contextily as ctx
#ctx.add_basemap(ax)
#ctx.add_basemap(ax,  source=ctx.providers.CartoDB.PositronOnlyLabels)
# ctx.add_basemap(ax,  source=ctx.providers.CartoDB.Voyager)
ctx.add_basemap(ax,  source=ctx.providers.Esri.WorldTopoMap)
from mpl_toolkits.axes_grid1 import make_axes_locatable

# Add colorbar
settings_cbar = {"ticks": levels, "extend": "both"}
divider = make_axes_locatable(ax)
cbar_ax = divider.append_axes("right", size="5%", pad="5%")
fig.colorbar(cbar, cmap=cmap, norm=norm, cax=cbar_ax, **settings_cbar, label ="Modelled head [m NZVD]")

# Plot settings
ax.set_title("")
ax.set_ylabel("[m EPSG:3857]")
ax.set_xlabel("[m EPSG:3857]")
fig.tight_layout()


# save fig

fig.savefig(
    modelled_heads_png,
    bbox_inches='tight',
    dpi=300,
)



#%% Plot comparison modelled and measured head depth

SMALL_SIZE = 8 #8
MEDIUM_SIZE = 8 #10
BIGGER_SIZE = 12

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title



for zrange in [[0, -100]]: # [[0, -10], [-10, -50]]:  # [0,-10]=phreatic, [-10,-50]=1wvp

    in_layer = df[
        (df["filt_middle"] < zrange[0]) & (df["filt_middle"] > zrange[1])
    ]

    x = in_layer["modelled_depth_mean"]
    y = in_layer["depth_measured_heads_from_original_top"]

    max_value = max([x.max(), y.max()])
    min_value = min([x.min(), y.min()])

    fig, ax = plt.subplots()
    ax.set_xlabel("Modelled depth [m below modeltop]")
    ax.set_ylabel("Observed depth [m below ground level]")
    ax.set_title("")  #not NAP, other vertical datum
    ax.plot([max_value, min_value], [max_value, min_value], "k-")
    ax.plot(
        [max_value + 0.5, min_value + 0.5],
        [max_value, min_value],
        "k--",
        alpha=0.5,
    )
    ax.plot([max_value + 1.0, min_value + 1.0], [max_value, min_value], "k--")
    ax.plot(
        [max_value - 0.5, min_value - 0.5],
        [max_value, min_value],
        "k--",
        alpha=0.5,
    )
    ax.plot([max_value - 1.0, min_value - 1.0], [max_value, min_value], "k--")
    ax.scatter(x, y, zorder=20, s=1, c="b")
    fig.tight_layout()
    fig.savefig(
        os.path.join(path_validate_heads, f"Comparison modelled and measured depth.png"),
        # f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/validate_heads/mean between {zrange[0]} and {zrange[1]}m NAP.png",
        bbox_inches='tight',
        dpi=300,
    )


#%% Plot comparison modelled and measured head


#https://dlab.berkeley.edu/news/adding-basemaps-python-contextily#:~:text=Basemaps%20are%20the%20contextual%20map%20data%2C%20like%20Google,to%20save%20map%20tiles%20as%20geospatial%20raster%20files.
#Esri
# dict_keys(['WorldStreetMap', 'DeLorme', 'WorldTopoMap', 'WorldImagery', 'WorldTerrain', 'WorldShadedRelief', 'WorldPhysical', 'OceanBasemap', 'NatGeoWorldMap', 'WorldGrayCanvas', 'ArcticImagery', 'ArcticOceanBase', 'ArcticOceanReference', 'AntarcticImagery', 'AntarcticBasemap'])


# df.to_csv(f"data/5-visualization/{modelname}/validate_heads/head-results.csv")
# df[df['top_topscr'] < -10.0].to_csv(f"data/5-visualization/{modelname}/validate_heads/head-deep.csv")
# df[df['top_topscr'] >= -10.0].to_csv(f"data/5-visualization/{modelname}/validate_heads/head-shallow.csv")



from sklearn.metrics import mean_squared_error
import scipy

## plots
for zrange in [[0, -50]]: # [[0, -10], [-10, -50]]:  # [0,-10]=phreatic, [-10,-50]=1wvp

    in_layer = df[
        (df["filt_middle"] < zrange[0]) & (df["filt_middle"] > zrange[1])
    ]

    x = in_layer["measured_head_mean"]
    y = in_layer["modelled_head_mean"]
    range_data= x.max()-x.min()
    rms = mean_squared_error(x, y, squared=False)/range_data * 100  # normalized RMSE in %
    slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(x, y)  # which order?
    rms = round(rms, 2)
    r_value = round(r_value, 2)
    
    max_value = max([x.max(), y.max()])
    min_value = min([x.min(), y.min()])

    fig, ax = plt.subplots()
    ax.set_xlabel("Observed head [m NZVD]")
    ax.set_ylabel("Modelled head [m NZVD]")
    ax.set_title("")  #not NAP, other vertical datum
    ax.annotate(f"Normalized RMSE = {rms} % \nR\N{SUPERSCRIPT TWO} = {r_value}",xy=(0, 30))
    ax.plot([max_value, min_value], [max_value, min_value], "k-")
    # ax.plot(
    #     [max_value + 0.5, min_value + 0.5],
    #     [max_value, min_value],
    #     "k--",
    #     alpha=0.5,
    # )
    # ax.plot([max_value + 1.0, min_value + 1.0], [max_value, min_value], "k--")
    # ax.plot(
    #     [max_value - 0.5, min_value - 0.5],
    #     [max_value, min_value],
    #     "k--",
    #     alpha=0.5,
    # )
    # ax.plot([max_value - 1.0, min_value - 1.0], [max_value, min_value], "k--")
    
    # groups = in_layer.groupby('measurement_number')
    # for name, group in groups:
    #      # plt.plot(x, y, marker='o', linestyle='', markersize=12, label=name)
    #      ax.scatter(x, y,  zorder=20, s=1, label=name)
    # plt.legend()

    in_layer_1 = in_layer[in_layer.measurement_number == "1"]
    in_layer_2 = in_layer[in_layer.measurement_number == "2-100"]
    in_layer_3 = in_layer[in_layer.measurement_number == ">100"]
    
    ax.scatter(in_layer_1["measured_head_mean"], in_layer_1["modelled_head_mean"], zorder=20, s=1, c="r", marker="o")
    ax.scatter(in_layer_2["measured_head_mean"], in_layer_2["modelled_head_mean"], zorder=20, s=1, c="b", marker="o")
    ax.scatter(in_layer_3["measured_head_mean"], in_layer_3["modelled_head_mean"], zorder=20, s=1, c="g", marker="o")
    
    # create manual symbols for legend
    marker_1 = Line2D([0], [0], label='1', marker='o', markersize=10, 
             markeredgecolor='r', markerfacecolor='r', linestyle='')

    marker_2 = Line2D([0], [0], label='2-100', marker='o', markersize=10, 
             markeredgecolor='b', markerfacecolor='b', linestyle='')

    marker_3 = Line2D([0], [0], label='>100', marker='o', markersize=10, 
             markeredgecolor='g', markerfacecolor='g', linestyle='')
    plt.rcParams["legend.fontsize"] = 10
    plt.legend(handles=[marker_1, marker_2, marker_3],  loc="lower right", title="Number of measurements") 
    
    
    #ax.scatter(x, y, zorder=20, s=1, c="b")
    fig.tight_layout()
    fig.savefig(
        os.path.join(path_validate_heads, f"Comparison modelled and measured heads.png"),
        # f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/validate_heads/mean between {zrange[0]} and {zrange[1]}m NAP.png",
        bbox_inches='tight',
        dpi=300,
    )



 #The Pearson correlation coefficient. The square of rvalue is equal to the coefficient of determination.
# https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.linregress.html





