#%% Water balance timeseries

import xarray as xr
import os
import matplotlib.pyplot as plt

### Snakemake
## Parameters
params = snakemake.params
modelname = params["modelname"]
bdg_path = params["bdg_path"]
## Paths
# Input
bdg_nc = snakemake.input.bdg_nc
# Output
# bdg_path = snakemake.output.bdg_path

# ### Withput Snakemake
# ## Parameters
# modelname = "ashley29"
# ## Paths
# # Input
# bdg_nc = f"c:/Users/schadt/training/ashley/data/4-output/{modelname}/bdg.nc"
# # Output
# bdg_path = f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/waterbalance/"

#%% Function
def split_bdg(ds, key):
    """Split budgets in a budget in and a budget out"""
    ds[f"{key}_out"] = ds[f"{key}"].where(ds[f"{key}"] < 0)
    ds[f"{key}_in"] = ds[f"{key}"].where(ds[f"{key}"] > 0)
    ds = ds.drop(f"{key}")
    return ds

#%% General code

## Open data
ds = xr.open_dataset(bdg_nc)
# ds = ds.isel(time=-1)  # for last timestep
# ds.bdgghb[40,:,:].plot.imshow()

# Split ghb budget in western boundary and sea
ds["bdgghb_west"] = ds["bdgghb"][:,:,:,0:1]
ds["bdgghb_sea"] = ds["bdgghb"][:,:,:,2:len(ds["bdgghb"].x)]
ds = ds.drop_vars("bdgghb")

# Split budgets in positive and negative flux
for pkg in ["bdgriv", "bdgsto", "bdgghb_west", "bdgghb_sea", "bdgbnd"]:  #"bdgghb", "bdgsto",
    if pkg in ds.keys():
        ds = split_bdg(ds, pkg)
        
# For some versions of imod-python spatial ref needs to be removed
if "spatial_ref" in ds.keys():
    ds = ds.drop("spatial_ref")

d_labels = {
    "riv_in": "River in",
    "riv_out": "River out",
    "ghb_in": "Western boundary/sea in",
    "ghb_out": "Western boundary/sea out",
    "ghb_west_in": "Western boundary in",
    "ghb_west_out": "Western boundary out",
    "ghb_sea_in": "Sea in",
    "ghb_sea_out": "Sea out",
    "bnd_in": "Constant head in",
    "bnd_out": "Constant head out",
    "sto_in": "Storage in",
    "sto_out": "Storage out",
    "drn": "Drain out",
    "rch": "Recharge",
}

# Settings for plot
SMALL_SIZE = 12 #8
MEDIUM_SIZE = 14 #10
BIGGER_SIZE = 16

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title



#%% Time series single


# data = []
for i in list(ds.data_vars):
    data = ds[i]
    label = d_labels[i.split("bdg")[1]]
    # label = [d_labels[i]]
    # Sum all flows
    som = data.sum(dim=["layer","x","y"])
    som_tabel = som.to_dataframe(name = 'flow [m^3/d]')
    som_tabel.to_csv(os.path.join(bdg_path, f"{i}.csv"))
    # Plot
    fig, ax = plt.subplots(figsize=[10,8])
    som = abs(som)
    som.plot.line("b", ax=ax)
    ax.set_title("")
    ax.set_ylabel(f"{label} [m$^3$/d]")
    ax.set_xlabel("Time [yrs]")
    plt.tight_layout()
    # Save figures
    fig.savefig(
        os.path.join(bdg_path, f"{i}.png"),
        bbox_inches='tight',
       dpi=300,
    )
    # np.append(data, som) 
    

#%% Time series double


# idea to get it either from csv files or to make a dataframe out of som above
    
# for j in ["bdgriv", "bdgghb_west",  "bdgghb_sea"]:  
#    data = 


# bdgriv = open(os.path.join(bdg_path, f"{i}.csv"))
# bdgriv = open(os.path.join(bdg_path, "bdgriv_in.csv"))


# fig, ax = plt.subplots(figsize=[10,8])
# som = abs(som)
# som.plot.line("b", ax=ax)
# ax.set_title(f"{label}")
# ax.set_ylabel("Flow (m3/d)")
# ax.set_xlabel("Time [yrs]")
# plt.tight_layout()
# # Save figures
# fig.savefig(
#     os.path.join(bdg_path, f"{i}.png"),
#    dpi=300,
# )
