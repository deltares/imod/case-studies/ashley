#%% Salt mass

import pathlib
import imod
import xarray as xr
import matplotlib.pyplot as plt
import nc_time_axis
import pandas as pd

# ### Snakemake
# ## Parameter
# params = snakemake.params
# modelname = params["modelname"]
# ## Paths
# # Input
# path_conductivity = snakemake.input.path_conductivity
# path_template = snakemake.input.path_template
# path_template_2d = snakemake.input.path_template_2d
# path_conc_nc = snakemake.input.path_conc_nc
# # Output
# mass_salt_csv = snakemake.output.mass_salt_csv
# mass_salt_png = snakemake.output.mass_salt_png

### Without Snakemake
## Parameter
modelname = "ashley32"
## Paths
# Input
path_conductivity = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/conductivity.nc"
path_template = f"C:/Users/schadt/training/ashley/data/2-interim/{modelname}/template.nc"
path_template_2d = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/template_2d.nc"
path_conc_nc = f"c:/Users/schadt/training/ashley/data/4-output/{modelname}/conc.nc"
# Output
mass_salt_csv = f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/total_mass/salt_mass.csv"
mass_salt_png = f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/total_mass/total_amount_salt.png"

#%% General code

# Open data
# Open templates
like = xr.open_dataset(path_template)["template"]
like_2d = xr.open_dataset(path_template_2d)["template"]
dx, xmin, xmax, dy, ymin, ymax = imod.util.spatial_reference(like)
zmin = like.zbot.min()
zmax = like.ztop.max()
dz = like.dz
# Open chloride
conc = xr.open_dataarray(path_conc_nc)
# Open porosity
conductivity = xr.open_dataset(path_conductivity)
porosity = conductivity["porosity"].swap_dims({"z": "layer"})

# Get mass loading over time
# data = conc*dx*(dy *-1)*(conc.dz * -1) * porosity  #conc in kg/ m3, dx dy in m  #somehow error
data = conc*dx*(dy *-1)*(dz[1] * -1) * porosity

# Sum all mass
som = data.sum(dim=["layer","x","y"])
som_tabel = som.to_dataframe(name = 'kgsalt')
som_tabel.to_csv(mass_salt_csv)

# Settings for plot
SMALL_SIZE = 12 #8
MEDIUM_SIZE = 14 #10
BIGGER_SIZE = 16

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

# Plot
som = som[3:-1]
fig, ax = plt.subplots(figsize=[10,8])
# som['time']= pd.to_datetime(som.time).strftime('%Y')
# som['time'] = som['time'].astype(int)   #astype(str).
# som['time']= som['time']-2000
som.plot.line("b", ax=ax)
ax.set_title("")
ax.set_ylabel("Total amount of salt [kg]")
ax.set_xlabel("Time of simulation [yrs]")
plt.tight_layout()
# Save figures
fig.savefig(
    mass_salt_png,
    bbox_inches='tight',
    dpi=300,
)

