#%% freshwater heads

import pathlib
import geopandas as gpd
import imod
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import numpy as np
import pandas as pd
import contextily as ctx
import xarray as xr
import os


### Without Snakemake
## Parameters
modelname = "ashley29"
## Paths
# Input
path_modeltop = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/modeltop.nc"
path_area = r"c:\Users\schadt\training\ashley\data\1-external\study_area\study_area_exact.shp"
path_heads = f"c:/Users/schadt/training/ashley/data/4-output/{modelname}/head.nc"
path_salt = f"c:/Users/schadt/training/ashley/data/4-output/{modelname}/conc.nc"
path_template = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/template.nc"
path_template_2d = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/template_2d.nc"

# Output
path_freshwater_head = f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/head/freshwater_head.nc"

#%% General code
## Open data
# Open templates
like = xr.open_dataset(path_template)["template"]
like_2d = xr.open_dataset(path_template_2d)["template"]
dx, xmin, xmax, dy, ymin, ymax = imod.util.spatial_reference(like)
zmin = like.zbot.min()
zmax = like.ztop.max()
dz = like.dz


# Open heads
head_i = xr.open_dataarray(path_heads)
conc = xr.open_dataarray(path_salt)
conc[0,1,:,:].plot.imshow()

upper_active_layer = imod.select.upper_active_layer(conc[0,:,:,:], is_ibound=False)

# # only for test
# upper_active_layer2 = upper_active_layer.sel(time = '2000-01-02')
# upper_active_layer2.plot.imshow()

test = conc.where(conc.layer == upper_active_layer)
test= test.max("layer")
test[-1,:,:].plot.imshow(vmax=5, vmin=0)







density_fresh = 1000

slope_density_conc = 1.25
density_i = conc * slope_density_conc + density_fresh
density_i[0,40,:,:].plot.imshow()


upper_active_layer = imod.select.upper_active_layer(density_i[0,:,:,:], is_ibound=False)

# # only for test
# upper_active_layer2 = upper_active_layer.sel(time = '2000-01-02')
# upper_active_layer2.plot.imshow()

test = density_i.where(density_i.layer == upper_active_layer)
test= test.max("layer")
test[0,:,:].plot.imshow()




z_i = like.z
z_i['z'] = z_i['layer']
z_i.rename({'layer': 'layerold'})
z_i = z_i.swap_dims({'z': 'layer'})

# z_i.values()


# z_i = xr.DataArray(like.z, coords={'layer': like.layer},
             #dims=['layer'])
# z_i.drop('z')
h_f_i = (density_i/density_fresh)*head_i - ((density_i-density_fresh)/density_fresh)*z_i




#h_f_i = (density_i/density_fresh)*head_i - ((density_i-density_fresh)/density_fresh)*like.z



h_f_i.to_netcdf(path_freshwater_head)


