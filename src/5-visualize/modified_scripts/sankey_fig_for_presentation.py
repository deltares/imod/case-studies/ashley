#%% Water balance

import numpy as np
import imod
import xarray as xr
import os
import matplotlib.pyplot as plt
from matplotlib.sankey import Sankey
import matplotlib.patches as mpatches
import matplotlib as mpl  
import contextily as ctx
import geopandas as gpd
import pandas as pd
from openpyxl.utils.dataframe import dataframe_to_rows
from openpyxl import Workbook, load_workbook

# ### Snakemake
# ## Parameters
# params = snakemake.params
# modelname = params["modelname"]
# ## Paths
# # Input
# bdg_nc = snakemake.input.bdg_nc
# path_area = snakemake.input.path_area
# # Output
# sankey_png = snakemake.output.sankey_png
# path_river_bdg = snakemake.output.path_river_bdg
# path_sea_bdg = snakemake.output.path_sea_bdg
# path_drain_bdg = snakemake.output.path_drain_bdg
# path_west_bdg = snakemake.output.path_west_bdg
# # budget_xlsx = snakemake.output.budget_xlsx


### Withput Snakemake
## Parameters
modelname = "ashley29"
## Paths
# Input
path_area = "c:/Users/schadt/training/ashley/data/1-external/study_area/study_area_exact.shp"
bdg_nc = f"c:/Users/schadt/training/ashley/data/4-output/{modelname}/bdg.nc"
# Output
sankey_png = f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/waterbalance/sankey_pres_with_storage.png"
path_river_bdg = f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/waterbalance/river_bdg_pres.png"
path_sea_bdg = f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/waterbalance/sea_bdg_pres.png"
path_drain_bdg = f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/waterbalance/drain_bdg_pres.png"
path_west_bdg = f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/waterbalance/west_bdg_pres.png"
budget_xlsx = r"C:\Users\schadt\training\ashley\data\5-visualization\water_balance.xlsx"

#%% Function
def split_bdg(ds, key):
    """Split budgets in a budget in and a budget out"""
    ds[f"{key}_out"] = ds[f"{key}"].where(ds[f"{key}"] < 0)
    ds[f"{key}_in"] = ds[f"{key}"].where(ds[f"{key}"] > 0)
    ds = ds.drop(f"{key}")
    return ds

#%% General code

## Open data
ds = xr.open_dataset(bdg_nc)
ds = ds.isel(time=-1)  # for last timestep
# ds.bdgghb[40,:,:].plot.imshow()

# Split ghb budget in western boundary and sea
ds["bdgghb_west"] = ds["bdgghb"][:,:,0:1]
ds["bdgghb_sea"] = ds["bdgghb"][:,:,2:len(ds["bdgghb"].x)]
#ds = ds.drop_vars("bdgsto") # neglect storage

# SMALL_SIZE = 12 #8
# MEDIUM_SIZE = 14 #10
# BIGGER_SIZE = 0

SMALL_SIZE = 16 #12 #8
MEDIUM_SIZE = 18 #14 #10
BIGGER_SIZE = 20 #16

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

#%% Plot map rivers

study_area = gpd.read_file(path_area)
study_area = study_area.to_crs("EPSG:3857")

river_bdg = ds["bdgriv"]
river_bdg_2d = river_bdg.sum(dim="layer")
river_bdg_2d.plot.imshow()

# reproject to CRS of map
river_bdg_2d = river_bdg_2d.rio.write_crs("EPSG:2193")
river_bdg_2d= river_bdg_2d.rio.reproject("EPSG:3857") 
river_bdg_2d = xr.where(river_bdg_2d > 10000000, 0, river_bdg_2d)
river_bdg_2d = xr.where(river_bdg_2d < 0, -1, river_bdg_2d)
river_bdg_2d = xr.where(river_bdg_2d > 0, 1, river_bdg_2d)
river_bdg_2d = river_bdg_2d.where(river_bdg_2d !=0)

print("plotting waterbalance of rivers...")
plt.axis("scaled")
fig, ax = plt.subplots(figsize=(15, 10))
cmap = (mpl.colors.ListedColormap(['blue', 'red']))
river_bdg_2d.plot(x="x", y="y", cmap=cmap, add_colorbar=False) 
ctx.add_basemap(ax,  source=ctx.providers.Esri.WorldTopoMap)
study_area.plot(edgecolor="black", color="none", ax=ax)
red_patch = mpatches.Patch(color='red', label='Losing stream')
blue_patch = mpatches.Patch(color='blue', label='Gaining stream')
plt.rcParams["legend.fontsize"] = 18
plt.legend(handles=[red_patch, blue_patch], loc="lower right")
ax.set_title("")
ax.set_ylabel("[m EPSG:3857]")
ax.set_xlabel("[m EPSG:3857]")
fig.tight_layout()
    # save fig
fig.savefig(
        os.path.join(path_river_bdg),
        bbox_inches='tight',
        dpi=300,
        )  
    
#%% Plot map sea
sea_bdg = ds["bdgghb_sea"]
sea_bdg_2d = sea_bdg.sum(dim="layer")
sea_bdg_2d.plot.imshow()
# reproject to CRS of map
sea_bdg_2d = sea_bdg_2d.rio.write_crs("EPSG:2193")
sea_bdg_2d= sea_bdg_2d.rio.reproject("EPSG:3857") 
sea_bdg_2d = xr.where(sea_bdg_2d > 10000000, 0, sea_bdg_2d)
sea_bdg_2d = sea_bdg_2d.where(sea_bdg_2d !=0)
print("plotting waterbalance of sea...")
plt.axis("scaled")
fig, ax = plt.subplots(figsize=(15, 10))
sea_bdg_2d = sea_bdg_2d.rename("flow [m$^3$/d]")
sea_bdg_2d.plot(x="x", y="y",  add_colorbar=True) 
ctx.add_basemap(ax,  source=ctx.providers.Esri.WorldTopoMap)
study_area.plot(edgecolor="black", color="none", ax=ax)
ax.set_title("")
ax.set_ylabel("[m EPSG:3857]")
ax.set_xlabel("[m EPSG:3857]")
fig.tight_layout()
    # save fig
fig.savefig(
        os.path.join(path_sea_bdg),
        bbox_inches='tight',
        dpi=300,
        )  

#%% Plot map drain
drain_bdg = ds["bdgdrn"]
drain_bdg_2d = drain_bdg.sum(dim="layer")
drain_bdg_2d.plot.imshow()
# reproject to CRS of map
drain_bdg_2d = drain_bdg_2d.rio.write_crs("EPSG:2193")
drain_bdg_2d= drain_bdg_2d.rio.reproject("EPSG:3857") 
drain_bdg_2d = xr.where(drain_bdg_2d > 10000000, 0, drain_bdg_2d)
drain_bdg_2d = drain_bdg_2d.where(drain_bdg_2d !=0)
drain_bdg_2d_abs = abs(drain_bdg_2d)
print("plotting waterbalance of drains...")
plt.axis("scaled")
fig, ax = plt.subplots(figsize=(15, 10))
drain_bdg_2d_abs = drain_bdg_2d_abs.rename("drain out [m$^3$/d]")
drain_bdg_2d_abs.plot(x="x", y="y",  add_colorbar=True) 
ctx.add_basemap(ax,  source=ctx.providers.Esri.WorldTopoMap)
study_area.plot(edgecolor="black", color="none", ax=ax)
ax.set_title("")
ax.set_ylabel("[m EPSG:3857]")
ax.set_xlabel("[m EPSG:3857]")
fig.tight_layout()
    # save fig
fig.savefig(
        os.path.join(path_drain_bdg),
        bbox_inches='tight',
        dpi=300,
        )  

#%% Plot map ghb west
west_bdg = ds["bdgghb_west"]
west_bdg_2d = west_bdg.sum(dim="layer")
west_bdg_2d.plot.imshow()
# reproject to CRS of map
west_bdg_2d = west_bdg_2d.rio.write_crs("EPSG:2193")
west_bdg_2d= west_bdg_2d.rio.reproject("EPSG:3857") 
west_bdg_2d = xr.where(west_bdg_2d > 10000000, 0, west_bdg_2d)
west_bdg_2d = west_bdg_2d.where(west_bdg_2d !=0)
print("plotting waterbalance of western ghb...")
plt.axis("scaled")
fig, ax = plt.subplots(figsize=(15, 10))
west_bdg_2d = west_bdg_2d.rename("flow [m$^3$/d]")
west_bdg_2d.plot(x="x", y="y",  add_colorbar=True) 
ctx.add_basemap(ax,  source=ctx.providers.Esri.WorldTopoMap)
study_area.plot(edgecolor="black", color="none", ax=ax)
ax.set_title("")
ax.set_ylabel("[m EPSG:3857]")
ax.set_xlabel("[m EPSG:3857]")
fig.tight_layout()
    # save fig
fig.savefig(
        os.path.join(path_west_bdg),
        bbox_inches='tight',
        dpi=300,
        )  


#%% Water balance
# ds["bdgghb_sea"][40,:,:].plot.imshow()

SMALL_SIZE = 10 #8
MEDIUM_SIZE = 12 #10
BIGGER_SIZE = 12

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title





ds = ds.drop_vars("bdgghb")

# Split budgets in positive and negative flux
for pkg in ["bdgriv",  "bdgghb_west",  "bdgghb_sea", "bdgbnd", "bdgsto"]:  #"bdgghb",
    if pkg in ds.keys():
        ds = split_bdg(ds, pkg)
        
# For some versions of imod-python spatial ref needs to be removed
if "spatial_ref" in ds.keys():
    ds = ds.drop("spatial_ref")

# Prepare for Sankey
d_labels = {
    "riv_in": "river in",
    "riv_out": "river out",
    "ghb_in": "western boundary/sea in",
    "ghb_out": "western boundary/sea out",
    "ghb_west_in": "western boundary in",
    "ghb_west_out": "western boundary out",
    "ghb_sea_in": "sea in",
    "ghb_sea_out": "sea out",
    "bnd_in": "constant head in",
    "bnd_out": "constant head out",
    "sto_in": "storage in",
    "sto_out": "storage out",
    "drn": "drain out",
    "rch": "recharge",
}

ds_sum = ds.sum()
d_bdg = ds_sum.to_dict()["data_vars"]
labels = [d_labels[key.split("bdg")[1]] for key in ds.keys()]
flows = [d_bdg[key]["data"] for key in ds.keys()]
labels, flows = zip(
    *[(label, flow) for label, flow in zip(labels, flows) if not np.isclose(flow, 0.0)]
)

if len(flows) == 6:
    orientations = [1, 1, 1, -1, -1, -1]
if len(flows) == 7:
    orientations = [1, 1, -1, 1, -1, -1, -1]
if len(flows) == 8:
    orientations = [1, 1, -1, 1, -1, -1, -1, 1]
if len(flows) == 9:
    orientations = [1, 1, -1, 1, -1, -1, -1, 1, 1]
if len(flows) == 10:
    orientations = [1, 1, -1, 1, -1, -1, -1, 1, 1, 1]
if len(flows) == 11:
    orientations = [1, 1, -1, 1, -1, -1, -1, 1, 1, 1, 1]
if len(flows) == 12:
    orientations = [1, 1, -1, 1, -1, -1, -1, 1, 1, 1, 1, 1]
    
# Plot Sankey
#plt.rcParams.update({"font.size": 14})

fig, ax = plt.subplots(figsize=(10, 8))

sankey = Sankey(
    ax=ax,
    flows=flows,
    orientations=orientations,
    labels=labels,
    scale=1 / max(flows) * 0.4,
    unit=r" $m^3/d$",
    pathlengths=0.3,
    offset=0.1, #0.25,
    format="%.1e",
).finish()
plt.axis("off")

plt.tight_layout()

plt.savefig(sankey_png, bbox_inches='tight', dpi=300)


#%% Save excel file


# # df = pd.DataFrame([flows], index=[f'{modelname}'], columns = [labels])
# df = pd.DataFrame([labels, flows], index=['Flow_m3/d', f'{modelname}']) #first one
# # df = pd.DataFrame([flows], index=[f'{modelname}']) # others
# # writer = pd.ExcelWriter(budget_xlsx, engine=None, date_format=None, datetime_format=None, mode='a', storage_options=None, if_sheet_exists='overlay', engine_kwargs=None)
# # df.to_excel(writer, sheet_name='Sheet1')

# wb = load_workbook(budget_xlsx)
# ws = wb['Sheet1']

# for r in dataframe_to_rows(df, index=True, header=False):  #index true
#     ws.append(r)

# wb.save(budget_xlsx)
