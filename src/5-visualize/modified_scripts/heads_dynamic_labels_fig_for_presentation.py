#%% Visualize heads

import pathlib
import geopandas as gpd
import imod
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import numpy as np
import pandas as pd
import contextily as ctx
import xarray as xr
import os

# ### Snakemake
# ## Parameters
# params = snakemake.params
# modelname = params["modelname"]
# path_head_visualization = params['path_head_visualization']
# ## Paths
# # Input
# path_modeltop = snakemake.input.path_modeltop
# path_area = snakemake.input.path_area
# path_heads = snakemake.input.path_head_nc
# # Output

### Without Snakemake
## Parameters
modelname = "ashley29"
## Paths
# Input
path_modeltop = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/modeltop.nc"
path_area = r"c:\Users\schadt\training\ashley\data\1-external\study_area\study_area_exact.shp"
path_heads = f"c:/Users/schadt/training/ashley/data/4-output/{modelname}/head.nc"
# Output
path_head_visualization = f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/head/"

#%% General code
#Open data
# Open modeltop
top = xr.open_dataset(path_modeltop)["modeltop"]
# Get area
area = gpd.read_file(path_area)
area_raster = imod.prepare.spatial.rasterize(area, like=top)

# fig, ax = plt.subplots()
# area_raster.plot.imshow()
# fig.savefig(
#                 f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/conc_crossections/area_raster.png",
#                 dpi=300,
#             )

# Open overlay
overlays = [{"gdf": area, "edgecolor": "black", "color": "none"}]

# Set levels
levels_depth = [
    0.0,
    0.25,
    0.5,
    0.75,
    1.0,
    1.25,
    1.5,
    1.75,
    2,
    2.25,
    2.5,
    3,
    4,
    5.0,
    7.5,
    10,
    15,
    20,
    30,
]

# # Set levels
# levels_depth = [
#     -7.0,
#     -6.0,
#     -5.0,
#     -4.0,
#     -3.0,
#     -2.0,
#     -1.0,
#     0.0,
# ]
# levels_gws = np.linspace(-1, 54, 20 + 1)
# levels_gws = np.linspace(-1, 4, 20 + 1) original

levels_gws = [
    0,
    1,
    2,
    4,
    6,
    8,
    10,
    12,
    14,
    16,
    18,
    20,
    22,
    24,
    26,
    28,
    30,
    32,
    34,
    36,
    38,
    40,
    42,
    44,
    46,
    48,
    50,
    52,
    54,
    56,
] 



textstr = "\n".join((r"Grey indicates no data",)) #do i need this

# Create folder to save to
# pathlib.Path(f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/head").mkdir(exist_ok=True, parents=True)

# pathlib.Path(f"data/5-visualization/{modelname}/head").mkdir(exist_ok=True, parents=True)

# Open heads
heads = xr.open_dataarray(path_heads)
# Select first layer
upper_active_layer = imod.select.upper_active_layer(heads, is_ibound=False)

# # only for test
# upper_active_layer2 = upper_active_layer.sel(time = '2000-01-02')
# upper_active_layer2.plot.imshow()

heads = heads.where(heads.layer == upper_active_layer).where(area_raster.notnull())

# heads = heads.sel(time = '2000-01-02')
# fig, ax = plt.subplots()
# #heads[1, :, :].plot.imshow()
# heads.plot.imshow()
# fig.savefig(
#                 f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/conc_crossections/heads_max.png",
#                 dpi=300,
#             )

heads = heads.max("layer")
#heads = heads.sel(time=slice("31-03-2011", "30-09-2021"))
#heads = heads.sel(time = '2000-01-02') #, drop = True)
heads = heads.rename("Groundwater level [m NZVD]").where(~top.isnull()).where(area_raster.notnull()) #do I need this
# heads.plot.imshow()
# heads.plot.imshow(vmin=-1, vmax=1)

depth = top - heads
#depth = depth.where(depth >= 0, 0.0).where(~top.isnull()) #do I need this
depth = depth.where(area_raster.notnull())
depth = depth.rename("gw depth (m)") #do I need this
# depth.plot.imshow()


SMALL_SIZE = 12 #8
MEDIUM_SIZE = 14 #10
BIGGER_SIZE = 16

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title



#%%  Plot groundater level
print("plotting groundwater level...")
for j in range(0,len(heads)):
 #heads.time:
    i = heads.time[j]
    plt.axis("scaled")
    fig, ax = imod.visualize.plot_map(
        heads.sel(time = i),
        colors="jet",
        levels=levels_gws,
        overlays=overlays,
        figsize=[15, 10],
        kwargs_colorbar={"label": "Groundwater level [m NZVD]"},
    )
    i = i.values
    # i = pd.to_datetime(i.values)
    # i = str(i.date())
    ax.set_facecolor("0.85")
    ax.set_title(f"Year: {pd.to_datetime(i).strftime('%Y')}", fontsize = 16)
    ax.set_ylabel("[m EPSG:2193]")
    ax.set_xlabel("[m EPSG:2193]")
    fig.tight_layout()
    fig.savefig(
        os.path.join(path_head_visualization, f"groundwater_level_{j}.png"),
        #f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/head/groundwater_level_{i}.png",
        #f"data/5-visualization/{modelname}/heads/{percent} groundwater level.png",
        bbox_inches='tight',
        dpi=300,
    )     

#%% Plot groundwater depth  
print("plotting depth to groundwater...")
for j in range(0,len(heads)):
 #heads.time:
    i = heads.time[j]
# for i in heads.time:
    plt.axis("scaled")
    fig, ax = imod.visualize.plot_map(
        depth.sel(time = i),
        colors="jet",
        levels=levels_depth,
        overlays=overlays,
        figsize=[15, 10],
        kwargs_colorbar={"label": "depth from modeltop [m] "},
    )
    i = i.values
    # i = pd.to_datetime(i.values)
    # i = str(i.date())
    ax.set_facecolor("0.85")
    ax.set_title(f"Year: {pd.to_datetime(i).strftime('%Y')}", fontsize = 16)
    ax.set_ylabel("[m EPSG:2193]")
    ax.set_xlabel("[m EPSG:2193]")
    fig.tight_layout()
    fig.savefig(
        os.path.join(path_head_visualization, f"groundwater_depth_{j}.png"),
        #f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/head/groundwater_depth_{i}.png",
        #f"data/5-visualization/{modelname}/heads/{percent} groundwater level.png",
        bbox_inches='tight',
        dpi=300,
        )    
    

#%% Visualize heads as contours

# Changing the CRS to fit the backround map
heads = heads.rio.write_crs("EPSG:2193")
heads = heads.rio.reproject("EPSG:3857")

# Levels
# levels_gws = np.linspace(0.5, 50.5, 21)  #-1, 10, 20+1

# Plot contour lines
print("plotting groundwater contour lines...")
for j in range(0,len(heads)):
 #heads.time:
    i = heads.time[j]
# for i in heads.time:
    plt.axis("scaled")
    fig, ax = plt.subplots(figsize=(15, 10))
    test = heads.sel(time = i).plot.contour(x="x", y="y", levels = levels_gws, add_colorbar=False, add_labels=True, cmap = 'viridis') #, colors = 'Blues'darkblue
    ctx.add_basemap(ax,  source=ctx.providers.Esri.WorldTopoMap)
    #path_area = r"c:\Users\schadt\training\ashley\data\1-external\study_area\study_area_new.shp"
    study_area = gpd.read_file(path_area)
    study_area = study_area.to_crs("EPSG:3857")
    study_area.plot(edgecolor="black", color="none", ax=ax)
    i = i.values
    # i = pd.to_datetime(i.values)
    # i = str(i.date())
    # ax.set_title(f"Groundwater level {i}")
    ax.set_title(f"Year: {pd.to_datetime(i).strftime('%Y')}", fontsize = 16)
    ax.set_ylabel("[m EPSG:3857]")
    ax.set_xlabel("[m EPSG:3857]")
    fig.tight_layout()
    ## new
    fmt = {}
    strs = levels_gws
    #strs = ['first', 'second', 'third', 'fourth', 'fifth', 'sixth', 'seventh']
    for l, s in zip(test.levels, strs):
        fmt[l] = s
    # Label every other level using strings
    # ax.clabel(test, test.levels[::2], inline=True, fmt=fmt, fontsize=10)
    
    ax.clabel(test, test.levels[::1], inline=True, fmt=fmt, fontsize=11)
    
    # save fig
    fig.savefig(
        os.path.join(path_head_visualization, f"groundwater_contours_{j}_blue.png"),
        #f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/head/groundwater_contours_{i}.png",
        bbox_inches='tight',
        dpi=300,
        )  