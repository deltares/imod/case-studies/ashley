#%% Cross section 

import pathlib
import geopandas as gpd
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import xarray as xr
import imod
import os
import shapely.geometry as sg

### Snakemake
## Parameter
params = snakemake.params
modelname = params["modelname"]
path_cross_section = params["path_cross_section"]
number_cross_sections = params["number_cross_sections"]
sea_level = params["sea_level"]
## Paths
# Input
path_template = snakemake.input.path_template
path_template_2d = snakemake.input.path_template_2d
path_formation_depth = snakemake.input.path_formation_depth
path_formation_top = snakemake.input.path_formation_top
path_water_mask = snakemake.input.path_water_masks
path_modeltop = snakemake.input.path_modeltop
path_area = snakemake.input.path_area
path_conc_nc = snakemake.input.path_conc_nc
# Output
path_cross_section_map1 = snakemake.output.path_cross_section_map1
path_cross_section_map2 = snakemake.output.path_cross_section_map2

# ### Without Snakemake
# ## Parameter
# modelname = "ashley27"
# number_cross_sections = 6 # put in snakemake
# sea_level = 0
# ## Paths
# # Input
# path_conc_nc = f"c:/Users/schadt/training/ashley/data/4-output/{modelname}/conc.nc"
# path_area = f"c:/Users/schadt/training/ashley/data/1-external/study_area/study_area_exact.shp"  
# path_template = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/template.nc"
# path_template_2d = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/template_2d.nc"
# path_formation_depth = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/formation_depth.nc"
# path_formation_top = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/formation_top.nc"
# path_water_mask = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/water_masks.nc"
# path_modeltop = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/modeltop.nc"
# # Output
# path_cross_section_map1 = f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/conc_cross_sections/map1.png"
# path_cross_section_map2 = f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/conc_cross_sections/map2.png"
# path_cross_section = f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/conc_cross_sections/"

#%% General code

# Open data
# Open templates
like = xr.open_dataset(path_template)["template"]
like = like.swap_dims({"z": "layer"})
like_2d = xr.open_dataset(path_template_2d)["template"]
dx, xmin, xmax, dy, ymin, ymax = imod.util.spatial_reference(like)
zmin = like.zbot.min()
zmax = like.ztop.max()
dz = like.dz
# Open formation layers and water mask
f_depth = xr.open_dataarray(path_formation_depth) #.rename({"geo_layer": "layer"})  #.swap_coords({"geo_layer": "layer"})
f_top = xr.open_dataarray(path_formation_top) #.rename({"geo_layer": "layer"})
# Open modeltop
modeltop = xr.open_dataset(path_modeltop)["modeltop"]
study_area = gpd.read_file(path_area)

f_depth[2,:,:].plot.imshow()
f_top[0,:,:].plot.imshow()
f_top[1,:,:].plot.imshow()
diff = f_top[0,:,:] - f_depth[1,:,:]
diff.plot.imshow()

#f_depth = f_depth.rename(['x', 'y', 'layer'])
# water_mask_2d = water_mask = xr.open_dataset(path_water_mask)["is_sea2d"]
water_mask = xr.open_dataset(path_water_mask)["is_sea3d"].swap_dims({"z":"layer"})  
wm_upper = imod.select.upper_active_layer(water_mask, is_ibound=False)
bathy = wm_upper.where(wm_upper.isnull(), wm_upper["ztop"])
bathy = bathy.rename("bathymetry")
bathy = bathy.assign_coords({"layer": "bathymetry"})
bathy = bathy.expand_dims("layer")
formations = xr.concat([f_depth, bathy], dim="layer")
# Open raster data
conc = xr.open_dataset(path_conc_nc).rename({'conc': 'chloride'})
conc = conc.assign_coords(top=like["ztop"])
conc = conc.assign_coords(bottom=like["zbot"])

# just test
conc.chloride[conc.time =='2000-01-02T00:00:00.000000000']
conc.chloride[0,0,:,:].plot.imshow()

# fig, ax = plt.subplots()
# conc2.chloride[2,10,:,:].plot.imshow()
# fig.savefig(
#                 f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/conc_crossections/conc_nc.png",
#                 dpi=300,
#             )


# fig, ax = plt.subplots()
# heads[0,0,:,:].plot.imshow()
# fig.savefig(
#                 f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/conc_crossections/head.png",
#                 dpi=300,
#             )


_, xmin, xmax, _, ymin, ymax = imod.util.spatial_reference(conc)  


# # Open shapefile defining cross section  #probably not needed
# gdf_2 = gpd.read_file(path_cross_section_shape)
# linestrings_2 = [ls for ls in gdf_2.geometry]  #see if needed
# linenames_2 = [ls for ls in gdf_2.name]   #see if needed

#%%  Make cross sections

# Plot settings
SMALL_SIZE = 12 #8
MEDIUM_SIZE = 14 #10
BIGGER_SIZE = 16

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title


distance_lines = (ymax-ymin)/(number_cross_sections +1)

linestrings  = []
linenames = []
pre_dfs = []

for i in range(number_cross_sections):
    linestring = sg.LineString([(xmin, ymax - (i+1) * distance_lines), (xmax, ymax - (i+1) * distance_lines)])
    linename = f"W-E{i+1}"
    linestrings.append(linestring)
    linenames.append(linename)
    # Creating a single-row-DataFrame.
    this_df = pd.DataFrame({'geometry':[linestring],
                            'name':[linename]})
    # Appending this single-row-DataFrame to the `pre_dfs` list
    pre_dfs.append(this_df)
# Concatenating all the separate dataframes into one big DataFrame
single_df = pd.concat(pre_dfs, ignore_index=True).reset_index(drop=True)
# Finally, generating the actual GeoDataFrame that can be manipulated
gdf = gpd.GeoDataFrame(single_df,
                          geometry='geometry',
                          crs='epsg:2193')


## Plot map of lines with background modeltop  # where is the area plotted
plt.axis("scaled")
fig, ax = imod.visualize.plot_map(
    modeltop,
    colors="terrain",
    #levels=np.linspace(-10, 10, 20),
    levels=np.linspace(-10, 60), #put maximum elevation for second number
    figsize=[15, 10],
    kwargs_colorbar={"label": "gw depth (m)"},
)
# Plot cross section line
gdf.plot(column="name", legend=True, cmap="Paired", ax=ax)
# Plot settings
fig.delaxes(fig.axes[1]) # do I need this
ax.set_title("Location cross sections")
# Save figure
fig.savefig(
    path_cross_section_map1,
    bbox_inches='tight',
    dpi=300,
)   

## Plot map of lines with background map
# Converting coordinates epsg:2193 to epsg:3857 to make base map
gdf.crs = "EPSG:2193"
gdf_conv = gdf.to_crs("EPSG:3857") 
gdf_conv_sel = gdf_conv[0:4]
study_area = study_area.to_crs("EPSG:3857")

fig, ax = plt.subplots(figsize=(15, 10))
# Plot cross section line
gdf_conv_sel.plot(column="name", legend=True, cmap="Accent", ax=ax)  #paired
# Plot study area
study_area.plot(edgecolor="black", color="none", ax=ax)
# Plot basemap
import contextily as ctx
ctx.add_basemap(ax,  source=ctx.providers.Esri.WorldTopoMap)
# Plot settings
ax.set_title("")
ax.set_ylabel("[m EPSG:3857]")
ax.set_xlabel("[m EPSG:3857]")
fig.tight_layout()
# Save figure
fig.savefig(
    path_cross_section_map2,
    bbox_inches='tight',
    dpi=300,
)   

# %% Cross section plot

# Plot settings
SMALL_SIZE = 8 #8
MEDIUM_SIZE = 8 #10
BIGGER_SIZE = 12

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title


# Get geology in one dataset
all_combined = xr.Dataset()
# top_combined = f_top.rename({"geo_layer": "layer"}) 
# base_combined = f_depth.rename({"geo_layer": "layer"}) 
all_combined["top"] = f_top #top_combined
all_combined["bot"] = f_depth #base_combined

#formations = [fm for fm in all_combined.layer.values if fm.split("_")[0] =='Springston']  #check if really everything that is not gravel is aquitard . no other formations are also aquifers
formations = [fm for fm in all_combined.layer.values if fm.split("_")[0] =='Avonside' or fm.split("_")[0] =='Christchurch' or fm.split("_")[0] =='Bromley' or fm.split("_")[0] =='Heathcote' or fm.split("_")[0] =='Shirley'] 
# formations = [fm for fm in all_combined.layer.values if fm.split("_")[1] =='Gravels']
aquitards = all_combined.sel(layer=formations)
#aquitards = aquitards.dropna("layer", how="all")
is_aquitard = aquitards["top"].notnull()  #is not 0 everywhere

#like_max_top = like_max_top.where(like_max_top["zbot"] < modeltop)
#top_combined1 = top_combined.where(top_combined <modeltop)
is_aquitard = is_aquitard.assign_coords(top=aquitards["top"])
is_aquitard = is_aquitard.assign_coords(bottom=aquitards["bot"])
#is_aquitard = is_aquitard.rename({"formation": "layer"}) 

## Get sections
# Concentration

# find where sea starts
sections_sea = [
    imod.select.cross_section_linestring(water_mask, ls) for ls in linestrings
]

sea_boundary = []
for i in sections_sea:
    WE = np.where(np.any(i == 1, axis=0))[0]
    W = WE[0]
    W_dist = dx *W
    sea_boundary.append(W_dist)
    
      
# sections_sea[1].plot.imshow()
# WE = np.where(np.any(sections_sea[1] == 1, axis=0))[0]
# W = WE[0]
# W_dist = dx *W

# #double check if it is correct
# sections_sea_1 = sections_sea[1].rio.write_crs("EPSG:2193")
# plt.axis("scaled")
# fig, ax = plt.subplots(figsize=(15, 10))
# sections_sea_1.plot()
# ax.axvline(x=sea_boundary[1], color='k', linestyle='--')
# ax.set_title(f"West GHB budget")
# fig.tight_layout()

# sections_sea_3 = sections_sea[3].rio.write_crs("EPSG:2193")
# plt.axis("scaled")
# fig, ax = plt.subplots(figsize=(15, 10))
# sections_sea_3.plot()
# ax.axvline(x=sea_boundary[3], color='k', linestyle='--')
# ax.set_title(f"West GHB budget")
# fig.tight_layout()


sections = [
    imod.select.cross_section_linestring(conc, ls) for ls in linestrings
]
# Aquitard
aq_sections = [
    imod.select.cross_section_linestring(is_aquitard, ls).compute() for ls in linestrings
]


# Plot
for species in conc.data_vars:
    for i, (cross_section, aq) in enumerate(zip(sections, aq_sections)):
        print(linenames[i])
        cross_section = cross_section[species]
        cross_section = cross_section.where(cross_section != cross_section.min())

        cross_section = cross_section.where(~(cross_section < 0.0), 0.0)
        cross_section = cross_section.where(~(cross_section > 18.9), 18.9)

        cross_section = cross_section.compute()
        
        #cross_section = cross_section.rename[{"x": "s"}]

        # pathlib.Path(
        #     f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/conc_crossections/species{species}_{linenames[i]}"
        # ).mkdir(exist_ok=True, parents=True)
        
        pathlib.Path(
            os.path.join(path_cross_section, f"{species}_{linenames[i]}"),
            #f"data/5-visualization/{modelname}/conc_crossections/{species}_{linenames[i]}"
        ).mkdir(exist_ok=True, parents=True)
        
        # pathlib.Path(
        #     f"c:/Users/schadt/training/ashley/data/species{species}_{linenames[i]}"
        # ).mkdir(exist_ok=True, parents=True)
        
        fig, ax = plt.subplots()
        kwargs_aquitards = {"hatch": "/", "edgecolor": "k", "facecolor": "grey", "alpha": 0.3}
        #levels=np.array([0, 0.15, 0.5, 1, 2, 3, 5, 7.5, 10, 16, 19])
        levels=np.array([0, 0.15, 0.5, 1, 2, 3, 5, 7.5, 10, 13, 16, 19])
        cmap = "jet"

        for j, time in enumerate(cross_section.time.values):
            print(j, time)
            s = cross_section.sel(time=time)
            s = s.rename("chloride (g/L)")

            fig, ax = imod.visualize.cross_section(
                s,
                colors=cmap,
                levels=levels,
                kwargs_colorbar={
                    "label": "Chloride [kg/m$^3$]",
                    "whiten_triangles": False,
                },
            )
            imod.visualize.cross_sections._plot_aquitards(aq, ax, kwargs_aquitards)

            ax.set_ylim(bottom=zmin, top=zmax) #10 
            ax.axhline(y=sea_level, color='k', linestyle='--', linewidth=0.5)
            ax.annotate("Sea level",xy=(25000, sea_level+4), ha="right", fontsize=8)
            # ax.axvline(x=sea_boundary[i], color='k', linestyle='--')
            # ax.annotate("Land",xy=(sea_boundary[i] - dx, 50), ha="right", fontsize=15)
            # ax.annotate("Sea",xy=(sea_boundary[i] + dx, 50), ha="left", fontsize=15)
            # s_x = cross_section.coords["x"].values
            # ax.set_xticks(s_x)
            # ax.set_xticklabels(s_x)
            # ax.set_xlim(left=xmin, right=xmax) 

            # datestring = pd.to_datetime(time).strftime("%Y%m%d")
            q = int(pd.to_datetime(time).strftime('%Y'))-2000
            # title = (
            #     # f"Section {linenames[i]}, {time}"
            #     # f"Section {linenames[i]}, {pd.to_datetime(time).strftime('%Y-%m-%d')}" 
            #     f"Section {linenames[i]}, Simulation time: {q} years"#before
            # )
            title = (
                f"Simulation time: {q} yrs"
            )
            ax.set_title(title)
            ax.set_ylabel("Elevation [m NZVD]")
            ax.set_xlabel("Distance from western boundary [m]")
            fig.tight_layout()
            fig.savefig(
                #f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/conc_crossections/species{species}_{linenames[i]}/{j:03d}.png",
                os.path.join(path_cross_section, f"{species}_{linenames[i]}/{j:03d}.png"),
                bbox_inches='tight',
                dpi=300,
            # fig.savefig(
            #     f"c:/Users/schadt/training/ashley/data/species{species}_{linenames[i]}/{j:03d}_s.png",
            #     dpi=300,
            )
