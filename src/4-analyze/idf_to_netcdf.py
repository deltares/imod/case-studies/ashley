#%% IDF to netcdf

import imod
import xarray as xr
import rioxarray
from glob import glob
import os

### Snakemake
## Parameters
proj_epsg = snakemake.params.proj_epsg
## Paths
# Input
head_dir = snakemake.input.head_dir
conc_dir = snakemake.input.conc_dir
bdg_dirs = snakemake.input.bdg_dirs
# Output
head_nc  = snakemake.output.head_nc
conc_nc  = snakemake.output.conc_nc
bdg_nc   = snakemake.output.bdg_nc

# ### Without Snakemake
# ## Parameters
# modelname = "ashley2"
# proj_epsg = "epsg:2193" 
# ## Paths
# # Input
# head_dir = f"c:/Users/schadt/training/ashley/data/4-output/{modelname}/head"
# conc_dir = f"c:/Users/schadt/training/ashley/data/4-output/{modelname}/conc"
# bdg_dirs = [f"c:/Users/schadt/training/ashley/data/4-output/{modelname}/bdgrch",  f"c:/Users/schadt/training/ashley/data/4-output/{modelname}/bdgghb", f"c:/Users/schadt/training/ashley/data/4-output/{modelname}/bdgriv"]
# # Output
# head_nc  = fr"c:\Users\schadt\training\ashley\data\4-output\{modelname}\head.nc"
# conc_nc  = fr"c:\Users\schadt\training\ashley\data\4-output\{modelname}\conc.nc"
# bdg_nc   = fr"c:\Users\schadt\training\ashley\data\4-output\{modelname}\bdg.nc"

### General Code

# Function
def idf_to_netcdf(globpath, nc_path, crs, nodata=1e30):
    """Convert idfs to netcdf, sets nodata sentinel value to np.nan

    Parameters
    ----------
    globpath : string
        Global path to idfs that should be read.
    nc_path : string
        Path to save netcdfs to.
    nodata : float, optional
        NoData value. The default is 1e30.

    Writes
    ------
    netcdf

    """
    da = imod.idf.open(globpath) #what is this for
    da = da.rio.write_crs(crs)
    da = da.where(da < nodata)
    da.to_netcdf(nc_path)

head_globpath = os.path.join(head_dir, "*.IDF")
conc_globpath = os.path.join(conc_dir, "*.IDF")
packages = [os.path.basename(bdg_dir) for bdg_dir in bdg_dirs]
# Convert states
idf_to_netcdf(head_globpath, head_nc, proj_epsg)
idf_to_netcdf(conc_globpath, conc_nc, proj_epsg)
# Convert budgets
bdg = xr.merge([imod.idf.open(os.path.join(bdg_dir, "*.idf")) for bdg_dir in bdg_dirs])
bdg = bdg.rio.write_crs(proj_epsg)
bdg.to_netcdf(bdg_nc)


