#%% Starting heads

import xarray as xr
import imod

### With Snakemake
## Parameters
params = snakemake.params
sea_level = params["sea_level"]
## Paths
# Input
path_modeltop = snakemake.input.path_modeltop
path_area = snakemake.input.path_area
path_template = snakemake.input.path_template
path_template_2d = snakemake.input.path_template_2d
# Output
path_starting_heads = snakemake.output.path_starting_heads

# ### Without Snakemake
# ## Parameters
# sea_level = 0.0
# modelname= "ashley_version85"
# ## Paths
# # Input
# path_template = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/template.nc"
# path_template_2d = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/template_2d.nc"
# path_modeltop = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/modeltop.nc"
# path_area = r"c:\Users\schadt\training\ashley\data\1-external\study_area\study_area_exact.shp"
# # Output
# path_starting_heads = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/starting_heads.nc"

#%% General code

## Open data
# Open templates
like = xr.open_dataset(path_template)["template"]
like_2d = xr.open_dataset(path_template_2d)["template"]
dx, xmin, xmax, dy, ymin, ymax = imod.util.spatial_reference(like)
zmin = like.zbot.min()
zmax = like.ztop.max()
dz = like.dz

# open modeltop
top=xr.open_dataset(path_modeltop)["modeltop"]

shd = top.where(top > sea_level, sea_level) # where modeltop is lower than sea level, assume sea level

# clip to ibound
import geopandas as gpd
gdf = gpd.read_file(path_area)
ibound_2d = imod.prepare.rasterize(gdf, like_2d, column=None)
shd = shd.where(ibound_2d == 1)  #.fillna(0)
shd.plot.imshow()

# fig, ax = plt.subplots()
# shd.plot.imshow()
# fig.savefig(
#                 f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/conc_crossections/sh.png",
#                 dpi=300,
#             )

# Save
shd.to_netcdf(path_starting_heads)