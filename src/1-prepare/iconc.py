#%% Initial concentration

import xarray as xr
import imod
import matplotlib.pyplot as plt

### Snakemake
## Parameters
params = snakemake.params
iconc = params["initial_conc"]
## Paths
# Input 
path_ibound = snakemake.input.path_ibound
# Output
path_chloride = snakemake.output.path_chloride

# ### Without Snakemake
# ## Parameters
# iconc = 19
# modelname = "ashley_version85"
# ## Paths
# # Input
# path_ibound = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/ibound.nc"
# # Output
# path_chloride = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/chloride.nc" 

#%% General code

# Set inital concentration, start with saltwater
ibound = xr.open_dataarray(path_ibound)
conc = ibound.where(ibound.isnull(), iconc)
conc = conc.where(ibound == 1) 
 
#conc[11,:,:].plot.imshow()


# fig, ax = plt.subplots()
# conc[6,:,:].plot.imshow()
# fig.savefig(
#                 f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/conc_crossections/conc.png",
#                 dpi=300,
#             )

# fig, ax = plt.subplots()
# conc2[6,:,:].plot.imshow()
# fig.savefig(
#                 f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/conc_crossections/conc2.png",
#                 dpi=300,
#             )

# Save
conc.to_netcdf(path_chloride)