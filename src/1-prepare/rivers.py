#%% Rivers

import geopandas as gpd
import imod
import xarray as xr
import pandas as pd
import numpy as np
import flopy
from gridit import Grid
import swn

### Snakemake
## Parameters
params = snakemake.params
modelname = params["modelname"]
waterway_resistance = params ["waterway_resistance"]
## Paths
# Input
path_waterways = snakemake.input.path_waterways
path_watersheds = snakemake.input.path_watersheds
path_area = snakemake.input.path_area  # not needed?
path_area_boundary = snakemake.input.path_area_boundary
path_modeltop = snakemake.input.path_modeltop
path_template = snakemake.input.path_template
path_template_2d = snakemake.input.path_template_2d
path_ibound = snakemake.input.path_ibound
# Output
path_river = snakemake.output.path_river

# ### Without snakemake
# ## Parameters
# modelname= "ashley12"
# waterway_resistance = 1 #days
# ## Paths
# # Input
# path_waterways = r"c:\Users\schadt\training\ashley\data\1-external\rivers\cant_riv_dn3v5.shp"
# path_watersheds = r"C:\Users\schadt\training\ashley\data\1-external\watershed\cant_wsd_dn3v5.shp"
# path_area = r"c:\Users\schadt\training\ashley\data\1-external\study_area\study_area_exact.shp"
# path_area_boundary = r"C:\Users\schadt\training\ashley\data\1-external\study_area\study_area_outline.shp" # snakemake
# path_modeltop = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/modeltop.nc" # use modeltop for stage
# path_template = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/template.nc"
# path_template_2d = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/template_2d.nc"
# path_ibound = f"C:/Users/schadt/training/ashley/data/2-interim/{modelname}/ibound.nc"
# # Output 
# path_river = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/river.nc"

#%% General code

## Open data
# Open templates
like = xr.open_dataset(path_template)["template"]
like_2d = xr.open_dataset(path_template_2d)["template"]
dx, xmin, xmax, dy, ymin, ymax = imod.util.spatial_reference(like)
zmin = like.zbot.min()
zmax = like.ztop.max()
dz = like.dz
# # Open modeltop
# modeltop = xr.open_dataset(path_modeltop)["modeltop"]
# Open stages
waterways_stages = xr.open_dataset(path_modeltop)["modeltop_min"]

#%% Pre-processing
resolution = dx
# clip river to study area, make it a bit larger so that rivers at edge are not cut off
model_bbox = xmin, ymin, xmax, ymax
expanded_bbox = (
    model_bbox[0] - resolution,
    model_bbox[1] - resolution,
    model_bbox[2] + resolution,
    model_bbox[3] + resolution,
)

lines = gpd.read_file(
    path_waterways, bbox=expanded_bbox)\
    .set_index("nzseg_v3", verify_integrity=True)
poly = gpd.read_file(
    path_watersheds, bbox=expanded_bbox)\
    .set_index("nzseg_v3", verify_integrity=True)

# Only take full LineStrings (chop off MultiLineStrings)
lines = lines[lines.geom_type == "LineString"]
poly = poly.reindex(lines.index)

# Build surface water network
n = swn.SurfaceWaterNetwork.from_lines(lines.geometry, poly.geometry)

# Get attributes from NIWA data
n.segments["upelev"] = lines["upelev"]
n.segments["downelev"] = lines["downelev"]
n.segments["stream_order"] = lines["sorder"]
n.segments["upstream_area"] = lines["cumarea"]
n.estimate_width()

# n.remove(n.segments.stream_order <= 2)
# OR
# remove smaller rivers
n.remove(n.segments.upstream_area < 5e6)

n.plot()

grid = Grid.from_bbox(*model_bbox, resolution=resolution, projection="EPSG:2193")

nlay = np.shape(like)[0]
nrow, ncol = grid.shape
nper = 1
xul, yul = grid.top_left

# get line length per cell
m = flopy.modflow.Modflow()
_ = flopy.modflow.ModflowDis(
    m, nlay=nlay, nrow=nrow, ncol=ncol, nper=nper,
    delr=resolution, delc=resolution,
    xul=xul, yul=yul)
_ = flopy.modflow.ModflowBas(m, stoper=5.0)

nm = swn.SwnModflow.from_swn_flopy(n, m)

# Copy over helpful things
nm.set_reach_data_from_segments("width", n.segments.width)
nm.set_reach_data_from_segments("elev", n.segments.upelev, n.segments.downelev)

# Look in QGIS, not needed
# swn.file.gdf_to_shapefile(nm.reaches, f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/nm_reaches.shp")
# swn.file.gdf_to_shapefile(nm.grid_cells, f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/grid_cells.shp")
# swn.file.gdf_to_shapefile(n.segments, f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/n_segments.shp")


#%% 

waterways = nm.reaches
waterways["is_water"] = 1.0
waterways["ID"] = waterways.index

# ## using waterways
# waterways ["area_cal"] = waterways["rchlen"] * waterways ["width"]
# waterways ["area_cal"].max() 
# waterways ["area_cal"].min() 

# what is row and what column ?
waterways ["row_index"] = waterways["i"]
waterways ["col_index"] = waterways["j"]

# do I need the celltable or can I use waterways directly
# to_rasterize = waterways.sort_values("area_cal", ascending=False).drop_duplicates(
#     ["row_index", "col_index"]
# )
to_rasterize = waterways.sort_values("rchlen", ascending=False).drop_duplicates(
    ["row_index", "col_index"]
)

# 1293 before, then 992 because duplicates dropped

# Create rasters from the table
is_river = imod.prepare.rasterize_celltable(
    to_rasterize, column="is_water", like=like_2d
)

is_river.plot.imshow()

# Compute conductance
length = waterways.groupby(["row_index", "col_index"])["rchlen"].sum()
tmp_df = pd.DataFrame()
tmp_df["length"] = length.values
tmp_df["row_index"] = length.index.get_level_values("row_index")
tmp_df["col_index"] = length.index.get_level_values("col_index")
to_rasterize = pd.merge(to_rasterize, tmp_df, on=["row_index", "col_index"])
assert (to_rasterize["length"] >= to_rasterize["rchlen"]).all()
w = 3 # width of river or ditch (use 1.5 m for ditches and 3.0 m for streams)
h = 0.75 #0.5 # water height (use 0.5 m)
r = waterway_resistance
# teest with same conductance independent from length
to_rasterize["conductance"] = (to_rasterize["length"] * (w + 2 * h) / r) # 0.5 #muss weg
# to_rasterize["conductance"] = 100
conductance = imod.prepare.spatial.rasterize_celltable(
    to_rasterize, column="conductance", like=like_2d
)

conductance.plot.imshow()
conductance.max()
# conductance[45-1,40-1] = 1500
# conductance[47-1, 33-1] = 1000 # try if this solves converging issue


# reduce the conductance for the river at the borders because they impact the model area and outside

# rasterize area
celltable = imod.prepare.spatial.celltable(   #computes area
    path_area_boundary, column="ID", resolution=0.5, like=like_2d #check california for resolution
)  #resolution 0.5 originally

# Take the values with the highest area
to_rasterize = celltable.sort_values("area", ascending=False).drop_duplicates(  # waterway data
    ["row_index", "col_index"]
)
# Create rasters from the table
is_boundary = imod.prepare.spatial.rasterize_celltable(
    to_rasterize, column="ID", like=like_2d
)

is_boundary.plot.imshow()


is_boundary_north = is_boundary.where(is_boundary.y>5207250)
is_boundary_north[:,0] = np.NaN
is_boundary_north[:,-1] = np.NaN
is_boundary_north.plot.imshow()
is_boundary_south = is_boundary.where(is_boundary.y<5196000)
is_boundary_south[:,0] = np.NaN
is_boundary_south[:,-1] = np.NaN
is_boundary_south.plot.imshow()

conductance = xr.where(is_boundary_north==1, conductance*3, conductance)  #10000conductance*50.5 instead of 1  #
conductance = xr.where(is_boundary_south==1, conductance*2, conductance) #10000 as before
conductance.plot.imshow()
conductance.max()






stage = waterways_stages.where(is_river.notnull())
stage.plot.imshow()
bottom = stage - h # should be h?
bottom.plot.imshow()

# Create 3D arrays.
riv_bot = xr.full_like(like, 1.0) * bottom
riv_stage = xr.full_like(like, 1.0) * stage
riv_cond = xr.full_like(like, 1.0) * conductance
riv_cond[1,:,:].plot.imshow()
riv_stage[1,:,:].plot.imshow()
riv_bot[0,:,:].plot.imshow()  # is complete until here

riv_bot = riv_bot.where((riv_bot <= riv_bot["ztop"]) & (riv_bot > riv_bot["zbot"])) # or instead of and # =< changed (before <), might have to change in drains too
# riv_bot = riv_bot.where((riv_bot <= riv_bot["ztop"])) 

# # terschelling
# riv_bot = riv_bot.where((riv_bot < riv_bot["ztop"]) & (riv_bot >= riv_bot["zbot"]))

# sel1 = (riv_bot <= riv_bot["ztop"])
# sel2 =  (riv_bot > riv_bot["zbot"])
# selall = sel1 & sel2
# sel1[0,:,:]
# riv_bot_2[0,:,:].plot.imshow() 
# riv_cond[:,:,20].plot.imshow()
# riv_cond[20,:,:].plot.imshow()

is_riv = xr.full_like(like, 1.0).where(
    ~np.isnan(riv_stage) & ~np.isnan(riv_cond) & ~np.isnan(riv_bot)
)

is_riv.notnull().any(axis=0).plot.imshow()


ibound = xr.open_dataarray(path_ibound)
is_riv = is_riv.where(ibound == 1)   # issue in layer 17 (index 16)

is_riv_test = is_riv.where(is_riv.isnull() & riv_bot.notnull())
# is_riv_test[16,:,:].plot.imshow()


riv_bot = riv_bot.where(~np.isnan(is_riv))
riv_stage = riv_stage.where(~np.isnan(is_riv))
riv_cond = riv_cond.where(~np.isnan(is_riv))

riv_density = xr.full_like(like, 1000.0)
riv_density = riv_density.where(~np.isnan(is_riv))

assert (
    riv_cond.count()
    == riv_stage.count()
    == riv_density.count()
    == riv_bot.count()
)

# riv_density[30,:,:].plot.imshow()
# riv_cond[30,:,:].plot.imshow()

# Save
ds = xr.Dataset()
ds["stage"] = riv_stage
ds["cond"] = riv_cond
ds["bot"] = riv_bot
ds["density"] = riv_density
ds.to_netcdf(path_river)



