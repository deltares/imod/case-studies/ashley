#%% Water mask

import xarray as xr
import imod
import numpy as np

### With Snakemake
## Parameters
params = snakemake.params
sea_level = params["sea_level"]
## Paths
# Input
path_modeltop = snakemake.input.path_modeltop
path_template = snakemake.input.path_template
path_template_2d = snakemake.input.path_template_2d
# Output 
path_water_mask = snakemake.output.path_water_masks

# ### Without Snakemake
# ## Parameters
# sea_level = 0.0
# modelname = "ashley_version59"
# ## Paths
# # Input
# path_modeltop = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/modeltop.nc"
# path_template = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/template.nc"
# path_template_2d = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/template_2d.nc"
# # Output 
# path_water_mask = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/water_mask.nc"

#%% General code

## Open data
# Open templates
like = xr.open_dataset(path_template)["template"]
like_2d = xr.open_dataset(path_template_2d)["template"]
dx, xmin, xmax, dy, ymin, ymax = imod.util.spatial_reference(like)
zmin = like.zbot.min()
zmax = like.ztop.max()
dz = like.dz
# Open modeltop
top = xr.open_dataset(path_modeltop)["modeltop"]

# Create watermask
is_sea = top.where(top < sea_level) #nan where sea level is below modeltop, value of top where top is below sea level
# neglect rivers by finding furthest point west in the middle and neglecting everything more west
middle = round(is_sea.shape[0]/2)
middle_vector = is_sea[middle-1:middle+1,:]
WE = np.where(np.any(middle_vector < 100, axis=0))[0]
W = WE[0]
is_sea = is_sea[:,W:len(like_2d[0])+3]
is_sea = is_sea.combine_first(like_2d)

is_sea = is_sea.where(is_sea.isnull(), 1) #where is_sea has a value (because sea level above modeltop), value is replaced by 1
is_sea.name = "is_sea" #variable 1 indicates that there is sea

# to test
is_sea.plot.imshow()

# Creating 3D water mask
is_sea3d = like.where(like.ztop <= top, 1)  # where the top elevation of the layer in the template is above the top, value 1
is_sea3d = is_sea3d.where(is_sea3d.ztop <= sea_level) #only where the top elevation of the layer is below sea level, 1 is kept
is_sea3d.name = "is_sea3d" #variable 1 indicates that there is sea

#to test
is_sea3d.sel(z=-10, method="nearest").plot.imshow()
is_sea3d.sel(z=-30, method="nearest").plot.imshow()

#Create datasets and save
ds = xr.Dataset()
ds["is_sea2d"] = is_sea
ds["is_sea3d"] = is_sea3d
ds.to_netcdf(path_water_mask)