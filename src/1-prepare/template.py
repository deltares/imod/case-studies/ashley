#%% Template

import geopandas as gpd
import imod
import numpy as np
import xarray as xr

### With Snakemake
## Parameters
params = snakemake.params
cellsize = params["cellsize"]
zmin = params["zmin"]
zmax = params["zmax"]
dz = params["z_discretization"]
## Paths
# Input
path_area = snakemake.input.path_area
# Output
path_template = snakemake.output.path_template
path_template_2d = snakemake.output.path_template_2d

# ### Without Snakemake
# ## Parameters
# modelname = "ashley28"
# cellsize = 250
# zmin = -100
# zmax = 60
# dz = np.array([-10] * 16)
# ## Paths
# # Input
# path_area = r"c:\Users\schadt\training\ashley\data\1-external\study_area\study_area_exact.shp"
# # Output
# path_template = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/template.nc"
# path_template_2d = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/template_2d.nc"

#%%  Functions

# function for getting the extent of the model based on the study area (shp or tif)
def get_extent(path_area, cellsize):
    if ".shp" in path_area:    
        gdf = gpd.read_file(path_area)
        gdf = gdf.loc[gdf.id == 1]  # area must have id 1
        extent = imod.prepare.spatial.round_extent(gdf.bounds.values[0], cellsize)
    elif ".tif" in path_area:
        dem = imod.rasterio.open(path_area)
        dx, xmin, xmax, dy, ymin, ymax = imod.util.spatial_reference(dem)
        extent = xmin + 0.5 * abs(dx), ymin + 0.5 * abs(dy), xmax - 0.5 * abs(dx), ymax - 0.5 * abs(dy)
    else:
        raise FileNotFoundError("No shapefile or DEM found to derive extent from") 
    return extent

#%%  General code
# Open data
extent = get_extent(path_area, cellsize)
xmin, ymin, xmax, ymax = extent
zmin = zmin
zmax = zmax

# Discretization (cells are squares, dx = dy)
cellsize = float(cellsize) 
dx = cellsize
dy = -cellsize
dz = dz
layer = np.arange(1, 1 + dz.size)

# Define template
dims = ("z", "y", "x")
coords = {
    "z": zmax + dz.cumsum() - 0.5 * dz,  # height in the middle of layer
    "y": np.arange(ymax, ymin, dy) + 0.5 * dy, # y coordinate in the middle of cell
    "x": np.arange(xmin, xmax, dx) + 0.5 * dx, # x coordinate in the middle of cell
    "dz": ("z", dz),
    "layer": ("z", layer),
    "zbot": ("z", zmax + dz.cumsum()), # bottom height of layer
    "ztop": ("z", zmax + dz.cumsum() - dz), # top height of layer
}

nrow = coords["y"].size
ncol = coords["x"].size
nlay = coords["z"].size
like = xr.DataArray(np.full((nlay, nrow, ncol), np.nan), coords, dims)
like_2d = like.isel(z=0).drop(["z", "layer", "dz", "zbot", "ztop"])

# Save
like.name = "template"
like_2d.name = "template"
like.to_netcdf(path_template)   
like_2d.to_netcdf(path_template_2d)