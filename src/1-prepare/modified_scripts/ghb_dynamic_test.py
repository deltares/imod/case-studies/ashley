#%% GHB for western boundary

import matplotlib.pyplot as plt
import numpy as np
import imod
import xarray as xr
import geopandas as gpd
import shapely.geometry as sg
import os
import matplotlib.patches as mpatches
import pandas as pd

# ### Snakemake
# ## Parameters
# params = snakemake.params
# modelname = params["modelname"]
# resistance_sea = params["resistance_ghb_sea"]
# slope_density_conc = params["slope_density_conc"]
# density_ref = params["density_ref"]
# conc_ghb_sea = params["conc_ghb_sea"]
# conc_ghb_west = params["conc_ghb_west"]
# sea_level = params["sea_level"]
# path_cross_section_ghb = params["path_cross_section_ghb"]
# number_cross_sections = params["number_cross_sections"]
# ## Paths
# # Input
# path_template = snakemake.input.path_template
# path_template_2d = snakemake.input.path_template_2d
# path_water_mask = snakemake.input.path_water_masks
# path_modeltop = snakemake.input.path_modeltop
# path_conductivity = snakemake.input.path_conductivity
# path_measured_heads = snakemake.input.path_measured_heads
# path_boundary = snakemake.input.path_boundary
# path_ibound = snakemake.input.path_ibound
# # Output
# path_ghb = snakemake.output.path_ghb


### Without Snakemake
## Parameters
modelname = "ashley29"
resistance_sea = 1.0
slope_density_conc = 1.25
density_ref = 1000.0
sea_level = -0.228
conc_ghb_west = 0 
conc_ghb_sea = 19 
number_cross_sections = 6 
## Paths
# Input
path_modeltop = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/modeltop.nc"
path_template = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/template.nc"
path_template_2d = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/template_2d.nc"
path_conductivity = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/conductivity.nc"
path_water_mask = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/water_masks.nc"
path_measured_heads = "c:/Users/schadt/training/ashley/data/1-external/ashley_ecan_gw_data/ashley_ecan_gwl_data.shp"
path_boundary = r"c:\Users\schadt\training\ashley\data\1-external\ghb\area.shp"
path_ibound = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/ibound.nc"
# Output
path_ghb = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/ghb.nc"
path_cross_section_ghb = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/"

#%% Function
def get_extent(path_area, cellsize):
    if ".shp" in path_area:    
        gdf = gpd.read_file(path_area)
        gdf = gdf.loc[gdf.id== 1]
        extent = imod.prepare.spatial.round_extent(gdf.bounds.values[0], cellsize)
    elif ".tif" in path_area:
        dem = imod.rasterio.open(path_area)
        dx, xmin, xmax, dy, ymin, ymax = imod.util.spatial_reference(dem)
        extent = xmin + 0.5 * abs(dx), ymin + 0.5 * abs(dy), xmax - 0.5 * abs(dx), ymax - 0.5 * abs(dy)
    else:
        raise FileNotFoundError("No shapefile or DEM found to derive extent from") 
    return extent

# %% General code

## Open data
# Open templates
like = xr.open_dataset(path_template)["template"]
like_2d = xr.open_dataset(path_template_2d)["template"]
dx, xmin, xmax, dy, ymin, ymax = imod.util.spatial_reference(like)
zmin = like.zbot.min()
zmax = like.ztop.max()
dz = like.dz

# %% Interpolate the measured heads
# source https://deltares.gitlab.io/imod/imod-python/examples/prepare/point_interpolation.html#sphx-glr-examples-prepare-point-interpolation-py

# Open measured heads
heads = gpd.read_file(path_measured_heads)
heads['heads'] = heads['mp_elevati'] + heads['mean_gwl'] -0.356 # for conversion NZVD2016 (New Zealand Vertical Datum) from LVD (Lyttelton Vertical Datum)

heads['bottom_bot'] = heads['mp_elevati'] -0.356 + heads['dist_mp_to']  - heads['bottom_bot'] # for conversion NZVD2016 
heads['top_topscr'] = heads['mp_elevati'] -0.356 + heads['dist_mp_to']  - heads['top_topscr'] # for conversion NZVD2016 

heads = heads.where(  
    (heads["bottom_bot"] > zmin.item()) & (heads["bottom_bot"] < zmax.item())   #wells filtered in z range of model depth
).dropna(axis=0, how="all") 

#remove heads with unreasonable values 
heads = heads.where(heads["heads"] < 150)  
heads = heads.where(heads["heads"] > -10)  

heads = heads[heads['heads'].notna()]
mean_heads = heads # in this case no mean because mean already in table
# Open area to be used for interpolation (different to model area since it contains all measurement points)
cellsize = dx # condition dx = dy
extent = get_extent(path_boundary, cellsize) # get area where to interpolate
xmin, ymin, xmax, ymax = extent
# Discretization
cellsize = float(cellsize) 
dx = cellsize
dy = -cellsize
grid = imod.util.empty_2d(dx, xmin, xmax, dy, ymin, ymax)
# Remove points outside the grid (should be none)
mean_heads['x'] = heads["geometry"].x
mean_heads['y'] = heads["geometry"].y
points_outside_grid = (
    (mean_heads["x"] < xmin)
    | (mean_heads["x"] > xmax)
    | (mean_heads["y"] < ymin)
    | (mean_heads["y"] > ymax)
)
mean_heads_in_grid = mean_heads.loc[~points_outside_grid]
mean_heads_in_grid.head(5)

# assign information to model grid
x = mean_heads_in_grid["x"]
y = mean_heads_in_grid["y"]

heads_grid = imod.select.points_set_values(
    grid,
    values=mean_heads_in_grid["head"].to_list(),
    x=x.to_list(),
    y=y.to_list(),
)
# # Plotting the points
# fig, ax = plt.subplots()
# heads_grid.plot.imshow(ax=ax)

# interpolate 
interpolated_heads = imod.prepare.laplace_interpolate(
    heads_grid, close=0.001, mxiter=150, iter1=100
)
# interpolated_heads.plot.imshow()
interpolated_heads.min()


##tests
modeltop = xr.open_dataset(path_modeltop)["modeltop"]
diff = interpolated_heads-modeltop
diff.plot.imshow()
modeltop.plot.imshow()
test = xr.where(diff<0, 1, 0)
test.plot.imshow()

diff_2 = modeltop-interpolated_heads
diff_2.plot.imshow()
test_2 = xr.where(diff_2<0, 1, 0)
test_2.plot.imshow()

# %% GHBs

## Open data, back to model extent
# Open templates
# like = xr.open_dataset(path_template)["template"]
# like_2d = xr.open_dataset(path_template_2d)["template"]
dx, xmin, xmax, dy, ymin, ymax = imod.util.spatial_reference(like)
zmin = like.zbot.min()
zmax = like.ztop.max()
dz = like.dz
# Open modeltop
modeltop = xr.open_dataset(path_modeltop)["modeltop"]

# Get stage (is equal to sea level or interpolated heads)
# dynamic increase of sea level



stage_ghb_sea = sea_level

stage_ghb_west = interpolated_heads

# Find upper active model layer
ibound = xr.open_dataarray(path_ibound).swap_dims({'z': 'layer'})
upper_active_layer = imod.select.upper_active_layer(ibound, is_ibound=True)
# find eastern active layer for ghb sea (vertical layer), modified upper_active_layer function
is_active = ibound.fillna(0) # fill NaN with 0 
is_active = is_active.where(is_active.x>1.575e+06) # otherwise it makes problems
is_active[20, :,:].plot.imshow() 
# get layer of easter active cell
da = is_active.x.isel(x=is_active.argmin(dim="x"))  
da = da.drop_vars("x")
da = da-dx
da.plot.imshow()
# skip where no active cells
# da_2 = da.where(is_active.sum(dim="x") > 0)     # do not remember what this means, test
# da_2.plot.imshow()        
easter_active_layer = da
# # assert upper_active_layer.count() == upper_active_layer2.count()
# upper_active_layer.plot.imshow()
# test = upper_active_layer.where(upper_active_layer==42)
# # test.plot.imshow()
# # ibound[40,:,:].plot.imshow()

# %% GHB sea
# keep only the locations in sea:
water_mask = xr.open_dataset(path_water_mask)['is_sea2d']
water_mask.plot.imshow()
upper_active_layer_sea = upper_active_layer.where(water_mask.notnull())
upper_active_layer_sea.plot.imshow()
easter_active_layer_sea = easter_active_layer.where(easter_active_layer.z < sea_level) # below sea level
easter_active_layer_sea = easter_active_layer_sea.where(easter_active_layer_sea.z>-50)  # remove wrong ghb parts in deep
easter_active_layer_sea.plot.imshow()

is_sea = modeltop.where(modeltop < sea_level) #nan where sea level is below modeltop, value of top where top is below sea level
# neglect rivers below sea level (close to coast) by finding furthest point west in the middle and neglecting everything more west
middle = round(is_sea.shape[0]/2)
middle_vector = is_sea[middle-1:middle+1,:]
WE = np.where(np.any(middle_vector < 100, axis=0))[0] #where middle_vector has a number
W = WE[0] # furthest west in index
Wx = like_2d.x[W]  #furthest west in coordinates 
easter_active_layer_sea = easter_active_layer_sea.where(easter_active_layer_sea >= Wx)

easter_active_layer_sea.plot.imshow()
# create the area where the GHB will be present
is_ghb_sea = xr.full_like(like, 1.).swap_dims({'z': 'layer'})
is_ghb_sea[20,:,:].plot.imshow()
is_ghb_sea_1 = is_ghb_sea.where(is_ghb_sea.layer == upper_active_layer_sea)
is_ghb_sea_2 = is_ghb_sea.where(is_ghb_sea.x == easter_active_layer_sea)  
is_ghb_sea_1 = is_ghb_sea_1.swap_dims({'layer': 'z'})
is_ghb_sea_2 = is_ghb_sea_2.swap_dims({'layer': 'z'})
# is_ghb_sea_3 before


# #test only upper active
# is_ghb_sea_4 = is_ghb_sea_1

is_ghb_sea_4 = is_ghb_sea_1.combine_first(is_ghb_sea_2)  #combine horizontal and vertical boundary
# # is_ghb_sea_5 = is_ghb_sea_2.combine_first(is_ghb_sea_1) 
# # is_ghb_sea_1[20,:,:].plot.imshow()
# # # is_ghb_sea_2[30,:,:].plot.imshow()
# # is_ghb_sea_2[:,40,:].plot.imshow()     
# # is_ghb_sea_1[:,40,:].plot.imshow()
# # is_ghb_sea_4[:,40,:].plot.imshow()


#%% add eastern part
eastern_boundary = like_2d.copy()
eastern_boundary[:,-1] = 1  # last column (easter border) 
eastern_boundary.plot.imshow()
is_ghb_sea_4 = is_ghb_sea_4.combine_first(eastern_boundary)

#%% delete eastern part
# eastern_boundary = like_2d.copy()
# eastern_boundary[:,-1] = 1  # last column (easter border) (maybe not necessary and included in easter active)
# eastern_boundary.plot.imshow()
# is_ghb_sea_4 = is_ghb_sea_4.where(np.isnan(eastern_boundary))
# # is_ghb_sea_4[30,:,:].plot.imshow()
#%%

# is_ghb_east = xr.full_like(like, 1.).where(eastern_boundary.notnull()).swap_dims({'z': 'layer'})
# #is_ghb_east = is_ghb_east.where(is_ghb_east.z < modeltop)
# is_ghb_east = is_ghb_east.where(ibound==1)
# # is_ghb_east[40,:,:].plot.imshow()
# is_ghb_sea_4 = is_ghb_sea_3.combine_first(is_ghb_east.swap_dims({'layer': 'z'}))
# is_ghb_sea_4[39,:,:].plot.imshow()

# For conductance, divide cell area by resistance
conductance_sea = abs(dx * dy) / resistance_sea*50 # 50 is calibration #* 100 # muss weg
ghb_cond_sea = xr.full_like(like, 1.0) * conductance_sea
# set ghb's
ghb_stage_sea_initial = xr.full_like(like, stage_ghb_sea)
# sea_level_rise = [0.05, 0.11, 0.19, 0.28, 0.39, 0.5, 0.64, 0.78, 0.94, 1.12] # update
sea_level_rise = [0, 0.05, 0.10, 0.17, 0.25, 0.35, 0.46, 0.58, 0.72, 0.87, 1.03] 
# sea_level_change = sea_level_rise + sea_level
coords = ["2005-01-01", "2010-01-01", "2020-01-01", "2030-01-01", "2040-01-01", "2050-01-01", "2060-01-01", "2070-01-01", "2080-01-01", "2090-01-01", "2100-01-01"]

#test
cf_dates = []
for date in coords:
    cf_dates.append(imod.wq.timeutil.to_datetime(date, use_cftime=False))
coords = cf_dates


sea_level_rise = xr.DataArray(
    #sea_level_change,
    sea_level_rise,
    coords=[('time', coords)])

ghb_stage_sea = ghb_stage_sea_initial + sea_level_rise

ghb_stage_sea[3,:,:,9].plot.imshow()
    
ghb_conc_sea = xr.full_like(like, conc_ghb_sea)
ghb_density_sea = ghb_conc_sea * slope_density_conc + density_ref
# making sure GHB is only present in the right layer
ghb_cond_sea = ghb_cond_sea.where(is_ghb_sea_4.notnull())
ghb_stage_sea = ghb_stage_sea.where(is_ghb_sea_4.notnull())
ghb_conc_sea =  ghb_conc_sea.where(is_ghb_sea_4.notnull())
ghb_density_sea = ghb_density_sea.where(is_ghb_sea_4.notnull())
# asserting to ensure ghb is present in the right cells

#test
#assert ghb_cond_sea.count() == ghb_stage_sea.count() == ghb_conc_sea.count() == ghb_density_sea.count()

# %% GHB western boundary
# keep only the locations at western boundary
western_boundary = like_2d.copy()
western_boundary[:,0:1] = 1
western_boundary.plot.imshow()

# get hydraulic conductivity
k = xr.open_dataset(path_conductivity)["kh"] 
is_ghb_west = k.where(western_boundary.notnull()) 
#is_ghb_west[15,:,:].plot.imshow()
# # put only in first ten layers
# is_ghb_west = is_ghb_west.where(is_ghb_west.layer <= (upper_active_layer+10))

#is_ghb_west = is_ghb_west.swap_dims({'layer': 'z'}) #needed if like_max_top

# # old code when ghb was in upper active layer
# upper_active_layer_west = upper_active_layer.where(western_boundary.notnull())
# upper_active_layer_west.plot.imshow()
# # create the area where the GHB will be present
# is_ghb_west = xr.full_like(like, 1.).swap_dims({'z': 'layer'})
# is_ghb_west = is_ghb_west.where(is_ghb_west.layer == upper_active_layer_west)
# is_ghb_west = is_ghb_west.swap_dims({'layer': 'z'})

# Increase distance of ghb from model boundary by moving interpolation grid 4 km (16 cells) to east
l = 16*cellsize
stage_ghb_west_shift = stage_ghb_west.copy()
stage_ghb_west_shift = stage_ghb_west_shift.assign_coords(x=(stage_ghb_west_shift.x + l)) 
stage_ghb_west.plot.imshow()
stage_ghb_west_shift.plot.imshow() 
# conductance = hydraulic conductivity * cross section area / distance of boundary from model cell
A = abs(dy * dz[0].values) # assuming all dz are the same, no different thickness of layers
conductance_west = k * A / l # k in m/d, A in m2, l in m  
#conductance_west[40,:,:].plot.imshow()

#%% decrease conductance with depth
# array = like[:,:,1]
# array[0,:] = 2
# array[-1,:] = 1
# array.plot.imshow()

# array = xr.concat(array, dim='help_dim')
# array.help_dim
# filled = array.interpolate_na(dim="help_dim")
# filled.plot.imshow()
# interpolated_surface = xr.concat(filled, dim='z')
# interpolated_surface.plot.imshow()
# test = conductance_west * interpolated_surface  # or just for aquifers ?
# test[40,:,:].plot.imshow()


#%% Calibration: decrease conductance to south
# array = like[1,:,:]
array = like_2d
array[0,:] = 2  # north
array[-1,:] = 1  # south
array.plot.imshow()
array = xr.concat(array, dim='help_dim')
array.help_dim
filled = array.interpolate_na(dim="help_dim")
filled.plot.imshow()
interpolated_surface = xr.concat(filled, dim='y')
interpolated_surface.plot.imshow()
conductance_west = conductance_west * interpolated_surface  # or just for aquifers ?
# test[40,:,:].plot.imshow()

#%%
ghb_cond_west = conductance_west # * 0.5 # should not be
# set ghb's
ghb_stage_west = stage_ghb_west_shift 

# #%% decrease stage to south
# # array = like[1,:,:]
# array2 = like_2d
# array2[0,:] = 1.1  #2
# array2[-1,:] = 1  #1
# array2.plot.imshow()
# array2 = xr.concat(array, dim='help_dim')
# array2.help_dim
# filled = array2.interpolate_na(dim="help_dim")
# filled.plot.imshow()
# interpolated_surface = xr.concat(filled, dim='y')
# interpolated_surface.plot.imshow()
# ghb_stage_west= ghb_stage_west * interpolated_surface  # or just for aquifers ?
# # test[40,:,:].plot.imshow()

#%%

 # increase stage+5 #weg
ghb_conc_west = xr.full_like(like, conc_ghb_west)
ghb_density_west = xr.full_like(like, density_ref)
# making sure GHB is only present in the right cells
ghb_cond_west = ghb_cond_west.where(is_ghb_west.notnull())
ghb_stage_west = ghb_stage_west.where(is_ghb_west.notnull()) 
ghb_conc_west =  ghb_conc_west.where(is_ghb_west.notnull())
ghb_density_west = ghb_density_west.where(is_ghb_west.notnull())
ghb_stage_west[:,:,10].plot.imshow()
ghb_cond_west[0,:,:].plot.imshow()
ghb_cond_west[1,:,:].plot.imshow()
ghb_cond_west[10,:,:].plot.imshow()
# asserting to ensure ghb is present in the right cells
assert ghb_cond_west.count() == ghb_stage_west.count() == ghb_conc_west.count() == ghb_density_west.count()

# %% combining sea and western boundary
ghb_cond = ghb_cond_west.combine_first(ghb_cond_sea)
ghb_stage = ghb_stage_west.combine_first(ghb_stage_sea)
ghb_conc =  ghb_conc_west.combine_first(ghb_conc_sea)
ghb_density = ghb_density_west.combine_first(ghb_density_sea)

# testing that they do not overlap
ghb_cond_2 = ghb_cond_sea.combine_first(ghb_cond_west)
ghb_stage_2 = ghb_stage_sea.combine_first(ghb_stage_west)
ghb_conc_2 =  ghb_conc_sea.combine_first(ghb_conc_west)
ghb_density_2 = ghb_density_sea.combine_first(ghb_density_west)
assert ghb_cond.all() == ghb_cond_2.all()
assert ghb_stage.all() == ghb_stage_2.all()
assert ghb_conc.all() == ghb_conc_2.all()
assert ghb_density.count() == ghb_density_2.count()

ghb_conc[2,:,:].plot.imshow()
ghb_conc[6,:,:].plot.imshow()
ghb_conc[7,:,:].plot.imshow()
ghb_cond_west[0,:,:].plot.imshow()
ghb_cond_west[1,:,:].plot.imshow()
ghb_cond_west[2,:,:].plot.imshow()

# clip everything to ibound
ibound = xr.open_dataarray(path_ibound)
ghb_stage = ghb_stage.where(ibound == 1)   
ghb_conc = ghb_conc.where(ibound == 1)
ghb_cond = ghb_cond.where(ibound == 1)
ghb_density = ghb_density.where(ibound == 1)

# give all parameters the time dimension
ghb_stage = ghb_stage.transpose("time", "z", "y", "x")
ghb_conc = ghb_conc.expand_dims({"time": coords})
ghb_cond = ghb_cond.expand_dims({"time": coords})
ghb_density = ghb_density.expand_dims({"time": coords})

# Save
ds = xr.Dataset()
ds["stage"] = ghb_stage
ds["conc"] = ghb_conc
ds["cond"] = ghb_cond
ds["density"] = ghb_density
# ds = ds.reindex_like(like).transpose("z","y","x")
ds.to_netcdf(path_ghb)

#%% Cross section for ghb

# distance_lines = (ymax-ymin)/(number_cross_sections +1)
# linestrings  = []
# linenames = []

# for i in range(number_cross_sections):
#     linestring = sg.LineString([(xmin, ymax - (i+1) * distance_lines), (xmax, ymax - (i+1) * distance_lines)])
#     linename = f"E-W{i+1}"
#     linestrings.append(linestring)
#     linenames.append(linename)



# levels=np.array([0, 1, 19, 20])  #[0, 1, 10, 100, 1000]
# cmap = "jet"
# # ghb_stage = xr.open_dataset(path_ghb).stage.swap_dims({"z":"layer"})
# ghb_conc = ghb_conc.swap_dims({"z":"layer"})
# ghb_conc ["top"] = ghb_conc.ztop
# ghb_conc ["bottom"] = ghb_conc.zbot

# sections = [
#     imod.select.cross_section_linestring(ghb_conc, ls) for ls in linestrings
# ]

# cmap = ("green", "red", 'blue') 
# levels=np.array([0, 10])
# for i in range(len(linestrings)):
#     # sections[i].s = sections[i].s -5000
#     fig, ax = plt.subplots()
#     imod.visualize.cross_section(
#     # s["chloride"],
#     sections[i],
#     colors=cmap,
#     levels=levels,
#     fig=fig,
#     ax=ax,
#     kwargs_colorbar={
#         "plot_colorbar": False,
#         #"label": "Chloride",
#         # "whiten_triangles": False,
#     }
#     )
#     ax.set_ylim(bottom=zmin, top=zmax)
#     #ax.set_xlim(0, 30000)
#     # s_x = sections[i].coords["x"].values
#     # ax.set_xticklabels(s_x)
#     # ax.set_xlim(left=0, right=30000) 
#     title = (
#             f"GHB Section {linenames[i]}"        
#         )
#     red_patch = mpatches.Patch(color='red', label='0')
#     blue_patch = mpatches.Patch(color='blue', label='19')
#     plt.legend(handles=[red_patch, blue_patch], title="Chloride [kg/m3]")
#     ax.set_title(title)
#     plt.tight_layout()      
#     fig.savefig(
#                 os.path.join(path_cross_section_ghb, f"cross_section_ghb {linenames[i]}_t3.png"),
#                 #f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/cross_section_kh{i}_t.png",
#                 dpi=300,
#             )

#%% Plot sea level rise

# coords2 = coords 
# Settings for plot
SMALL_SIZE = 12 #8
MEDIUM_SIZE = 14 #10
BIGGER_SIZE = 16

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title


sea_level_rise_png = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/sea_level_rise.png"

# vector for only sea level rise
slr = [0, 0.03, 0.06, 0.11, 0.17, 0.26, 0.34, 0.44, 0.56, 0.69, 0.83] 
slr = xr.DataArray(
    #sea_level_change,
    slr,
    coords=[('time', coords)])
# vector for vertical land movement
VLM = sea_level_rise - slr
VLM = xr.DataArray(
    #sea_level_change,
    VLM,
    coords=[('time', coords)])

import matplotlib.dates as mdates

# Plot
fig, ax = plt.subplots(figsize=[10,8])
sea_level_rise.plot.line("r", ax=ax)
slr.plot.line("b", ax=ax)
VLM.plot.line("brown", ax=ax)
plt.scatter(sea_level_rise.time, sea_level_rise,marker='o', color='r')
plt.scatter(slr.time, slr, marker='o', color='b')
plt.scatter(VLM.time, VLM, marker='o', color='peru')
ax.set_title("")
ax.set_ylabel("Change compared to 2005 [m]")
ax.set_xlabel("Time [yrs]")
red_patch = mpatches.Patch(color='red', label='Relative sea level rise')
blue_patch = mpatches.Patch(color='blue', label='Sea level rise (SSP5-8.5)')
brown_patch = mpatches.Patch(color='peru', label='Vertical land movement')
plt.legend(handles=[red_patch, blue_patch, brown_patch])
# plt.xticks(np.arange(min(x), max(x)+1, 1.0))
years = pd.to_datetime(coords).strftime('%Y')
# years = years.astype(int)  
# years = ["2005", "2010", "2020", "2030", "2040", "2050", "2060", "2070", "2080", "2090", "2100"]
plt.xticks(years)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))
plt.tight_layout()
# Save figures
fig.savefig(
    sea_level_rise_png,
    bbox_inches='tight',
    dpi=300,
)


import numpy as np
def datetime_to_int(dt):
    return int(dt)
x = coords
ds = []
for i in range(0,len(x)):
    ds.append(datetime_to_int(y[i]))

y = sea_level_rise.values.flatten()
p = np.polyfit(ds, np.log(y), 1)
a = np.exp(p[1])
b = p[0]
x_fitted = np.linspace(np.min(ds), np.max(ds), 100)
y_fitted = a * np.exp(b * x_fitted)


ax = plt.axes()
ax.scatter(ds, sea_level_rise, label='Raw data')
ax.plot(x_fitted, y_fitted, 'k', label='Fitted curve')


