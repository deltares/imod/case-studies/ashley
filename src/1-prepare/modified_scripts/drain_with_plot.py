#%% Drain

import geopandas as gpd
import imod
import xarray as xr
import pandas as pd
import numpy as np
import flopy
from gridit import Grid
import swn

# ### Snakemake
# ## Parameters
# params = snakemake.params
# modelname = params["modelname"]
# waterway_resistance = params ["resistance_drn"]
# ## Paths
# # Input
# path_channels = snakemake.input.path_channels
# path_pipes = snakemake.input.path_pipes
# path_other = snakemake.input.path_other
# # path_area = snakemake.input.path_area  # not needed?
# path_modeltop = snakemake.input.path_modeltop
# path_template = snakemake.input.path_template
# path_template_2d = snakemake.input.path_template_2d
# path_ibound = snakemake.input.path_ibound
# # Output
# path_drains = snakemake.output.path_drn

### Without snakemake
## Parameters
modelname= "ashley28"
waterway_resistance = 1 #days
## Paths
# Input
path_channels = r"C:\Users\schadt\training\ashley\data\1-external\Drains\Stormwater_Channels.shp"
path_pipes = r"C:\Users\schadt\training\ashley\data\1-external\Drains\Stormwater_Pipes.shp"
path_other = r"C:\Users\schadt\training\ashley\data\1-external\Drains\Stormwater_Pipes_Other.shp"
# path_area = r"c:\Users\schadt\training\ashley\data\1-external\study_area\study_area_exact.shp"
path_modeltop = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/modeltop.nc" # use modeltop for stage
path_template = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/template.nc"
path_template_2d = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/template_2d.nc"
path_ibound = f"C:/Users/schadt/training/ashley/data/2-interim/{modelname}/ibound.nc"
# Output 
path_drains= f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/drn.nc"

#%% Functions

def preprocess_drains(path_waterways, channel):
    lines = gpd.read_file(
        path_waterways, bbox=expanded_bbox) # clip to study area
    if channel == True:
        lines = lines.loc[lines['CLASSIFI_2'] == "Network Drain"] # take only man made drains
    # Only take full LineStrings (chop off MultiLineStrings)
    lines = lines[lines.geom_type == "LineString"]
    # Build surface water network
    n = swn.SurfaceWaterNetwork.from_lines(lines.geometry)
    # gives a file with the length of the line per cell
    m = flopy.modflow.Modflow()  
    _ = flopy.modflow.ModflowDis(
        m, nlay=nlay, nrow=nrow, ncol=ncol, nper=nper,
        delr=resolution, delc=resolution,
        xul=xul, yul=yul)
    _ = flopy.modflow.ModflowBas(m, stoper=5.0)
    nm = swn.SwnModflow.from_swn_flopy(n, m)
    return nm.reaches

def rasterize_waterways(waterways):
    waterways["is_water"] = 1.0
    waterways["ID"] = waterways.index
    waterways ["row_index"] = waterways["i"]
    waterways ["col_index"] = waterways["j"]
    to_rasterize = waterways.sort_values("rchlen", ascending=False).drop_duplicates(
        ["row_index", "col_index"]
    )
    # 1293 before, then 992 because duplicates dropped
    # Create rasters from the table
    is_water = imod.prepare.rasterize_celltable(
        to_rasterize, column="is_water", like=like_2d
    )
    return waterways, to_rasterize, is_water

def parameters_waterways(waterways, to_rasterize, is_water, width, depth):
    # get parameters by mergin rasterization with attributes and calculating conductance and depth
    length = waterways.groupby(["row_index", "col_index"])["rchlen"].sum()
    tmp_df = pd.DataFrame()
    tmp_df["length"] = length.values
    tmp_df["row_index"] = length.index.get_level_values("row_index")
    tmp_df["col_index"] = length.index.get_level_values("col_index")
    to_rasterize = pd.merge(to_rasterize, tmp_df, on=["row_index", "col_index"])
    assert (to_rasterize["length"] >= to_rasterize["rchlen"]).all()
    w = width
    h = depth
    r = waterway_resistance
    to_rasterize["conductance"] = (to_rasterize["length"] * (w + 2 * h) / r) # * 0.5 # muss weg
    conductance = imod.prepare.spatial.rasterize_celltable(
        to_rasterize, column="conductance", like=like_2d
    )
    conductance.plot.imshow()
    depth = waterways_stages.where(is_water.notnull())  # - depth # or put at modeltop?
    return conductance, depth

#%%  General code
## Open data
# Open templates
like = xr.open_dataset(path_template)["template"]
like_2d = xr.open_dataset(path_template_2d)["template"]
dx, xmin, xmax, dy, ymin, ymax = imod.util.spatial_reference(like)
zmin = like.zbot.min()
zmax = like.ztop.max()
dz = like.dz

# Open stages
waterways_stages = xr.open_dataset(path_modeltop)["modeltop_min"] # -0.5  ["modeltop_min"]

#%% Pre-processing

resolution = dx # condition: dx = dy

model_bbox = xmin, ymin, xmax, ymax  # create box for the model area which is a bit larger so that drains at edge are not cut off
expanded_bbox = (
    model_bbox[0] - resolution,
    model_bbox[1] - resolution,
    model_bbox[2] + resolution,
    model_bbox[3] + resolution,
)

grid = Grid.from_bbox(*model_bbox, resolution=resolution, projection="EPSG:2193") #grid for bbox
nlay = np.shape(like)[0]
nrow, ncol = grid.shape
nper = 1
xul, yul = grid.top_left

# get line length per cell
channel_pre = preprocess_drains(path_channels, True)  #get rid of some (natural ones)
channel_pre.plot()
pipe_pre = preprocess_drains(path_pipes, False) 
other_pre = preprocess_drains(path_other, False) 
pipe_pre.plot()
other_pre.plot()

#%% Rasterize

pipe = rasterize_waterways(pipe_pre)
pipe[2].plot.imshow()
channel = rasterize_waterways(channel_pre)
channel[2].plot.imshow()
other = rasterize_waterways(other_pre)
other[2].plot.imshow()

is_drain = channel[2].combine_first(pipe[2])
is_drain = is_drain.combine_first(other[2])

## Compute parameters
conductance_channels = parameters_waterways(channel[0], channel[1], channel[2], 1, 1)[0]
depth_channels = parameters_waterways(channel[0], channel[1], channel[2], 1, 1)[1]
conductance_pipes = parameters_waterways(pipe[0], pipe[1], pipe[2], 1, 1)[0]
depth_pipes = parameters_waterways(pipe[0], pipe[1], pipe[2], 1, 1)[1]
conductance_others = parameters_waterways(other[0], other[1], other[2], 0.5, 1)[0]
depth_others = parameters_waterways(other[0], other[1], other[2], 0.5, 1)[1]

# Combine the different files 
# cond = conductance_channels.combine_first(conductance_pipes)
# cond = cond.combine_first(conductance_others)
# cond = cond *2 # increase conductance everywhere by 2
# add conductances 
conductance_channels.plot.imshow()
conductance_pipes.plot.imshow()
conductance_others.plot.imshow()
# substitute nan by 0
conductance_channels_2 = xr.where(np.isnan(conductance_channels), 0, conductance_channels)
conductance_pipes_2 = xr.where(np.isnan(conductance_pipes), 0, conductance_pipes)
conductance_others_2 = xr.where(np.isnan(conductance_others), 0, conductance_others)
cond = conductance_channels_2+conductance_pipes_2+conductance_others_2 
cond.plot.imshow()
cond.max()
# For depth: order of priority: channels > pipes > others
depth = depth_channels.combine_first(depth_pipes)
depth = depth.combine_first(depth_others)
depth.plot.imshow()



cond_drain = cond.where(cond>0)

#plot map
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt

plt.axis("scaled")
# fig, ax = plt.subplots(figsize=(15, 10))
# change.plot.imshow(add_colorbar=False)
# study_area.plot(edgecolor="black", color="none", ax=ax)
fig, ax = plt.subplots(figsize=(15, 10))
# is_drain.plot.imshow()
cond_drain.plot.imshow()
# fig, ax = imod.visualize.plot_map(
#     cond,
#     return_cbar = False,
#     colors=['green', 'red', 'blue'],
#     levels=[1,2],
#     overlays=overlays,
#     figsize=[15, 10],
# )
# red_patch = mpatches.Patch(color='red', label='Additional areas 2110')
# blue_patch = mpatches.Patch(color='blue', label='Areas with groundwater at surface 2010')
# plt.rcParams["legend.fontsize"] = 18
# fig.legend(handles=[blue_patch, red_patch], bbox_to_anchor=(0.85, 0.1), loc="lower right")  #, 
ax.set_facecolor("0.85")
ax.set_title(f"", fontsize = 16)
ax.set_ylabel("[m EPSG:2193]")
ax.set_xlabel("[m EPSG:2193]")
fig.tight_layout()
fig.savefig(
        f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/drn.png",
        dpi=300,
        )  


















# Create 3D arrays.
drn_depth = xr.full_like(like, 1.0) * depth
drn_cond = xr.full_like(like, 1.0) * cond
drn_depth = drn_depth.where((drn_depth <= drn_depth["ztop"]) & (drn_depth > drn_depth["zbot"])) 
# drn_depth = drn_depth.where((drn_depth <= drn_depth["ztop"])) 

is_drn = xr.full_like(like, 1.0).where(
    ~np.isnan(drn_depth) & ~np.isnan(drn_cond) 
)

is_drn.notnull().any(axis=0).plot.imshow()
ibound = xr.open_dataarray(path_ibound)
is_drn = is_drn.where(ibound == 1)   
drn_depth = drn_depth.where(~np.isnan(is_drn))
drn_cond = drn_cond.where(~np.isnan(is_drn))
assert (
    drn_cond.count()
    == drn_depth.count()
)






# Save
ds = xr.Dataset()
ds["drn_cond"] = drn_cond
ds["drn_depth"] = drn_depth
ds.to_netcdf(path_drains)
