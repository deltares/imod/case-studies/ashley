#%% Geology and aquifer properties

import geopandas as gpd
import imod
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
import os
import shapely.geometry as sg

# ### Snakemake
# ## Parameters
# params = snakemake.params
# modelname = params["modelname"]
# porosity_subsoil = params["porosity_subsoil"]
# specific_storage_constant = params["specific_storage_constant"]
# specific_yield_constant = params["specific_yield_constant"]
# path_cross_section_k = params ["path_cross_section_k"]
# number_cross_sections = params["number_cross_sections"]
# ## Paths
# # Input
# path_top = snakemake.input.path_top 
# path_base = snakemake.input.path_base
# path_modeltop = snakemake.input.path_modeltop
# path_template = snakemake.input.path_template
# path_template_2d = snakemake.input.path_template_2d
# path_area = snakemake.input.path_area
# path_water_mask = snakemake.input.path_water_masks
# # path_cross_section_shape = snakemake.input.path_cross_section_shape
# # Output
# path_conductivity = snakemake.output.path_conductivity
# path_formation_depth = snakemake.output.path_formation_depth
# path_formation_top = snakemake.output.path_formation_top
# path_ibound = snakemake.output.path_ibound


### Without Snakemake
## Parameters
modelname= "ashley28"
porosity_subsoil = 0.3
specific_storage_constant = 1.e-5
specific_yield_constant = 0.15
number_cross_sections = 6 # put in snakemake
## Paths
# Input
path_top = "c:/Users/schadt/training/ashley/data/1-external/geology/top_layers/" 
path_base = "c:/Users/schadt/training/ashley/data/1-external/geology/base_layers/" 
path_modeltop = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/modeltop.nc"
path_template = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/template.nc"
path_template_2d = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/template_2d.nc"
path_cross_section_shape = r"c:\Users\schadt\training\ashley\data\1-external\cross_section\cross_section_lines_short.shp"
path_area = r"c:\Users\schadt\training\ashley\data\1-external\study_area\study_area_exact.shp"
path_water_mask = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/water_masks.nc"
# Output
path_conductivity = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/conductivity.nc"
path_formation_depth = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/formation_depths.nc"
path_formation_top = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/formation_top.nc"
path_ibound = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/ibound.nc"
path_cross_section_k = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/"

#%% General code

## Open data
# Open templates
like = xr.open_dataset(path_template)["template"]
like_2d = xr.open_dataset(path_template_2d)["template"]
dx, xmin, xmax, dy, ymin, ymax = imod.util.spatial_reference(like)
zmin = like.zbot.min() 
zmax = like.ztop.max()
dz = like.dz

# Open modeltop
top = xr.open_dataset(path_modeltop)["modeltop"]
top.plot.imshow()

# Regrid
regridder = imod.prepare.regrid.Regridder("multilinear") #regrid everything to the same grid

#creating array with base of the layers
filenamelist_base = []
filenamecontent_base = []
ibnd_geo_comb = []

for filename in os.listdir(path_base):
    if filename.endswith(".asc"):
        f = xr.open_dataarray(os.path.join(path_base,filename)).sel(y=slice(ymax, ymin)).sel(x=slice(xmin, xmax))
        filename =filename.split("_")[1] + "_" + filename.split("_")[2]
        filenamelist_base.append(filename)
        da_downscaled = regridder.regrid(f.sel(band=1), like_2d)
        # extrapolation: create ibound where the layer should be extrapolated
        NS = np.where(np.any(da_downscaled < 100, axis=1))[0]#question: M[67,0] is a float number but an array --. how to do it instead of <0 with "is na'is numer"
        WE = np.where(np.any(da_downscaled < 100, axis=0))[0]
        N = NS[0]
        S = NS[-1]
        W = WE[0]
        E = WE[-1]
        ibnd_geo = da_downscaled.where(da_downscaled < 100)
        ibnd_geo = ibnd_geo.where(ibnd_geo.isnull(), 1)
        if filename.split("_")[0] =='Springston': # extrapolate to west but not to east
            for i in range(0,E-5,1): #-20 so that it does not smoothen the eastern boundary
                ibnd_geo[:,i] = 1  
        if filename.split("_")[0] =='Avonside': # no extrapolation
                ibnd_geo = ibnd_geo
        if filename.split("_")[0] =='Bromley' or filename.split("_")[0] =='Christchurch' or filename.split("_")[0] =='Heathcote' or filename.split("_")[0] =='Shirley':
            for x in range (N-50,S+1,1): #extrapolate to east but not to west
                for i in range(W+20,like_2d.shape[1],1):  #+20 so that it does not smoothen the western boundary
                    ibnd_geo[x,i] = 1
        if filename.split("_")[1] =='Gravels' or filename.split("_")[0] =='Early-middle':
                    ibnd_geo = xr.where(np.isnan(like_2d), 1,0) # extrapolate whole area 
        da_downscaled = imod.prepare.laplace_interpolate(da_downscaled, ibound=ibnd_geo)
        filenamecontent_base.append(da_downscaled) 
        ibnd_geo_comb.append(ibnd_geo) 
base_combined_e = xr.concat(filenamecontent_base, dim='layer')  #combine extrapolated layers
base_combined_e = base_combined_e.assign_coords({"layer": filenamelist_base})
ibnd_geo_combined = xr.concat(ibnd_geo_comb, dim='layer')  # combine ibounds, will be reused for tops so that tops and bottoms are equal in size
ibnd_geo_combined = ibnd_geo_combined.assign_coords({"layer": filenamelist_base})
# ibnd_geo_combined[1,:,:].plot.imshow()
# base_combined_e[1,:,:].plot.imshow()
 
#creating array with the top of the layers
filenamelist_top = []
filenamecontent_top = []

for filename in os.listdir(path_top):
    if filename.endswith(".asc"):
        f = xr.open_dataarray(os.path.join(path_top,filename)).sel(y=slice(ymax, ymin)).sel(x=slice(xmin, xmax))
        filename =filename.split("_")[1] + "_" + filename.split("_")[2]
        filenamelist_top.append(filename)
        da_downscaled = regridder.regrid(f.sel(band=1), like_2d)
        # extrapolation
        da_downscaled = imod.prepare.laplace_interpolate(da_downscaled, ibound=ibnd_geo_combined['layer' == filename]) #use of the same ibound, to make sure tops and bottoms are equal
        filenamecontent_top.append(da_downscaled) 
top_combined_e = xr.concat(filenamecontent_top, dim='layer')
top_combined_e = top_combined_e.assign_coords({"layer": filenamelist_top}) 
# ibnd_geo_combined[1,:,:].plot.imshow()
# top_combined_e[1,:,:].plot.imshow()
# base_combined_e[1,:,:].plot.imshow()
# test = imod.prepare.laplace_interpolate(top_combined_e[1,:,:], ibound=ibnd_geo_combined[1,:,:])
# test.plot.imshow()
# da_downscaled.plot.imshow()
# test = imod.prepare.laplace_interpolate(top_combined_e[10,:,:], ibound=ibnd_geo_combined[10,:,:])
# test.plot.imshow()

# manual adjustments for geological layers to make sure no gaps present (top of one layer is base of the other)
top_combined_e_nan = top_combined_e.where(ibnd_geo_combined == 1) # get rid of 0 and put nan outside of geology
base_combined_e_nan = base_combined_e.where(ibnd_geo_combined == 1)

for i in range (0, base_combined_e_nan.shape[0]):
    if i == 0:
        top_geo_formation = top
    if i == 1:
        top_geo_formation = base_combined_e_nan[i-1, :, :].combine_first(top)
    if i == 2:
        top_geo_formation = base_combined_e_nan[i-1, :, :].combine_first(base_combined_e_nan[i-2, :, :])
    if i == 3:
        top_geo_formation= base_combined_e_nan[i-1, :, :].combine_first(base_combined_e_nan[i-2, :, :])
        top_geo_formation = top_geo_formation.combine_first(base_combined_e_nan[i-3, :, :])
    if i == 4 :
        top_geo_formation= base_combined_e_nan[i-1, :, :].combine_first(base_combined_e_nan[i-2, :, :])
        top_geo_formation = top_geo_formation.combine_first(base_combined_e_nan[i-3, :, :])
        top_geo_formation = top_geo_formation.combine_first(base_combined_e_nan[i-4, :, :])
    if i == 5:
        top_geo_formation= base_combined_e_nan[i-1, :, :].combine_first(base_combined_e_nan[i-2, :, :])
        top_geo_formation = top_geo_formation.combine_first(base_combined_e_nan[i-3, :, :])
        top_geo_formation = top_geo_formation.combine_first(base_combined_e_nan[i-4, :, :])
        top_geo_formation = top_geo_formation.combine_first(base_combined_e_nan[i-5, :, :])
    if i == 6:
        top_geo_formation= base_combined_e_nan[i-1, :, :].combine_first(base_combined_e_nan[i-2, :, :])
        top_geo_formation = top_geo_formation.combine_first(base_combined_e_nan[i-3, :, :])
        top_geo_formation = top_geo_formation.combine_first(base_combined_e_nan[i-4, :, :])
        top_geo_formation = top_geo_formation.combine_first(base_combined_e_nan[i-5, :, :])
        top_geo_formation = top_geo_formation.combine_first(base_combined_e_nan[i-6, :, :])
    if i==7 or i==8 or i==9 or i==10 or i == 11:
        top_geo_formation= base_combined_e_nan[i-1, :, :].combine_first(base_combined_e_nan[i-2, :, :])
        top_geo_formation = top_geo_formation.combine_first(base_combined_e_nan[i-3, :, :])
        top_geo_formation = top_geo_formation.combine_first(base_combined_e_nan[i-4, :, :])
        top_geo_formation = top_geo_formation.combine_first(base_combined_e_nan[i-5, :, :])
        top_geo_formation = top_geo_formation.combine_first(base_combined_e_nan[i-6, :, :])
        top_geo_formation = top_geo_formation.combine_first(base_combined_e_nan[i-7, :, :])
    top_combined_e_nan[i,:,:] = top_geo_formation.where(~np.isnan(top_combined_e_nan[i, :, :]))
    base_combined_e_nan[base_combined_e_nan.shape[0]-1,:,:]=zmin # test
    base_combined_e_nan[i,:,:] = xr.where((top_combined_e_nan[i, :, :]-base_combined_e_nan[i, :, :])>0,  base_combined_e_nan[i, :, :], top_combined_e_nan[i, :, :]) # make sure that base is lower than top of that layer
    
# ibnd_geo_combined[3,:,:].plot.imshow() 
# top_combined_e_nan[0,:,:].plot.imshow()   
# base_combined_e_nan[9,:,:].plot.imshow()
# da_downscaled.plot.imshow()

# use to assess how thick layers are - often only 2 m, use high vertical resolution
diff = top_combined_e_nan[1, :, :]- base_combined_e_nan[1, :, :] 
diff.plot.imshow()
diff.any()<0

## Define kh and kv
# put in snakemake
kh_aquifer = 500 #10 # 500 #30# 50 #100 originally, but collapsed when everything aquifer -- put 50
kh_aquitard = 0.5
kv_aquifer = kh_aquifer/10  #1 beofre , but changed to 0.1 so that not higher than kh
kv_aquitard = kh_aquitard

geo_layer_kh_value_lookup = {
    "Early-middle_Quaternary" : 100, #kh_aquifer, #3, #kh_aquitard, # 50,#no values, assumption as it is gravel but sandy
    "Wainoni_Gravels" : 200, #kh_aquifer, # 100, #no values, assumption
    "Shirley_Formation" : kh_aquitard, # 0.56, #no values, assumption
    "Burwood_Gravels" : 300, #kh_aquifer, # 100, #2-600 
    "Heathcote_Formation" : kh_aquitard, # 0.56, #aquitard
    "Linwood_Gravels" : 400, #kh_aquifer, # 100, #2-600
    "Bromley_Formation" : kh_aquitard, # 0.07, #aquitard
    "Riccarton_Gravels" : 500, #kh_aquifer, # 100, #2-600
    "Avonside_Member" : kh_aquitard, # 0.04, #aquitard
    "Christchurch_Formation" : 100, # kh_aquitard, # 50, #2-150,  # 15 - 46 according to setiawan
    "Springston_Formation" : 500, #500 #kh_aquifer, # 100, #2-600,
    }

geo_layer_kv_value_lookup = {
    "Early-middle_Quaternary" : 100/10, #kv_aquifer, #kv_aquitard, # 1, # no values, assumption
    "Wainoni_Gravels" : 200/10, #kv_aquifer, # 7.5, # no values, assumption
    "Shirley_Formation" : kv_aquitard, #kv_aquifer, #kv_aquitard, # 0.2, #no values, assumption
    "Burwood_Gravels" : 300/10, #kv_aquifer, # 7.5,
    "Heathcote_Formation" : kv_aquitard, #kv_aquifer, #kv_aquitard, # 0.2,
    "Linwood_Gravels" : 400/10, #kv_aquifer, # 0.02, 
    "Bromley_Formation" : kv_aquitard, #kv_aquifer, # kv_aquitard, # 0.006,
    "Riccarton_Gravels" : 500/10, #kv_aquifer, # 5.1, 
    "Avonside_Member" : kv_aquitard, #100/10, #kv_aquifer, #kv_aquitard, # 5.1,
    "Christchurch_Formation" : 100/10,
    "Springston_Formation" : 500/10, #kv_aquifer, # 28.4, 
    }

#creating array with the kh of the layers
kh = []
for i in filenamelist_top:
    k = xr.full_like(like_2d, 1.0) * geo_layer_kh_value_lookup[i]
    k = k.where(~np.isnan(ibnd_geo_combined.sel(layer =  i)))
    kh.append(k) 
kh_combined_e = xr.concat(kh, dim='layer')
kh_combined_e = kh_combined_e.assign_coords({"layer": filenamelist_top}) 
kh_combined_e[5, :, :].plot.imshow()

#creating array with the kv of the layers
kv = []
for i in filenamelist_top:
    k = xr.full_like(like_2d, 1.0) * geo_layer_kv_value_lookup[i]
    k = k.where(~np.isnan(ibnd_geo_combined.sel(layer =  i)))
    kv.append(k) 
kv_combined_e = xr.concat(kv, dim='layer')
kv_combined_e = kv_combined_e.assign_coords({"layer": filenamelist_top}) 
kv_combined_e[5, :, :].plot.imshow()

# voxelizers
mean_voxelizer = imod.prepare.Voxelizer(method="mean")
harmonicmean_voxelizer = imod.prepare.Voxelizer(method="harmonic_mean")
kh_voxelized = mean_voxelizer.voxelize(kh_combined_e,  top_combined_e_nan,  base_combined_e_nan, like)
kv_voxelized = harmonicmean_voxelizer.voxelize(kv_combined_e,  top_combined_e_nan,  base_combined_e_nan, like)

# ## just to check
# kh_voxelized.sel(z=100, method="nearest").plot.imshow()
# kh_voxelized.sel(z=50, method="nearest").plot.imshow()
# kh_voxelized.sel(z=25, method="nearest").plot.imshow()
# kh_voxelized.sel(z=0, method="nearest").plot.imshow()
# kh_voxelized.sel(z=-5, method="nearest").plot.imshow()
# kh_voxelized.sel(z=-10, method="nearest").plot.imshow()
# kh_voxelized.sel(z=-20, method="nearest").plot.imshow()
# kh_voxelized.sel(z=-30, method="nearest").plot.imshow()

#%%
kh_voxelized = kh_voxelized*1.3
kv_voxelized = kv_voxelized*1.3

# change kh west east
array = like_2d
array[:,0] = 0.7  # west
array[:,-1] = 1.3 # east
array.plot.imshow()
# array = xr.concat(array, dim='help_dim')
# array.help_dim
# filled = array.interpolate_na(dim="help_dim")
filled = array.interpolate_na(dim="x")
filled.plot.imshow()
# interpolated_surface = xr.concat(filled, dim='y')
interpolated_surface = filled
interpolated_surface.plot.imshow()
kh_voxelized = kh_voxelized * interpolated_surface  # or just for aquifers ?
kh_voxelized[20,:,:].plot.imshow()

# change kh north south
array = like_2d
array[0,:] = 0.9  # north
array[-1,:] = 1 #south
array.plot.imshow()
array = xr.concat(array, dim='help_dim')
array.help_dim
filled = array.interpolate_na(dim="help_dim")
filled.plot.imshow()
interpolated_surface = xr.concat(filled, dim='y')
interpolated_surface.plot.imshow()
kh_voxelized = kh_voxelized * interpolated_surface  # or just for aquifers ?


#%%

## Get ibound
# Find active domain 
geo_top = top_combined_e_nan.max(dim="layer") # find top layer (should be equal to modeltop)
# geo_bottom = base_combined_e_nan.min(dim="layer") #find bottom layer  
geo_bottom = top_combined_e_nan[-1,:,:] 
geo_bottom.plot.imshow()
geo_top.plot.imshow()
diff = geo_top-top # are the same
diff.plot.imshow()
ibound = xr.where((like.coords["zbot"] < top ) & (like.coords["z"] > geo_bottom), 1, 0) #where part of cell is within model ibound is 1
gdf = gpd.read_file(path_area)
study_area = imod.prepare.rasterize(gdf, like_2d, column=None)
ibound = ibound.where(study_area == 1).fillna(0)
                                                                                   
# #ibound = xr.where((like.coords["zbot"] < top ) & (like.coords["ztop"] > geo_bottom), 1, 0)
# ibound[0,:,:].plot.imshow()
# ibound[1,:,:].plot.imshow()
# ibound[2,:,:].plot.imshow()
# ibound[3,:,:].plot.imshow()
# ibound[4,:,:].plot.imshow()
# ibound[5,:,:].plot.imshow()
# ibound[16,:,:].plot.imshow()  #check values
# # ibound[60,:,:].plot.imshow()

#  Create porosity
porosity = like.fillna(porosity_subsoil).where(ibound.notnull())  # was kh before
porosity.sel(z=-100, method="nearest").plot.imshow()

# Create specific storage and specific yield
# Setting storange criteria
specific_storage = like.where(ibound.isnull(), specific_storage_constant)
specific_yield = like.fillna(specific_yield_constant).where(ibound.notnull())
# Setting specific storage with specific yield for first layer because water level below surface (unconfined)
# # upper_active_layer = imod.select.upper_active_layer(kh_voxelized, is_ibound=False)
# specific_storage.sel(z=-50, method="nearest").plot.imshow()
# specific_yield.sel(z=-100, method="nearest").plot.imshow()
specific_storage_conf = xr.full_like(like, specific_storage_constant)
# correcting specific yield for first layer
specific_yield_conf = xr.full_like(like, specific_yield_constant)/abs(like.dz)
ibound_swap = ibound.swap_dims({"z":"layer"})
upper_active_layer = imod.select.upper_active_layer(ibound_swap, is_ibound=True)
is_topcell = ibound_swap.where(ibound_swap.layer == upper_active_layer).swap_dims({"layer":"z"})
# only yield where water is supposed to be below surface - clip part that is not sea
water_mask = xr.open_dataset(path_water_mask)["is_sea2d"]
water_mask.plot.imshow()
is_topcell = is_topcell.where(np.isnan(water_mask))
is_topcell[20,:,:].plot.imshow()
specific_storage_conf = xr.where(is_topcell ==1, specific_yield_conf, specific_storage_conf)
specific_storage_conf[20,:,:].plot.imshow()

# clip everything to ibound
kh_voxelized = kh_voxelized.where(ibound == 1) 
kv_voxelized = kv_voxelized.where(ibound == 1) 
porosity = porosity.where(ibound == 1) 
specific_storage = specific_storage.where(ibound == 1) 
specific_storage_conf = specific_storage_conf.where(ibound == 1) 
specific_yield = specific_yield.where(ibound == 1) 

# Save
top_combined_e_nan.to_netcdf(path_formation_top)
base_combined_e_nan.to_netcdf(path_formation_depth)
ds = xr.Dataset()
ds["kh"] = kh_voxelized
ds["kv"] = kv_voxelized
ds["porosity"] = porosity
ds["specific_storage"] = specific_storage
ds["specific_storage_conf"] = specific_storage_conf
ds["specific_yield"] = specific_yield #maybe not needed

ds.to_netcdf(path_conductivity)
ibound.to_netcdf(path_ibound)

#%% Cross section with k values
# Open shapefile
# gdf2 = gpd.read_file(path_cross_section_shape)
# linestrings = [ls for ls in gdf2.geometry]
# linenames = [ls for ls in gdf2.name]
# linestring = gdf2.geometry[0]


# # Plot settings
# SMALL_SIZE = 8 #8
# MEDIUM_SIZE = 8 #10
# BIGGER_SIZE = 12

SMALL_SIZE = 12
MEDIUM_SIZE = 12
BIGGER_SIZE = 16

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title






distance_lines = (ymax-ymin)/(number_cross_sections +1)
linestrings  = []
linenames = []

for i in range(number_cross_sections):
    linestring = sg.LineString([(xmin, ymax - (i+1) * distance_lines), (xmax, ymax - (i+1) * distance_lines)])
    linename = f"W-E{i+1}"
    linestrings.append(linestring)
    linenames.append(linename)
    
levels=np.logspace(0, 3, num=10, endpoint=True) 
# levels=np.array([0.01, 0.1, 1, 10, 100])
# levels=np.array([0, 0.04, 0.07, 0.56, 49, 100])  #[0, 1, 10, 100, 1000]
cmap = "jet_r"
kh = xr.open_dataset(path_conductivity).kh.swap_dims({"z":"layer"})
kh ["top"] = kh.ztop
kh ["bottom"] = kh.zbot

kv = xr.open_dataset(path_conductivity).kv.swap_dims({"z":"layer"})
kv ["top"] = kv.ztop
kv ["bottom"] = kv.zbot

sections = [
    imod.select.cross_section_linestring(kh, ls) for ls in linestrings
]   

for i in range(len(linestrings)):
    # sections[i] = sections[i].rename("Kh (m/d)") 
    fig, ax = plt.subplots()
    imod.visualize.cross_section(
    #s["chloride"],
    sections[i],
    colors=cmap,
    levels=levels,
    fig=fig,
    ax=ax,
    kwargs_colorbar={
        "label": "$K_h$ [m/d]",
        "whiten_triangles": False,
    }
    )
    ax.set_ylim(bottom=zmin, top=zmax) #10 before
    # s_x = sections[i].coords["x"].values
    # ax.set_xticklabels(s_x)
    # ax.set_xlim(left=0, right=30000) 
    title = (
            f"Section {linenames[i]}"        
        )
    ax.set_ylabel("Elevation [m NZVD]")
    ax.set_xlabel("Distance from western boundary [m]")
    ax.set_title(title)
    plt.tight_layout()      
    fig.savefig(
                os.path.join(path_cross_section_k, f"cross_section_kh {linenames[i]}_presentation.png"),
                #f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/cross_section_kh{i}.png",
                bbox_inches='tight',
                dpi=300,
            )


levels=np.logspace(0, 2, num=11, endpoint=True) 
# levels=np.array([0, 0.0004, 0.0006, 0.02, 0.2, 1, 5.1, 7.5, 28.4])  #[0, 1, 10, 100, 1000]
sections = [
    imod.select.cross_section_linestring(kv, ls) for ls in linestrings
]
for i in range(len(linestrings)):
    fig, ax = plt.subplots()
    imod.visualize.cross_section(
    #s["chloride"],
    sections[i],
    colors=cmap,
    levels=levels,
    fig=fig,
    ax=ax,
    kwargs_colorbar={
        "label": "$K_v$ [m/d]",
        "whiten_triangles": False,
    }
    )
    ax.set_ylim(bottom=zmin, top=zmax) #10 before
    # s_x = sections[i].coords["x"].values
    # ax.set_xticklabels(s_x)
    # ax.set_xlim(left=0, right=30000) 
    title = (
            f"Section {linenames[i]}"        
        )
    ax.set_ylabel("Elevation [m NZVD]")
    ax.set_xlabel("Distance from western boundary [m]")
    ax.set_title(title)
    plt.tight_layout()           
    fig.savefig(
                os.path.join(path_cross_section_k, f"cross_section_kv {linenames[i]}_presentation.png"),
                #f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/cross_section_kv{i}.png",
                bbox_inches='tight',
                dpi=300,
            )



