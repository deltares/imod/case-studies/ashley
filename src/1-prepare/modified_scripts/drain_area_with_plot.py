#%% Artificial drainage

import imod
import xarray as xr
import numpy as np

# ### Snakemake
# ## Parameters
# params = snakemake.params
# resistance = params["resistance_drn"]
# ## Paths
# # Input
# path_template = snakemake.input.path_template
# path_template_2d = snakemake.input.path_template_2d
# path_water_mask = snakemake.input.path_water_mask
# path_drainage_raster = snakemake.input.path_drainage_raster
# path_modeltop = snakemake.input.path_modeltop
# path_ibound = snakemake.input.path_ibound # not needed
# # Output
# path_drn2 = snakemake.output.path_drn2

### Without Snakemake
## Parameters
modelname = "ashley28"
resistance = 1.0
## Paths
# Input
path_modeltop = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/modeltop.nc"
path_template = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/template.nc"
path_template_2d = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/template_2d.nc"
path_water_mask = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/water_masks.nc"  # needed?
path_drainage_raster = r"C:\Users\schadt\training\ashley\data\1-external\artificial_drainage\TPP_drainagecls_NZ_v1.img" # new
# Output
path_drn = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/drn.nc"

### General code
## Open data
# Open templates
like = xr.open_dataset(path_template)["template"]
like_2d = xr.open_dataset(path_template_2d)["template"]
dx, xmin, xmax, dy, ymin, ymax = imod.util.spatial_reference(like)
zmin = like.zbot.min()
zmax = like.ztop.max()
dz = like.dz
# Open water mask
water_mask = xr.open_dataset(path_water_mask)["is_sea2d"]
water_mask.plot.imshow()
# Open modeltop
modeltop = xr.open_dataset(path_modeltop)["modeltop"]
modeltop.plot.imshow()
# Open artificial drainage location
artificial_drainage = xr.open_dataarray(path_drainage_raster).sel(band=1, drop=True)
artificial_drainage =artificial_drainage.sel(x=slice(xmin, xmax), y=slice(ymax, ymin))  
artificial_drainage.plot.imshow()
# Regrid 
mean_2d_regridder = imod.prepare.Regridder(method="mean")
artificial_drainage_regrid = mean_2d_regridder.regrid(artificial_drainage, like_2d)
artificial_drainage_regrid = artificial_drainage_regrid.astype("int16")
artificial_drainage_regrid.plot.imshow()

## Calculate drain inputs
# drn depth
# drn_depth = modeltop.where(np.isnan(water_mask))- 1.5 #1.5 m below modeltop 
drn_depth = modeltop.where(artificial_drainage_regrid == 4)  # where it is very likely that there is artificial drainage
drn_depth.plot.imshow()

# Conductance
l = 125 # length of river or ditch in cell, try out # 250 beginning
w = 1.5 # width of river or ditch (use 1.5 m for ditches and 3.0 m for streams)
h = 0.5 # water height (use 0.5 m)
R = resistance  # entry resistance (use 1 for both ditches and streams)
conductance = (l * ( w + 2 * h ) / R) *1.5 #.3 #1.9  # is this supposed to be in m3/d?
# conductance = abs(dx * dy) / resistance
drn_cond = xr.full_like(like_2d, 1.0) * conductance
# drn_cond = drn_cond.where(np.isnan(water_mask))
drn_cond = drn_cond.where(artificial_drainage_regrid == 4)
drn_cond.plot.imshow()

# # Create 3D arrays.
# drn_depth = xr.full_like(like, 1.0) * drn_depth
# drn_cond = xr.full_like(like, 1.0) * drn_cond
# # drn_depth = drn_depth.where((drn_depth <= drn_depth["ztop"]) & (drn_depth > drn_depth["zbot"])) 
# drn_depth = drn_depth.where((drn_depth <= drn_depth["ztop"])) 

# is_drn = xr.full_like(like, 1.0).where(
#     ~np.isnan(drn_depth) & ~np.isnan(drn_cond) 
# )

# is_drn.notnull().any(axis=0).plot.imshow()
# ibound = xr.open_dataarray(path_ibound)
# is_drn = is_drn.where(ibound == 1)   
# drn_depth = drn_depth.where(~np.isnan(is_drn))
# drn_cond = drn_cond.where(~np.isnan(is_drn))
# assert (
#     drn_cond.count()
#     == drn_depth.count()
# )


# Save data
ds = xr.Dataset()
ds["drn_cond"] = drn_cond
ds["drn_depth"] = drn_depth
ds.to_netcdf(path_drn2)
