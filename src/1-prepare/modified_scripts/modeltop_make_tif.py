#%% Modeltop

import xarray as xr
import rioxarray
import imod

# ### Snakemake
# ## Parameters
# params = snakemake.params
# modelname = params["modelname"]
# ## Paths
# # Input
# path_dem = snakemake.input.path_dem
# path_bathymetry = snakemake.input.path_bathymetry
# path_template = snakemake.input.path_template
# path_template_2d = snakemake.input.path_template_2d
# # path_ibound = snakemake.input.path_ibound  #probably no need
# # path_area = snakemake.input.path_area
# # Output
# path_bathymetry_reprojected = snakemake.output.path_bathymetry_reprojected
# path_modeltop = snakemake.output.path_modeltop

### Without Snakemake 
## Parameters
modelname= "ashley28"
## Paths
# Input
path_template = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/template.nc"
path_template_2d = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/template_2d.nc"
path_dem = r"c:\Users\schadt\training\ashley\data\1-external\DEM\DEM.tif"  
path_bathymetry = r"c:\Users\schadt\training\ashley\data\1-external\bathymetry\gebco_2022_n-30.9375_s-58.3594_w162.0879_e186.6973.tif"  
path_ibound = f"C:/Users/schadt/training/ashley/data/2-interim/{modelname}/ibound.nc"
# path_area = r"c:\Users\schadt\training\ashley\data\1-external\study_area\study_area_exact.shp"
# Output
path_bathymetry_reprojected = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/bathy_reproj.tif"
path_modeltop = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/modeltop.nc"

#%% General code

## Open data
# Open templates
like = xr.open_dataset(path_template)["template"]
like_2d = xr.open_dataset(path_template_2d)["template"]
dx, xmin, xmax, dy, ymin, ymax = imod.util.spatial_reference(like)
zmin = like.zbot.min()
zmax = like.ztop.max()
dz = like.dz
# Open DEM
dem = xr.open_dataset(path_dem).sel(x=slice(xmin, xmax), y=slice(ymax, ymin)).sel(band=1, drop=True)["band_data"]  # in m 
# Open bathymetry and reproject to ESPG 2193
bathy = rioxarray.open_rasterio(path_bathymetry)
bathy_reprojected = bathy.rio.reproject("EPSG:2193")
bathy_reprojected.rio.to_raster(path_bathymetry_reprojected) #save reprojected raster
# Open reprojected raster and extract extent
bathy = (
    imod.rasterio.open(path_bathymetry_reprojected)
    .sel(y=slice(ymax, ymin))
    .sel(x=slice(xmin, xmax))
)

# Regrid DEM and the bathymetry
mean_2d_regridder = imod.prepare.Regridder(method="mean") 
top = mean_2d_regridder.regrid(dem, like_2d)
seafloor = mean_2d_regridder.regrid(bathy, like_2d)

# minimum to use for river stage later on
min_2d_regridder = imod.prepare.Regridder(method="minimum")
top_min = min_2d_regridder.regrid(dem, like_2d)
seafloor_min = min_2d_regridder.regrid(bathy, like_2d)

# just to check
diff = top-seafloor
# fig, ax = plt.subplots()
# top.plot.imshow()
# fig.savefig(
#                 f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/conc_crossections/top.png",
#                 dpi=300,
#             )
# fig, ax = plt.subplots()
# seafloor.plot.imshow()
# fig.savefig(
#                 f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/conc_crossections/bathy.png",
#                 dpi=300,
#             )
# fig, ax = plt.subplots()
# diff.plot.imshow()
# fig.savefig(
#                 f"c:/Users/schadt/training/ashley/data/1-external/dem_bathymetry_diff.png",
#                 dpi=300,
#             )

# Combining DEM and bathymetry (where there is not data for DEM, bathymetry is added)
top = top.combine_first(seafloor)
top_min = top_min.combine_first(seafloor_min)
# Fill in case there are gaps
top = imod.prepare.fill(top)  
top_min = imod.prepare.fill(top_min) 

# test
diff= top-top_min
diff.plot.imshow()

# # clip to ibound
# ibound = xr.open_dataarray(path_ibound)
# top = top.where(ibound == 1) 
# top_min = top_min.where(ibound == 1) 
# ## check top.max() 

# # import ibound and check for maximal modeltop within area
# import geopandas as gpd
# gdf = gpd.read_file(path_area)
# ibound_2d = imod.prepare.rasterize(gdf, like_2d, column=None)
# top = top.where(ibound_2d == 1).fillna(0)
# top_min = top_min.where(ibound_2d == 1).fillna(0)
# top.plot.imshow()
# top.max()

# Save
ds = xr.Dataset()
ds["modeltop"] = top
ds["modeltop_min"] = top_min
ds.to_netcdf(path_modeltop)

# import Image
# im = Image.fromarray(top)
# im.save(f'c:/Users/schadt/training/ashley/data/2-interim/{modelname}/modeltop.tif')


top.rio.to_raster(f'c:/Users/schadt/training/ashley/data/2-interim/{modelname}/modeltop_higher_reso_25.tif')






from matplotlib import cbook
from matplotlib import cm
from matplotlib.colors import LightSource
import matplotlib.pyplot as plt
import numpy as np

# Load and format data
dem = cbook.get_sample_data('jacksboro_fault_dem.npz', np_load=True)
z = dem['elevation']
nrows, ncols = z.shape
x = np.linspace(dem['xmin'], dem['xmax'], ncols)
y = np.linspace(dem['ymin'], dem['ymax'], nrows)
x, y = np.meshgrid(x, y)

region = np.s_[5:50, 5:50]
x, y, z = x[region], y[region], z[region]


# x= top.x
# y = top.y
# z= top
# Set up plot
fig, ax = plt.subplots(subplot_kw=dict(projection='3d'))

ls = LightSource(270, 45)
# To use a custom hillshading mode, override the built-in shading and pass
# in the rgb colors of the shaded surface calculated from "shade".
rgb = ls.shade(z, cmap=cm.gist_earth, vert_exag=0.1, blend_mode='soft')



surf = ax.plot_surface(x, y, z, rstride=1, cstride=1, facecolors=rgb,
                       linewidth=0, antialiased=False, shade=False)

plt.show()
















