#%% Recharge

import xarray as xr
import imod
import numpy as np

# ### With Snakemake
# ## Parameters
# ## Paths
# # Input
# path_area = snakemake.input.path_area
# path_template = snakemake.input.path_template
# path_template_2d = snakemake.input.path_template_2d
# path_rch = snakemake.input.path_rch
# path_water_masks = snakemake.input.path_water_masks
# # Output
# path_recharge = snakemake.output.path_recharge

### Without Snakemake
## Parameters
modelname = 'ashley28'
## Paths
# Input
path_template = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/template.nc"
path_template_2d = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/template_2d.nc"
path_water_masks = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/water_masks.nc"
path_rch= r"c:\Users\schadt\training\ashley\data\1-external\recharge\RainfallRecharge_v3.0_inclAge_NHP_meanAnnual.tif"
path_rch= r"C:\Users\schadt\training\ashley\data\1-external\recharge\recharge_arrays.nc"
path_area = r"c:\Users\schadt\training\ashley\data\1-external\study_area\study_area_exact.shp"
# Output
path_recharge = f"c:/Users/schadt/training/ashley/data/2-interim/{modelname}/recharge.nc"

#%% General code

## Open data
# Open templates
like = xr.open_dataset(path_template)["template"]
like_2d = xr.open_dataset(path_template_2d)["template"]
dx, xmin, xmax, dy, ymin, ymax = imod.util.spatial_reference(like)
zmin = like.zbot.min() 
zmax = like.ztop.max()
dz = like.dz
# Open watermask
water_mask = xr.open_dataset(path_water_masks)["is_sea2d"]

# Open recharge zeb 
recharge = xr.open_dataset(path_rch)["current_recharge"][2,:,:]
recharge = recharge.rename({'latitude': 'y'})
recharge = recharge.rename({'longitude': 'x'})
recharge = recharge.drop("scenario")
recharge = recharge.sel(x=slice(xmin, xmax), y=slice(ymax, ymin))  

## for nationwide model
# recharge = xr.open_dataset(path_rch).sel(x=slice(xmin, xmax), y=slice(ymax, ymin)).sel(band=1, drop=True)["band_data"]  
# recharge = recharge.where(recharge != -9999., 0)
# recharge = recharge/365 # in m/d

#just to test
recharge.plot.imshow()

# remove high recharge values over kapoi
recharge[57:62,52:62].plot.imshow()
recharge[62:65,52:62].plot.imshow()
mean_r = (np.mean(recharge[56:57,52:62]) + np.mean(recharge[62:65,52:62]))/2
recharge[57:62,52:62] = mean_r
recharge.plot.imshow()


# Regrid recharge to template resolution
mean_2d_regridder = imod.prepare.Regridder(method="mean")
rch_out = mean_2d_regridder.regrid(recharge, like_2d)
# with 50% recharge:
# rch_out_half = 0.5* rch_out
# rch_out.plot.imshow(vmin=0, vmax=1) #just to check
# rch_out_half.plot.imshow()
rch_out.plot.imshow()
# Masking where water is present according to water_mask 
rch_out = imod.prepare.fill(rch_out).where(water_mask != 1)  

# clipping to ibound, creating 2d ibound from study area
import geopandas as gpd
gdf = gpd.read_file(path_area)
ibound_2d = imod.prepare.rasterize(gdf, like_2d, column=None)
rch_out = rch_out.where(ibound_2d == 1) #.fillna(0)
rch_out.plot.imshow()
rch_out_m3 = rch_out*250*250
rch_out_m3.sum()
area = 4138*250*250 #m2
rch_out_m3.sum()/area *1000 *365 # mm/y

# recharge from ghb west in ref scenario m3/d
1.7e6/area *1000*365

# Creating dataset and saving
rch = xr.Dataset()
rch["rch"] = rch_out
rch.to_netcdf(path_recharge)

# fig, ax = plt.subplots()
# rch_out.plot.imshow(vmin=0, vmax=0.004)
# fig.savefig(
#                 f"c:/Users/schadt/training/ashley/data/5-visualization/{modelname}/conc_crossections/recharge.png",
#                 dpi=300,
#             )
